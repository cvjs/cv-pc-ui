import cvNoticeBar from './src/notice-bar.vue';

cvNoticeBar.install = function (Vue) {
  Vue.component(cvNoticeBar.name, cvNoticeBar);
};
export { cvNoticeBar };
