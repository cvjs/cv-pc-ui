import cvTabsJump from './src/tabs.vue';
import cvTabPane from './src/tab-pane.vue';

cvTabsJump.install = function (Vue) {
  Vue.component(cvTabsJump.name, cvTabsJump);
};
cvTabPane.install = function (Vue) {
  Vue.component(cvTabPane.name, cvTabPane);
};

export { cvTabsJump, cvTabPane };
