// import cvTransferBase from './src/base.vue'
// cvTransferBase.install = function (Vue) {
//   Vue.component(cvTransferBase.name, cvTransferBase);
// }

import cvTransferTable from './src/table.vue';
cvTransferTable.install = function (Vue) {
  Vue.component(cvTransferTable.name, cvTransferTable);
};

export {
  // cvTransferBase,
  cvTransferTable
};
