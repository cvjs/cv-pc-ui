export default {
  undo: false, // 撤销
  redo: false, // 重做
  strong: true, // 粗体
  italic: true, // 斜体
  overline: true, // 下划线
  h1: true,
  h2: true,
  h3: true,
  h4: true,
  h5: true,
  h6: true,
  hr: true, // 分割线
  quote: true, // 引用
  ul: true, // 无序列表
  ol: true, // 有序列表
  notChecked: true, // 未完成列表
  checked: true, // 已完成列表
  link: true, // 链接
  table: true, // 表格
  /**
   *
   */
  image: true, // 图片
  uploadImage: true, // 本地图片
  custom_image: true, // 图片空间
  custom_uploadImage: true, // 上传图片
  custom_uploadFile: true, // 上传文件
  importmd: true, // 导入
  exportmd: true, // 导出
  code: true, // 代码
  html_to_markdown: true, // html转markdown
  /** 右侧 */
  split: true, // 分屏显示
  preview: true, // 预览
  scrollSync: true, // 同步滚动
  save: true, // 保存
  clear: true, // 清除
  copy: true, // 复制
  toc: true, // 目录
  fullscreen: true, // 全屏
  /**
   * 新版
   */
  theme: true, // 代码块主题
  backgroundColor: false,
  language: false,
  phrases: false,
  disabled: false,
  autofocus: false,
  indentWithTab: false
};
