export default {
  props: {
    editorID: {
      type: [String],
      required: false,
      default() {
        return '';
      }
    }
  },
  data() {
    return {
      editorRefEL: null
    };
  },
  computed: {
    editorRefID() {
      return this.editorID ? this.editorID : this.$options.name + +new Date() + ((Math.random() * 1000).toFixed(0) + '');
    }
  }
};
