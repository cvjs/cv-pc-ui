import { saveFile, datePad } from './utils.js';
export default {
  methods: {
    toggleSlideDown() {
      // 显示主题选项
      this.slideDown = !this.slideDown;
    },
    // 设置主题
    actionThemesSet(name) {
      this.codemrrConfig.theme = name;
      this.actionThemeDropdown = false;
      this.$emit('on-act-theme', name);
    },
    actionChooseImage() {
      // 选择图片
      const input = document.createElement('input');
      input.type = 'file';
      input.accept = 'image/*';
      input.onchange = () => {
        const files = input.files;
        if (files[0]) {
          this.$emit('on-act-choose-image', files[0]);
          input.value = '';
        }
      };
      input.click();
    },
    // 撤销
    actionUndo() {},
    // 重做
    actionRedo() {},
    // 背景色
    actionInsBg() {
      const point = this.getCursortPosition();
      const lastChart = this.localVal.substring(point - 1, point);
      this.insertContent('====');
      if (lastChart !== '\n' && this.localVal !== '') {
        this.setCaretPosition(point + 5);
      } else {
        this.setCaretPosition(point + 5);
      }
    },
    // 粗体
    actionInsStrong() {
      const point = this.getCursortPosition();
      const lastChart = this.localVal.substring(point - 1, point);
      this.insertContent('****');
      if (lastChart !== '\n' && this.localVal !== '') {
        this.setCaretPosition(point + 2);
      } else {
        this.setCaretPosition(point + 2);
      }
    },
    // 斜体
    actionInsItalic() {
      const point = this.getCursortPosition();
      const lastChart = this.localVal.substring(point - 1, point);
      this.insertContent('**');
      if (lastChart !== '\n' && this.localVal !== '') {
        this.setCaretPosition(point + 1);
      } else {
        this.setCaretPosition(point + 1);
      }
    },
    // 删除线
    actionInsOverline() {
      const point = this.getCursortPosition();
      const lastChart = this.localVal.substring(point - 1, point);
      this.insertContent('~~~~');
      if (lastChart !== '\n' && this.localVal !== '') {
        this.setCaretPosition(point + 2);
      } else {
        this.setCaretPosition(point + 2);
      }
    },
    // 下划线
    actionInsUnderline() {
      const point = this.getCursortPosition();
      const lastChart = this.localVal.substring(point - 1, point);
      this.insertContent('<u></u>');
      if (lastChart !== '\n' && this.localVal !== '') {
        this.setCaretPosition(point + 3);
      } else {
        this.setCaretPosition(point + 5);
      }
    },
    // 插入标题
    actionInsTitle(level) {
      const titleLevel = {
        1: '#  ',
        2: '##  ',
        3: '###  ',
        4: '####  ',
        5: '#####  ',
        6: '######  '
      };
      this.insertContent(titleLevel[level]);
    },
    // 插入分割线
    actionInsLine() {
      this.insertContent('\n----\n');
    },
    // 引用
    actionInsQuote() {
      this.insertContent('\n>  ');
    },
    // 无序列表
    actionInsUl() {
      this.insertContent('-  ');
    },
    // 有序列表
    actionInsOl() {
      this.insertContent('1. ');
    },
    // 未完成列表
    insertNotFinished() {
      this.insertContent('- [ ]  ');
    },
    // 已完成列表
    insertFinished() {
      this.insertContent('- [x]  ');
    },
    // 插入链接
    actionInsLink() {
      this.insertContent('\n[link](href)');
    },
    // 插入表格
    actionInsTable() {
      this.insertContent('\nheader 1 | header 2\n---|---\nrow 1 col 1 | row 1 col 2\nrow 2 col 1 | row 2 col 2\n\n');
    },
    // 插入图片
    actionInsImage() {
      this.insertContent('\n![image](imgUrl)');
    },
    // 导入本地文件
    actionImportFile(e) {
      const file = e.target.files[0];
      if (!file) {
        return;
      }
      const { type } = file;
      if (!['text/markdown', 'text/src'].includes(type)) {
        return;
      }
      const reader = new FileReader();
      reader.readAsText(file, {
        encoding: 'utf-8'
      });
      reader.onload = () => {
        this.localVal = reader.result;
        e.target.value = '';
        // // 专业版，手动set value
        // this.editor.setOption('value', this.localVal);
      };
      reader.onerror = (err) => {
        console.error(err);
      };
    },
    // 导出为.md格式
    actionExportFile() {
      saveFile(this.localVal, this.lastExpFileName + '.md');
    },
    // 插入code
    insertCode() {
      const point = this.getCursortPosition();
      const lastChart = this.localVal.substring(point - 1, point);
      this.insertContent('\n```\n\n```');
      if (lastChart !== '\n' && this.localVal !== '') {
        this.setCaretPosition(point + 5);
      } else {
        this.setCaretPosition(point + 5);
      }
    },
    // 保存操作
    actionSaveSet() {
      const { localVal, codemrrConfig, lastHtml } = this;
      this.$emit('on-act-save', {
        theme: codemrrConfig.theme || '',
        value: localVal || '',
        html: lastHtml || ''
      });
    },
    /**
     * 自定义事件
     * @param act
     */
    actionCustom(act) {
      this.$emit('on-act-custom', act);
    }
  }
};
