import { saveFile, datePad } from './utils.js';
import html2md from 'html-to-md';
export default {
  methods: {
    // 撤销
    actionUndo() {},
    // 重做
    actionRedo() {
      const { editor } = this;
      editor.redo();
      setTimeout(() => {
        editor.refresh();
      }, 20);
    },
    // 背景色
    actionInsBg() {},
    // 粗体
    actionInsStrong() {
      const { editor, lastPos = {} } = this;
      const { line = 0, ch = 0 } = lastPos;
      const selection = editor.getSelection();
      if (selection) {
        this.insertContent('**' + selection + '**');
      } else {
        this.insertContent('****');
        this.setCursor(line, ch + 2);
      }
    },
    // 斜体
    actionInsItalic() {
      const { editor, lastPos = {} } = this;
      const { line = 0, ch = 0 } = lastPos;
      const selection = editor.getSelection();
      if (selection) {
        this.insertContent('*' + selection + '*');
      } else {
        this.insertContent('**');
        this.setCursor(line, ch + 1);
      }
    },
    // 删除线
    actionInsOverline() {
      const { editor, lastPos = {} } = this;
      const { line = 0, ch = 0 } = lastPos;
      const selection = editor.getSelection();
      if (selection) {
        this.insertContent('~~' + selection + '~~');
      } else {
        this.insertContent('~~~~');
        this.setCursor(line, ch + 2);
      }
    },
    // 下划线
    actionInsUnderline() {
      const { editor, lastPos = {} } = this;
      const { line = 0, ch = 0 } = lastPos;
      const selection = editor.getSelection();
      if (selection) {
        this.insertContent('<u>' + selection + '</u>');
      } else {
        this.insertContent('<u></u>');
        this.setCursor(line, ch + 3);
      }
    },
    // 插入标题
    actionInsTitle(level) {
      const titleLevelArr = {
        1: '#  ',
        2: '##  ',
        3: '###  ',
        4: '####  ',
        5: '#####  ',
        6: '######  '
      };
      const { editor, lastPos = {} } = this;
      const { line } = lastPos;
      const selection = editor.getSelection();
      if (selection) {
        this.insertContent('\n' + titleLevelArr[level] + selection + '\n');
      } else {
        const title = titleLevelArr[level];
        if (editor.isClean()) {
          this.insertContent(title);
          this.setCursor(0, title.length);
        } else {
          this.insertContent('\n' + title);
          this.setCursor(line + 1, title.length);
        }
      }
    },
    // 插入分割线
    actionInsLine() {
      const { editor } = this;
      if (editor.isClean()) {
        this.insertContent('----\n');
      } else {
        this.insertContent('\n\n----\n');
      }
    },
    // 引用
    actionInsQuote() {
      const { editor, lastPos = {} } = this;
      const { line = 0 } = lastPos;
      const selection = editor.getSelection();
      if (selection) {
        this.insertContent('\n>  ' + selection + '\n\n');
      } else {
        if (editor.isClean()) {
          this.insertContent('>  ');
          this.setCursor(0, 3);
        } else {
          this.insertContent('\n>  ');
          this.setCursor(line + 1, 3);
        }
      }
    },
    // 无序列表
    actionInsUl() {
      const { editor, lastPos = {} } = this;
      const { line = 0, ch = 0 } = lastPos;
      const selection = editor.getSelection();
      if (selection) {
        this.insertContent('\n-  ' + selection + '\n\n');
      } else {
        if (editor.isClean() || ch === 0) {
          this.insertContent('-  ');
          this.setCursor(line, 3);
        } else {
          this.insertContent('\n-  ');
          this.setCursor(line + 1, 3);
        }
      }
    },
    // 有序列表
    actionInsOl() {
      const { editor, lastPos = {} } = this;
      const { line = 0, ch = 0 } = lastPos;
      const selection = editor.getSelection();
      if (selection) {
        this.insertContent('\n1.  ' + selection + '\n\n');
      } else {
        if (editor.isClean() || ch === 0) {
          this.insertContent('1.  ');
          this.setCursor(line, 4);
        } else {
          this.insertContent('\n1.  ');
          this.setCursor(line + 1, 4);
        }
      }
    },
    // 未完成列表
    insertNotFinished() {
      const { editor, lastPos = {} } = this;
      const { line = 0, ch = 0 } = lastPos;
      const selection = editor.getSelection();
      if (selection) {
        this.insertContent('\n- [ ] ' + selection + '\n\n');
      } else {
        if (editor.isClean() || ch === 0) {
          this.insertContent('- [ ] ');
          this.setCursor(line, 6);
        } else {
          this.insertContent('\n- [ ] ');
          this.setCursor(line + 1, 6);
        }
      }
    },
    // 已完成列表
    insertFinished() {
      const { editor, lastPos = {} } = this;
      const { line = 0, ch = 0 } = lastPos;
      const selection = editor.getSelection();
      if (selection) {
        this.insertContent('\n- [x] ' + selection + '\n\n');
      } else {
        if (editor.isClean() || ch === 0) {
          this.insertContent('- [x] ');
          this.setCursor(line, 6);
        } else {
          this.insertContent('\n- [x] ');
          this.setCursor(line + 1, 6);
        }
      }
    },
    // 插入链接
    actionInsLink() {
      this.insertContent('\n[link](href)');
    },
    // 插入表格
    actionInsTable() {
      this.insertContent('\nheader 1 | header 2\n---|---\nrow 1 col 1 | row 1 col 2\nrow 2 col 1 | row 2 col 2\n\n');
    },
    // 插入图片
    actionInsImage() {
      this.insertContent('\n![image](imgUrl)');
    },
    // 导入本地文件
    actionImportFile(e) {
      const file = e.target.files[0];
      if (!file) {
        return;
      }
      const { type } = file;
      if (!['text/markdown', 'text/src'].includes(type)) {
        return;
      }
      const reader = new FileReader();
      reader.readAsText(file, {
        encoding: 'utf-8'
      });
      reader.onload = () => {
        this.localVal = reader.result;
        e.target.value = '';
        // // 专业版，手动set value
        this.editor.setOption('value', this.localVal);
      };
      reader.onerror = (err) => {
        console.error(err);
      };
    },
    // 导出为.md格式
    actionExportFile() {
      saveFile(this.localVal, this.lastExpFileName + '.md');
    },
    // 插入code
    insertCode() {
      const { editor, lastPos = {} } = this;
      const { line } = lastPos;
      const selection = editor.getSelection();
      if (selection) {
        this.insertContent('\n```\n' + selection + '\n```\n');
      } else {
        if (editor.isClean()) {
          this.insertContent('```\n\n```');
          this.setCursor(1, 0);
        } else {
          this.insertContent('\n```\n\n```');
          this.setCursor(line + 2, 0);
        }
      }
    },
    /** html 转 md */
    actionHtmlVisible(type) {
      this.actionHtmlText = '';
      if (type) {
        this.actionHtmlDiaVisible = true;
      } else {
        this.actionHtmlDiaVisible = false;
      }
    },
    actionHtmlToMD() {
      // this.insertContent('\n' + toMarkdown(this.actionHtmlValue, { gfm: true }));
      this.insertContent('\n' + html2md(this.actionHtmlText));
      this.actionHtmlText = '';
      this.actionHtmlDiaVisible = false;
    },
    // 保存操作
    actionSaveSet() {
      const { localVal, codemrrConfig, lastHtml } = this;
      this.$emit('on-save', {
        theme: codemrrConfig.theme || '',
        value: localVal || '',
        html: lastHtml || ''
      });
    }
  }
};
