import { saveFile, datePad } from './utils.js';
import html2md from 'html-to-md';
import { EditorState, EditorSelection } from '@codemirror/state';
// import { EditorState } from '@codemirror/state';
// import { EditorState, basicSetup } from '@codemirror/basic-setup';
/*
 * 默认头部菜单配置
 */
import defaultTools from './toolbar-config.js';

import SvgImgMap from '../markdown/font/SvgImgMap.vue';
import SvgUploadFile from '../markdown/font/SvgUploadFile.vue';
import SvgToc from '../markdown/font/SvgToc.vue';
import SvgCopy from '../markdown/font/SvgCopy.vue';
import SvgLanguage from '../markdown/font/SvgLanguage.vue';
export default {
  components: { SvgImgMap, SvgUploadFile, SvgToc, SvgCopy, SvgLanguage },
  emits: ['on-action', 'on-act-copy', 'on-act-save', 'on-act-theme', 'on-act-custom', 'on-act-choose-image'],
  props: {
    exportFileName: {
      // 默认导出文件名称
      type: String,
      default: ''
    },
    toolbars: {
      // 工具栏
      type: Object,
      default() {
        return {};
      }
    },
    isPreview: {
      //是否是预览模式
      type: Boolean,
      default: false
    },
    isFullscreen: {
      //是否是预览模式
      type: Boolean,
      default: false
    }
  },
  data() {
    return {
      actionThemeDropdown: false, // 主题
      actionPhrasesDropdown: false, //
      actionLanguageDropdown: false, // 语言
      actionBgColorDropdown: false, // 背景颜色
      actionSplit: true, // 分屏显示
      actionPreview: false, // 是否是预览状态
      actionScrollSync: true, // 同步滚动
      actionToc: true, // 目录
      actionFullscreen: false, // 是否是全屏
      actionHtmlDiaVisible: false, // 弹窗是否显示
      actionHtmlText: ''
    };
  },
  computed: {
    lastExpFileName() {
      let now = new Date();
      let year = now.getFullYear();
      let month = now.getMonth() + 1; // 月份是从 0 开始的，所以要加 1
      let day = now.getDate();
      let hour = now.getHours();
      let minute = now.getMinutes();
      // 格式化
      let formattedDate = year + '-' + datePad(month) + '-' + datePad(day) + '_' + datePad(hour) + '-' + datePad(minute);
      return this.exportFileName || formattedDate;
    },
    toolConfig() {
      const { toolbars = {} } = this;
      return {
        ...defaultTools,
        ...toolbars
      };
    }
  },
  created() {
    // 初始化
    this.actionPreview = this.isPreview || false;
  },
  methods: {
    actionPhrasesSet(type) {
      this.codemrrConfig.phrases = type;
      this.actionPhrasesDropdown = false;
    },
    actionLanguageSet(type) {
      this.codemrrConfig.language = type;
      this.actionLanguageDropdown = false;
    },
    actionBgColorSet(type) {
      this.codemrrConfig.backgroundColor = type;
      this.actionBgColorDropdown = false;
    },
    // 设置主题
    actionThemesSet(name) {
      this.codemrrConfig.theme = name;
      this.actionThemeDropdown = false;
      this.$emit('on-act-theme', name);
    },
    // 设置全屏
    actionFullscreenSet(type) {
      this.actionFullscreen = type;
      let elObj = this.$refs[this.editorWrapRefID];
      if (type) {
        elObj.classList.add('fullscreen');
      } else {
        elObj.classList.remove('fullscreen');
      }
      this.actionCurrent('fullscreen', type);
    },
    actionChooseImage() {
      // 选择图片
      const input = document.createElement('input');
      input.type = 'file';
      input.accept = 'image/*';
      input.onchange = () => {
        const files = input.files;
        if (files[0]) {
          this.$emit('on-act-choose-image', files[0]);
          input.value = '';
        }
      };
      input.click();
    },
    // 背景色
    actionInsBg() {
      const lastPos = this.getLastPoint();
      const selection = lastPos.content || false;
      let insertContent = '';
      if (selection) {
        this.insertContent('==' + selection + '==');
      } else {
        this.insertContent('====');
        this.setCursor(lastPos.line, lastPos.ch + 1);
      }
    },
    /**
     * =========
     */
    // 撤销
    actionUndo() {
      // editor.undo()
      if (this.editorViewInst) {
        // const newState = this.editorViewInst.state.update({ changes: { undo: true } });
        // this.editorViewInst.update([
        //   {
        //     changes: { from: 0, to: newState.doc.length, insert: newState.doc }
        //   }
        // ]);
        const prevState = this.editorViewInst.state.behavior(EditorState.effect(EditorState.undo));
        this.editorViewInst.update([
          {
            changes: { from: 0, to: prevState.doc.length, insert: prevState.doc },
            selection: EditorSelection.single(0)
          }
        ]);
        // const newState = EditorState.undo(this.editorViewInst.state);
        // this.editorViewInst.update([
        //   {
        //     changes: newState.changes,
        //     annotations: newState.annotation
        //   }
        // ]);

        // this.editorViewInst.dispatch({ ...this.editorViewInst.state, effects: EditorState.undo() });
      }
    },
    // 重做
    actionRedo() {
      if (this.editorViewInst) {
        this.editorViewInst.dispatch({ ...this.editorViewInst.state, effects: EditorState.redo() });
      }
    },
    // 粗体
    actionInsStrong() {
      const lastPos = this.getLastPoint();
      const selection = lastPos.content || false;
      let insertContent = '';
      if (selection) {
        insertContent = '**' + selection + '**';
        this.deleteContent(lastPos.start, lastPos.end);
        this.insertContent(lastPos.start, insertContent);
      } else {
        insertContent = '****';
        this.insertContent(lastPos.start, insertContent);
      }
      this.setCursor(lastPos.start + insertContent.length, lastPos.start + insertContent.length);
    },
    // 斜体
    actionInsItalic() {
      const lastPos = this.getLastPoint();
      const selection = lastPos.content || false;
      let insertContent = '';
      if (selection) {
        insertContent = '*' + selection + '*';
        this.deleteContent(lastPos.start, lastPos.end);
        this.insertContent(lastPos.start, insertContent);
      } else {
        insertContent = '**';
        this.insertContent(lastPos.start, insertContent);
      }
      this.setCursor(lastPos.start + insertContent.length, lastPos.start + insertContent.length);
    },
    // 删除线
    actionInsOverline() {
      const lastPos = this.getLastPoint();
      const selection = lastPos.content || false;
      let insertContent = '';
      if (selection) {
        insertContent = '~~' + selection + '~~';
        this.deleteContent(lastPos.start, lastPos.end);
        this.insertContent(lastPos.start, insertContent);
      } else {
        insertContent = '~~~~';
        this.insertContent(lastPos.start, insertContent);
      }
      this.setCursor(lastPos.start + insertContent.length, lastPos.start + insertContent.length);
    },
    // 下划线
    actionInsUnderline() {
      const lastPos = this.getLastPoint();
      const selection = lastPos.content || false;
      let insertContent = '';
      if (selection) {
        insertContent = '<u>' + selection + '</u>';
        this.deleteContent(lastPos.start, lastPos.end);
        this.insertContent(lastPos.start, insertContent);
      } else {
        insertContent = '<u></u>';
        this.insertContent(lastPos.start, insertContent);
      }
      this.setCursor(lastPos.start + insertContent.length, lastPos.start + insertContent.length);
    },
    // 插入标题
    actionInsTitle(level) {
      const titleLevelArr = {
        1: '#  ',
        2: '##  ',
        3: '###  ',
        4: '####  ',
        5: '#####  ',
        6: '######  '
      };
      const titleLevel = titleLevelArr[level];
      const lastPos = this.getLastPoint();
      const selection = lastPos.content || false;
      let insertContent = '\n' + titleLevel + '标题\n\n';
      if (selection) {
        if (selection.startsWith('#')) {
          this.insertContent(lastPos.end + 1, insertContent);
        } else {
          insertContent = '\n' + titleLevel + selection + '\n';
          this.deleteContent(lastPos.start, lastPos.end);
          this.insertContent(lastPos.start, insertContent);
        }
      } else {
        this.insertContent(lastPos.start, insertContent);
      }
      this.setCursor(lastPos.start + insertContent.length, lastPos.start + insertContent.length);
    },
    // 插入分割线
    actionInsLine() {
      const lastPos = this.getLastPoint();
      const selection = lastPos.content || false;
      let insertContent = '\n----\n';
      if (selection) {
        this.insertContent(lastPos.end + 1, insertContent);
      } else {
        this.insertContent(lastPos.start, insertContent);
      }
      this.setCursor(lastPos.start + insertContent.length, lastPos.start + insertContent.length);
    },
    // 引用
    actionInsQuote() {
      const lastPos = this.getLastPoint();
      const selection = lastPos.content || false;
      let insertContent = '';
      if (selection) {
        insertContent = '\n>  ' + selection + '\n\n';
        this.deleteContent(lastPos.start, lastPos.end);
        this.insertContent(lastPos.start, insertContent);
      } else {
        insertContent = '>  ';
        this.insertContent(lastPos.start, insertContent);
      }
      this.setCursor(lastPos.start + insertContent.length, lastPos.start + insertContent.length);
    },
    // 无序列表
    actionInsUl() {
      const lastPos = this.getLastPoint();
      const selection = lastPos.content || false;
      let insertContent = '';
      if (selection) {
        insertContent = '\n-  ' + selection + '';
        this.deleteContent(lastPos.start, lastPos.end);
        this.insertContent(lastPos.start, insertContent);
      } else {
        insertContent = '-  ';
        this.insertContent(lastPos.start, insertContent);
      }
      this.setCursor(lastPos.start + insertContent.length, lastPos.start + insertContent.length);
    },
    // 有序列表
    actionInsOl() {
      const lastPos = this.getLastPoint();
      const selection = lastPos.content || false;
      let insertContent = '';
      if (selection) {
        insertContent = '\n1.  ' + selection + '';
        this.deleteContent(lastPos.start, lastPos.end);
        this.insertContent(lastPos.start, insertContent);
      } else {
        insertContent = '1.  ';
        this.insertContent(lastPos.start, insertContent);
      }
      this.setCursor(lastPos.start + insertContent.length, lastPos.start + insertContent.length);
    },
    // 未完成列表
    insertNotFinished() {
      const lastPos = this.getLastPoint();
      const selection = lastPos.content || false;
      let insertContent = '';
      if (selection) {
        insertContent = '\n- [ ] ' + selection + '';
        this.deleteContent(lastPos.start, lastPos.end);
        this.insertContent(lastPos.start, insertContent);
      } else {
        insertContent = '- [ ] ';
        this.insertContent(lastPos.start, insertContent);
      }
      this.setCursor(lastPos.start + insertContent.length, lastPos.start + insertContent.length);
    },
    // 已完成列表
    insertFinished() {
      const lastPos = this.getLastPoint();
      const selection = lastPos.content || false;
      let insertContent = '';
      if (selection) {
        insertContent = '\n- [x] ' + selection + '';
        this.deleteContent(lastPos.start, lastPos.end);
        this.insertContent(lastPos.start, insertContent);
      } else {
        insertContent = '- [x] ';
        this.insertContent(lastPos.start, insertContent);
      }
      this.setCursor(lastPos.start + insertContent.length, lastPos.start + insertContent.length);
    },
    // 插入链接
    actionInsLink() {
      const lastPos = this.getLastPoint();
      const selection = lastPos.content || false;
      let insertContent = '  \n[link](http://cvjs.cn)';
      var regex = /\[.*?\]\(.*?\)/g;
      if (selection) {
        if (selection.match(regex) != null) {
          // return false;
          console.log('---selection.match(regex)---', selection.match(regex), selection, lastPos.start, selection.length);
          this.insertContent(lastPos.end, insertContent);
        } else {
          insertContent = `\n[${selection}](http://cvjs.cn)`;
          this.deleteContent(lastPos.start, lastPos.end);
          this.insertContent(lastPos.start, insertContent);
        }
      } else {
        this.insertContent(lastPos.start, insertContent);
      }
      this.setCursor(lastPos.start + insertContent.length, lastPos.start + insertContent.length);
    },
    // 插入表格
    actionInsTable() {
      const lastPos = this.getLastPoint();
      let insertContent = '';
      insertContent = '\n\n表头 1 | 表头 2\n:---|:---\nrow 1 col 1 | row 1 col 2\nrow 2 col 1 | row 2 col 2\n\n';
      this.insertContent(lastPos.end, insertContent);
      this.setCursor(lastPos.start + insertContent.length, lastPos.start + insertContent.length);
    },
    // 插入图片
    actionInsImage() {
      const lastPos = this.getLastPoint();
      let insertContent = '';
      insertContent = '\n\n![图片](https://f.10yun.com/default/default.gif)';
      this.insertContent(lastPos.end, insertContent);
      this.setCursor(lastPos.start + insertContent.length, lastPos.start + insertContent.length);
    },
    // 导入本地文件
    actionImportFile(e) {
      const file = e.target.files[0];
      if (!file) {
        return;
      }
      const { type } = file;
      if (!['text/markdown', 'text/src'].includes(type)) {
        return;
      }
      const reader = new FileReader();
      reader.readAsText(file, {
        encoding: 'utf-8'
      });
      reader.onload = () => {
        this.localVal = reader.result;
      };
      reader.onerror = (err) => {
        console.error(err);
      };
    },
    // 导出为.md格式
    actionExportFile() {
      let lastExpFileExe = '.text';
      const exeArr = {
        java: '.java',
        php: '.php',
        css: '.css',
        vue: '.vue',
        go: '.go',
        python: '.py',
        json: '.json',
        javascript: '.js',
        js: '.js',
        html: '.html',
        markdown: '.md'
      };
      if (this.language && exeArr[this.language]) {
        lastExpFileExe = exeArr[this.language];
      }
      saveFile(this.localVal, this.lastExpFileName + lastExpFileExe);
    },
    // 插入code
    insertCode() {
      const lastPos = this.getLastPoint();
      const selection = lastPos.content || false;
      let insertContent = '';
      if (selection) {
        insertContent = '\n```text\n' + selection + '\n```\n';
        this.deleteContent(lastPos.start, lastPos.end);
        this.insertContent(lastPos.start, insertContent);
      } else {
        insertContent = '\n```text\n\n```\n';
        this.insertContent(lastPos.start, insertContent);
      }
      this.setCursor(lastPos.start + insertContent.length, lastPos.start + insertContent.length);
    },
    // 保存操作
    actionSaveSet() {
      const { localVal, codemrrConfig, lastHtml } = this;
      this.$emit('on-act-save', {
        theme: codemrrConfig.theme || '',
        value: localVal || '',
        html: lastHtml || ''
      });
    },
    /**复制 */
    actionCopySet() {
      this.$cvCopy(this.localVal);
      this.$emit('on-act-copy', this.localVal);
    },
    /** html 转 md */
    actionHtmlVisible(type) {
      this.actionHtmlText = '';
      if (type) {
        this.actionHtmlDiaVisible = true;
      } else {
        this.actionHtmlDiaVisible = false;
      }
    },
    actionHtmlToMD() {
      // this.insertContent('\n' + toMarkdown(this.actionHtmlValue, { gfm: true }));
      const lastPos = this.getLastPoint();
      let insertContent = '';
      insertContent = '\n' + html2md(this.actionHtmlText);
      this.insertContent(lastPos.end, insertContent);
      this.setCursor(lastPos.start + insertContent.length, lastPos.start + insertContent.length);

      this.actionHtmlText = '';
      this.actionHtmlDiaVisible = false;
    },
    /**
     * 返回当前
     */
    actionCurrent(currAction, currState) {
      this.$emit('on-action', currAction, currState);
    },
    /**
     * 自定义事件
     * @param act
     */
    actionCustom(act) {
      this.$emit('on-act-custom', act);
    }
  }
};
