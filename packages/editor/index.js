import cvEditorCode from './src/code.vue';
import cvEditorJson from './src/json.vue';
import cvEditorTinymce from './src/tinymce.vue';
/**
 * markdown
 */
// import cvEditorMarkdown from './src/markdown-v5.vue';
import cvEditorMarkdown from './src/markdown-v6.vue';
import cvMarkdownShow from './src/markdown-show.vue';
import cvMarkdownSimple from './src/markdown-simple.vue';
import cvMarkdownNostyle from './src/markdown-nostyle.vue';

cvEditorMarkdown.install = function (Vue) {
  Vue.component(cvEditorMarkdown.name, cvEditorMarkdown);
};
cvMarkdownShow.install = function (Vue) {
  Vue.component(cvMarkdownShow.name, cvMarkdownShow);
};
cvMarkdownSimple.install = function (Vue) {
  Vue.component(cvMarkdownSimple.name, cvMarkdownSimple);
};
cvMarkdownNostyle.install = function (Vue) {
  Vue.component(cvMarkdownNostyle.name, cvMarkdownNostyle);
};

cvEditorCode.install = function (Vue) {
  Vue.component(cvEditorCode.name, cvEditorCode);
};
cvEditorJson.install = function (Vue) {
  Vue.component(cvEditorJson.name, cvEditorJson);
};
cvEditorTinymce.install = function (Vue) {
  Vue.component(cvEditorTinymce.name, cvEditorTinymce);
};

export { cvEditorCode, cvEditorJson, cvEditorTinymce, cvEditorMarkdown, cvMarkdownShow, cvMarkdownSimple, cvMarkdownNostyle };
