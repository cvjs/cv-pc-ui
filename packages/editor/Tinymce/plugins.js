// Any plugins you want to use has to be imported
// Detail plugins list see https://www.tinymce.com/docs/plugins/
// Custom builds see https://www.tinymce.com/download/custom-builds/

const plugins = [
  'advlist',
  'anchor',
  'autolink',
  'autosave',
  'code',
  'codesample',
  'directionality',
  'emoticons',
  'fullscreen',
  // 'imagetools',
  'image',
  'insertdatetime',
  'link',
  'lists',
  'media',
  'nonbreaking',
  'pagebreak',
  'preview',
  'save',
  'searchreplace',
  'table',
  // 'textpattern',
  // 'template',
  'visualblocks',
  'visualchars',
  'wordcount'
];

export default plugins;
