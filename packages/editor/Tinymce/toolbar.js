// Here is a list of the toolbar
// Detail list see https://www.tinymce.com/docs/advanced/editor-control-identifiers/#toolbarcontrols

const toolbar = [
  // charmap
  'hr blockquote anchor pagebreak insertdatetime table emoticons | searchreplace preview code codesample | fullscreen | link media image ',
  'inserttemplate undo redo | styles | removeformat subscript superscript forecolor backcolor bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | bullist numlist indent'
];

export default toolbar;
