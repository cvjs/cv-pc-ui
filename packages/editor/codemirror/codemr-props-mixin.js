import CodemirrorEditor from '../codemirror/component.js';
/**
 * 主题
 */
import { oneDark } from '@codemirror/theme-one-dark';
import { materialLight } from '@ddietr/codemirror-themes/material-light';
import { materialDark } from '@ddietr/codemirror-themes/material-dark';
import { solarizedLight } from '@ddietr/codemirror-themes/solarized-light';
import { solarizedDark } from '@ddietr/codemirror-themes/solarized-dark';
import { dracula } from '@ddietr/codemirror-themes/dracula';
import { githubLight } from '@ddietr/codemirror-themes/github-light';
import { githubDark } from '@ddietr/codemirror-themes/github-dark';
import { aura } from '@ddietr/codemirror-themes/aura';
import { tokyoNight } from '@ddietr/codemirror-themes/tokyo-night';
import { tokyoNightStorm } from '@ddietr/codemirror-themes/tokyo-night-storm';
import { tokyoNightDay } from '@ddietr/codemirror-themes/tokyo-night-day';
/**
 * 语法
 */
import { javascript } from '@codemirror/lang-javascript';
import { html } from '@codemirror/lang-html';
import { json } from '@codemirror/lang-json';
import { markdown } from '@codemirror/lang-markdown';
import { vue } from '@codemirror/lang-vue';
import { php } from '@codemirror/lang-php';
import { java } from '@codemirror/lang-java';
import { sql } from '@codemirror/lang-sql';
import { python } from '@codemirror/lang-python';
import { xml } from '@codemirror/lang-xml';
import { go } from '@codemirror/lang-go';
import { css } from '@codemirror/lang-css';
import { language } from '@codemirror/language';

const defaultConfig = {
  /**
   * 自定义的
   */
  // styleActiveLine: true,
  // lineNumbers: true,
  // lineWrapping: false,
  // matchBrackets: true,
  // autoCloseBrackets: true,
  // autoRefresh: true,
  // line: true,
  // mode: 'javascript',
  // mode: 'text/x-src',
  // cursorHeight: 0.8,
  // lineWiseCopyCut: true,
  // height: '500px',
  // fontSize: '14px',
  /**
   *
   */
  disabled: false,
  indentWithTab: true,
  tabSize: 4,
  autofocus: false,
  placeholder: 'input...',
  backgroundColor: 'white',
  language: 'markdown',
  theme: 'oneDark',
  phrases: 'en-us'
};

export default {
  emits: ['update:config'],
  components: { CodemirrorEditor },
  data() {
    return {
      codemrrConfig: { ...defaultConfig },
      codemrrThemes: {
        oneDark,
        materialLight,
        materialDark,
        solarizedLight,
        solarizedDark,
        dracula,
        githubLight,
        githubDark,
        aura,
        tokyoNight,
        tokyoNightStorm,
        tokyoNightDay
      },
      codemrrLanguages: {
        javascript: javascript(),
        js: javascript(),
        html: html(),
        markdown: markdown(),
        json: json(),
        vue: vue(),
        java: java(),
        php: php(),
        python: python(),
        go: go(),
        mysql: sql(),
        sql: sql(),
        xml: xml(),
        css: css()
      },
      codemrrPhrases: {
        // @codemirror/view
        'Control character': 'Steuerzeichen',
        // @codemirror/commands
        'Selection deleted': 'Auswahl gelöscht',
        // @codemirror/language
        'Folded lines': 'Eingeklappte Zeilen',
        'Unfolded lines': 'Ausgeklappte Zeilen',
        to: 'bis',
        'folded code': 'eingeklappter Code',
        unfold: 'ausklappen',
        'Fold line': 'Zeile einklappen',
        'Unfold line': 'Zeile ausklappen',
        // @codemirror/search
        'Go to line': 'Springe zu Zeile',
        go: 'OK',
        Find: 'Suchen',
        Replace: 'Ersetzen',
        next: 'nächste',
        previous: 'vorherige',
        all: 'alle',
        'match case': 'groß/klein beachten',
        'by word': 'ganze Wörter',
        replace: 'ersetzen',
        'replace all': 'alle ersetzen',
        close: 'schließen',
        'current match': 'aktueller Treffer',
        'replaced $ matches': '$ Treffer ersetzt',
        'replaced match on line $': 'Treffer on Zeile $ ersetzt',
        'on line': 'auf Zeile',
        // @codemirror/autocomplete
        Completions: 'Vervollständigungen',
        // @codemirror/lint
        Diagnostics: 'Diagnosen',
        'No diagnostics': 'Keine Diagnosen'
      },
      editorViewInst: null,
      editorStateInst: null
    };
  },
  computed: {
    optionTheme() {
      let themes = Object.keys(this.codemrrThemes);
      let theOpt = ['default'].concat(themes);
      return theOpt;
    },
    optionPhrases() {
      return ['en-us', 'de-de'];
    },
    optionLanguage() {
      return Object.keys(this.codemrrLanguages);
    },
    extensions() {
      const result = [];
      result.push(this.codemrrLanguages[this.codemrrConfig.language]);
      if (this.codemrrThemes[this.codemrrConfig.theme]) {
        result.push(this.codemrrThemes[this.codemrrConfig.theme]);
      }
      return result;
    },
    codemrrBind() {
      return {
        theme: this.codemrrConfig.theme,
        language: this.codemrrConfig.language,
        autofocus: this.codemrrConfig.autofocus,
        placeholder: this.codemrrConfig.placeholder,
        indentWithTab: this.codemrrConfig.indentWithTab,
        tabSize: this.codemrrConfig.tabSize,
        disabled: this.codemrrConfig.disabled,
        phrases: this.codemrrConfig.phrases === 'en-us' ? {} : this.codemrrPhrases
      };
    }
  },
  watch: {
    codemrrConfig(newVal) {
      this.$emit('update:config', newVal);
    }
  }
};
