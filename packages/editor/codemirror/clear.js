import { EditorView } from '@codemirror/view';
import { EditorState } from '@codemirror/state';
import { history } from '@codemirror/commands';

const historyComp = new Compartment();

const state = EditorState.create({
  doc: '',
  extensions: [historyComp.of(history())]
});

const view = new EditorView({
  parent: document.querySelector('#app'),
  state
});

const clearHistory = () => {
  // 移除历史扩展
  view.dispatch({
    effects: historyComp.reconfigure([])
  });

  // 重新添加历史扩展
  view.dispatch({
    effects: historyComp.reconfigure(history())
  });
};
