import { defineComponent, shallowRef, computed, watch, toRaw, onMounted, onBeforeUnmount, h, inject } from 'vue';
import { basicSetup } from 'codemirror';
import { EditorState, EditorSelection, Compartment, StateEffect } from '@codemirror/state';
import { EditorView, keymap, placeholder } from '@codemirror/view';
import { indentWithTab } from '@codemirror/commands';
import { indentUnit } from '@codemirror/language';

export const DEFAULT_CONFIG = Object.freeze({
  autofocus: false,
  disabled: false,
  indentWithTab: true,
  tabSize: 2,
  placeholder: '',
  autoDestroy: true,
  extensions: [basicSetup]
});

// const CONFIG_SYMBOL = Symbol('vue-codemirror-global-config');
// export const injectGlobalConfig = (app, config) => {
//   app.provide(CONFIG_SYMBOL, config);
// };

// export const useGlobalConfig = () => {
//   return inject(CONFIG_SYMBOL, {});
// };

const EventKey = {
  Change: 'change',
  Update: 'update',
  Focus: 'focus',
  Blur: 'blur',
  Ready: 'ready',
  ModelUpdate: 'update:modelValue'
};

export const createEditorState = ({ onUpdate, onChange, onFocus, onBlur, ...config }) => {
  return EditorState.create({
    doc: config.doc,
    selection: config.selection,
    extensions: [
      ...(Array.isArray(config.extensions) ? config.extensions : [config.extensions]),
      EditorView.updateListener.of((viewUpdate) => {
        // https://discuss.codemirror.net/t/codemirror-6-proper-way-to-listen-for-changes/2395/11
        onUpdate(viewUpdate);
        // doc changed
        if (viewUpdate.docChanged) {
          onChange(viewUpdate.state.doc.toString(), viewUpdate);
        }
        // focus state change
        if (viewUpdate.focusChanged) {
          viewUpdate.view.hasFocus ? onFocus(viewUpdate) : onBlur(viewUpdate);
        }
      })
    ]
  });
};

// https://codemirror.net/examples/config/
// https://github.com/uiwjs/react-codemirror/blob/22cc81971a/src/useCodeMirror.ts#L144
// https://gist.github.com/s-cork/e7104bace090702f6acbc3004228f2cb
export const createEditorCompartment = (view) => {
  const compartment = new Compartment();
  const run = (extension) => {
    compartment.get(view.state)
      ? view.dispatch({ effects: compartment.reconfigure(extension) }) // reconfigure
      : view.dispatch({ effects: StateEffect.appendConfig.of(compartment.of(extension)) }); // inject
  };
  return { compartment, run };
};

// https://codemirror.net/examples/reconfigure/
export const createEditorExtensionToggler = (view, extension) => {
  const { compartment, run } = createEditorCompartment(view);
  return (targetApply) => {
    const exExtension = compartment.get(view.state);
    const apply = targetApply ?? exExtension !== extension;
    run(apply ? extension : []);
  };
};

export const getEditorTools = (view) => {
  // doc state
  const getDoc = () => view.state.doc.toString();
  const setDoc = (newDoc) => {
    if (newDoc !== getDoc()) {
      view.dispatch({
        changes: {
          from: 0,
          to: view.state.doc.length,
          insert: newDoc
        }
      });
    }
  };

  // UX operations
  const focus = () => view.focus();

  // reconfigure extension
  const { run: reExtensions } = createEditorCompartment(view);

  // disabled editor
  const toggleDisabled = createEditorExtensionToggler(view, [EditorView.editable.of(false), EditorState.readOnly.of(true)]);

  // https://codemirror.net/examples/tab/
  const toggleIndentWithTab = createEditorExtensionToggler(view, keymap.of([indentWithTab]));

  // tab size
  // https://gist.github.com/s-cork/e7104bace090702f6acbc3004228f2cb
  const { run: reTabSize } = createEditorCompartment(view);
  const setTabSize = (tabSize) => {
    reTabSize([EditorState.tabSize.of(tabSize), indentUnit.of(' '.repeat(tabSize))]);
  };

  // phrases
  // https://codemirror.net/examples/translate/
  const { run: rePhrases } = createEditorCompartment(view);
  const setPhrases = (phrases) => {
    rePhrases([EditorState.phrases.of(phrases)]);
  };

  // set editor's placeholder
  const { run: rePlaceholder } = createEditorCompartment(view);
  const setPlaceholder = (value) => {
    rePlaceholder(placeholder(value));
  };

  // set style to editor element
  // https://codemirror.net/examples/styling/
  const { run: reStyle } = createEditorCompartment(view);
  const setStyle = (style = {}) => {
    reStyle(EditorView.theme({ '&': { ...style } }));
  };

  return {
    focus,
    getDoc,
    setDoc,
    reExtensions,
    toggleDisabled,
    toggleIndentWithTab,
    setTabSize,
    setPhrases,
    setPlaceholder,
    setStyle
  };
};

export default defineComponent({
  name: 'VueCodemirror',
  props: {
    modelValue: {
      type: String,
      default: ''
    },
    autofocus: {
      type: Boolean,
      default: false
    },
    disabled: {
      type: Boolean,
      default: false
    },
    indentWithTab: {
      type: Boolean,
      default: false
    },
    tabSize: Number,
    placeholder: String,
    style: Object,
    autoDestroy: {
      type: Boolean,
      default: false
    },
    phrases: Object,
    // codemirror options
    root: Object,
    extensions: Array,
    selection: Object
  },
  emits: {
    // when content(doc) change only
    [EventKey.Change]: (value, viewUpdate) => true,
    // when codemirror state change
    [EventKey.Update]: (viewUpdate) => true,
    [EventKey.Focus]: (viewUpdate) => true,
    [EventKey.Blur]: (viewUpdate) => true,
    // when component mounted
    // payload : { view: null, state: null, container: null }
    [EventKey.Ready]: (payload) => true,
    [EventKey.ModelUpdate]: (value, viewUpdate) => true
  },
  setup(props, context) {
    const container = shallowRef();
    const state = shallowRef();
    const view = shallowRef();

    const defaultConfig = {
      ...DEFAULT_CONFIG
      // ...useGlobalConfig()
    };

    const config = computed(() => {
      const result = {};
      Object.keys(toRaw(props)).forEach((key) => {
        if (key !== 'modelValue') {
          // @ts-ignore
          // MARK: ensure access to `prop[key]` original object
          result[key] = props[key] ?? defaultConfig[key];
        }
      });
      return result;
    });

    onMounted(() => {
      state.value = createEditorState({
        doc: props.modelValue,
        selection: config.value.selection,
        // The extensions are split into two parts, global and component prop.
        // Only the global part is initialized here.
        // The prop part is dynamically reconfigured after the component is mounted.
        extensions: defaultConfig.extensions ?? [],
        onFocus: (viewUpdate) => context.emit(EventKey.Focus, viewUpdate),
        onBlur: (viewUpdate) => context.emit(EventKey.Blur, viewUpdate),
        onUpdate: (viewUpdate) => context.emit(EventKey.Update, viewUpdate),
        onChange: (newDoc, viewUpdate) => {
          if (newDoc !== props.modelValue) {
            context.emit(EventKey.Change, newDoc, viewUpdate);
            context.emit(EventKey.ModelUpdate, newDoc, viewUpdate);
          }
        }
      });

      view.value = new EditorView({
        state: state.value,
        parent: container.value,
        root: config.value.root
      });

      view.value.contentDOM.addEventListener('scroll', (event) => {
        console.log('Scrolling', event);
        // 在这里执行滚动事件的逻辑
      });

      const editorTools = getEditorTools(view.value);

      // watch prop.modelValue
      watch(
        () => props.modelValue,
        (newValue) => {
          if (newValue !== editorTools.getDoc()) {
            editorTools.setDoc(newValue);
          }
        }
      );

      // watch prop.extensions
      watch(
        () => props.extensions,
        (extensions) => editorTools.reExtensions(extensions || []),
        { immediate: true }
      );

      // watch prop.disabled
      watch(
        () => config.value.disabled,
        (disabled) => editorTools.toggleDisabled(disabled),
        { immediate: true }
      );

      // watch prop.indentWithTab
      watch(
        () => config.value.indentWithTab,
        (iwt) => editorTools.toggleIndentWithTab(iwt),
        { immediate: true }
      );

      // watch prop.tabSize
      watch(
        () => config.value.tabSize,
        (tabSize) => editorTools.setTabSize(tabSize),
        { immediate: true }
      );

      // watch prop.phrases
      watch(
        () => config.value.phrases,
        (phrases) => editorTools.setPhrases(phrases || {}),
        { immediate: true }
      );

      // watch prop.placeholder
      watch(
        () => config.value.placeholder,
        (placeholder) => editorTools.setPlaceholder(placeholder),
        { immediate: true }
      );

      // watch prop.style
      watch(
        () => config.value.style,
        (style) => editorTools.setStyle(style),
        { immediate: true }
      );

      // immediate autofocus
      if (config.value.autofocus) {
        editorTools.focus();
      }

      context.emit(EventKey.Ready, {
        state: state.value,
        view: view.value,
        container: container.value,
        editorTools: editorTools
      });
    });

    onBeforeUnmount(() => {
      if (config.value.autoDestroy && view.value) {
        view.value.destroy();
      }
    });
    return () => {
      return h('div', {
        class: 'cv-editor-codemirror',
        // style: { display: 'contents' },
        ref: container
      });
    };
  }
});
