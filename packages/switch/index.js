import cvSwitchBase from './src/base.vue';

cvSwitchBase.install = function (Vue) {
  Vue.component(cvSwitchBase.name, cvSwitchBase);
};

export { cvSwitchBase };
