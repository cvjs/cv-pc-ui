export default {
  props: {
    // 是否剪裁
    crop: {
      type: Boolean,
      default: false
    },
    // 裁剪高度
    cropHeight: {
      type: Number
    },
    // 裁剪宽度
    cropWidth: {
      type: Number
    }
  },
  data() {
    return {
      isShowCrop: false,
      cropData: {}
    };
  },
  watch: {
    isShowCrop(value) {
      if (value === false) {
        this.cropData = {};
      }
    }
  },
  mounted() {
    // 插入到body中, 避免弹出层被遮盖
    if (this.crop && this.$refs.cropper) {
      document.body.appendChild(this.$refs.cropper.$el);
    }
  },
  methods: {
    handleSetFileSet(fileName, fileType, fileSize) {
      const uid = this.cropData.uid || new Date().getTime();
      this.cropData = {
        name: fileName,
        percentage: 0,
        size: fileSize,
        type: fileType,
        status: 'ready',
        uid: uid
      };
    },
    handleCropSuccess(b64Data) {
      this.cropData.url = b64Data;
    },
    handleCropUploadError(status) {
      this.$message.error('上传失败, 请重试');
      this.$emit('error', status);
    },
    handleCropUploadSuccess(response) {
      this.cropData.status = 'success';
      this.cropData.percentage = 100;
      this.cropData.response = response;
      const file = Object.assign({}, this.cropData);
      let index = this.fileList.findIndex((item) => item.uid === file.uid);
      if (index > -1) {
        this.fileList.splice(index, 1, file);
      } else {
        this.fileList.push(file);
      }
      this.upImageSuccess(response, file, this.fileList);
    }
  }
};
