// 用于上传类的组件工具mixin
import ajax from '/src/utils/ajax.js';

export default {
  inheritAttrs: false,
  props: {
    // // 上传地址 (同官网) action
    // upAction: {
    //   type: [String],
    //   default: () => {
    //     // let globParam = this.$cvUiParams || '';
    //     // if (globParam != '') {
    //     //   return (globParam
    //     //     && globParam[this.name]
    //     //     && globParam[this.name].upAction) || "https://httpbin.org/post";
    //     // } else {
    //     //   return "https://httpbin.org/post";
    //     // }
    //     return "https://httpbin.org/post";
    //   },
    // },
    // upHeaders: {
    //   type: [Object],
    //   default: () => {
    //     return {}
    //   }
    // },
    // // 上传时附带的额外参数(同官网)
    // upData: {
    //   type: [Object],
    //   default: () => {
    //     return {}
    //   }
    // },
    // // 上传的文件字段名 (同官网)
    // upName: {
    //   type: [String],
    //   default: "file"
    // },
    valInit: {
      type: [String, Array],
      default: () => {
        return '';
      }
    },
    // 是否启用拖拽上传 (同官网) drag
    drag: {
      type: Boolean,
      default: false
    },
    // 	支持发送 cookie 凭证信息 (同官网) withCredentials
    withCredentials: {
      type: Boolean,
      default: false
    },
    // 是否支持多选文件 (同官网) multiple
    multiple: {
      type: Boolean,
      default: false
    },
    // 覆盖默认的上传行为，可以自定义上传的实现 (同官网) httpRequest
    httpRequest: Function,
    // 接受上传的文件类型（thumbnail-mode 模式下此参数无效）(同官网)
    accept: String,
    // 删除前的操作
    // 同 element upload 组件
    beforeRemove: Function,
    // 响应处理函数
    upResponseFn: Function
  },
  data() {
    return {
      // 标识: 用于注入全局的 upload 参数
    };
  },
  computed: {
    //
    upAction() {
      let lastSett = this.mergeAttrs.upAction || '';
      return lastSett;
    },
    // 设置上传的请求头部(同官网) ，headers
    upHeaders() {
      let lastSett = this.mergeAttrs.upHeaders || {};
      return lastSett;
    },
    // 上传时附带的额外参数(同官网) ，data
    upData() {
      let lastSett = this.mergeAttrs.upData || {};
      return lastSett;
    },
    // 上传的文件字段名 (同官网)，name
    upName() {
      let lastSett = this.mergeAttrs.upName || 'file';
      return lastSett;
    },
    // 获取组件名: 去除EleForm, 并将组件转为 kebab-case
    componentName() {
      let name = this.$options.name;
      // name = name
      //   .replace('cv', '')
      //   .replace(/[A-Z\u00C0-\u00D6\u00D8-\u00DE]/g, '-$&')
      //   .toLowerCase()
      // if (name.startsWith('-')) name = name.slice(1)
      return name;
    },
    // 属性: 默认属性 + 全局属性 + 自定义属性
    mergeAttrs() {
      // 全局属性
      let globalAttrs = this.$cvUiParams || {};
      // 全局上传组件属性
      let globalUploadAttrs = globalAttrs.upload || {};
      // 最终组件属性
      let lastCompAttrs = Object.assign({}, globalUploadAttrs, globalAttrs[this.componentName]);
      // console.log('---lastCompAttrs---', lastCompAttrs, this.componentName);
      return Object.assign(
        {},
        lastCompAttrs,
        this.$attrs
        // { disabled: this.disabled || this.$attrs.disabled },
        // { readonly: this.readonly || this.$attrs.readonly }
      );
    }
  },
  methods: {
    // 上传图片
    handleImageUpload(file, callback) {
      const mergeAttrs = this.mergeAttrs;
      if (!file) {
        this.$message.error('文件不存在');
        return;
      }
      if (mergeAttrs.fileSize) {
        const isLt = file.size / 1024 / 1024 < mergeAttrs.fileSize;
        if (!isLt) {
          this.$message.error('上传文件大小' + ` ${mergeAttrs.fileSize} MB!`);
          return;
        }
      }
      const isImg = file.type.includes('image');
      if (!isImg) {
        this.$message.error('上传类型错误');
        return;
      }
      if (!mergeAttrs.upAction) {
        this.$message.error('上传地址错误');
        return;
      }
      ajax({
        action: mergeAttrs.upAction,
        file: file,
        headers: mergeAttrs.upHeaders,
        filename: mergeAttrs.upName,
        data: mergeAttrs.upData,
        onProgress() {
          return false;
        },
        onSuccess: (response) => {
          if (mergeAttrs.upResponseFn) {
            file.url = URL.createObjectURL(file);
            response = mergeAttrs.upResponseFn(response, file);
          }
          callback(response);
        },
        onError: (error) => {
          this.$message.error('上传错误');
          console.error(error);
        }
      });
    },
    beforeRemove2() {
      return new Promise((resolve, reject) => {
        this.$confirm('提示', {
          title: '确定删除??',
          showCancelButton: true,
          confirmButtonText: '确定',
          cancelButtonText: '取消',
          type: 'warning'
        })
          .then(() => {
            resolve(true);
          })
          .catch(() => {
            reject(false);
          });
      });
    },
    // 自定义上传
    uploadRequestFn(param) {
      console.log(this.listObj);
      let formData = new FormData();
      formData.append('file', param.file);
      let filetype = '';
      if (param.file.type === 'image/png') {
        filetype = 'png';
      } else {
        filetype = 'jpg';
      }
      console.log(filetype);
      const postData = {
        filetype: filetype,
        param: param
      };
      axios({
        url: 'http://api.10.com/v1sdk/ctofile.file_img_opt',
        methods: 'post',
        data: formData,
        headers: {
          'Content-Type': 'multipart/form-data',
          ...this.headers
        }
      }).then((succ) => {
        console.log(succ);
      });
      // usersApi.getImgUploadToken(msg).then((response) => {
      //   if (response.stateCode === 200) {
      //     commit('uploadImg', { 'token': response.token, 'key': response.key, 'param': formdata.param })
      //   }
      // }, (error) => {
      //   console.log(`获取图片上传凭证错误：${error}`)
      //   commit('uploadImgError')
      // })
    }
  }
};
