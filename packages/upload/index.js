import cvUploadAvatar from './src/upload-avatar';
import cvUploadImage from './src/upload-image';
import cvUploadFile from './src/upload-file';
import cvUploadVideo from './src/upload-video';

cvUploadAvatar.install = function (Vue) {
  Vue.component(cvUploadAvatar.name, cvUploadAvatar);
};

cvUploadImage.install = function (Vue) {
  Vue.component(cvUploadImage.name, cvUploadImage);
};

cvUploadFile.install = function (Vue) {
  Vue.component(cvUploadFile.name, cvUploadFile);
};

cvUploadVideo.install = function (Vue) {
  Vue.component(cvUploadVideo.name, cvUploadVideo);
};

export { cvUploadAvatar, cvUploadImage, cvUploadFile, cvUploadVideo };
