import cvRow from './src/row.vue';
import cvCol from './src/col.vue';

// 作用：清除浮动，解决高度塌陷；使用情景：作为组件的外部容器
cvRow.install = function (Vue) {
  Vue.component(cvRow.name, cvRow);
};
// 当组件未占满一行但有换行需求时，添加一个cvCol空标签可以占满改该行剩余空间
cvCol.install = function (Vue) {
  Vue.component(cvCol.name, cvCol);
};

export { cvRow, cvCol };
