import { ExtendInput } from '/src/extend-ui.js';
export default {
  extends: ExtendInput,
  emits: ['update:modelValue', 'change'],
  props: {
    ...ExtendInput.props,
    // 提示文本
    modelValue: {
      type: [Number, String],
      default: ''
    },
    placeholder: {
      type: [Number, String],
      default: ''
    },
    disabled: {
      type: Boolean,
      default: false
    }
  },
  watch: {
    modelValue(val) {
      this.localVal = val;
    },
    localVal() {
      this.handleChange();
      this.handleInput();
    }
  },
  data() {
    return {
      localVal: this.modelValue,
      // 分割后的验证信息
      localRules: []
    };
  },
  created() {},
  methods: {
    handleChange() {
      this.$emit('change', this.localVal);
    },
    handleInput() {
      this.$emit('update:modelValue', this.localVal);
    },
    // 失去焦点触发验证
    handleBlur() {
      // 验证输入内容是否合法
      this._isCheckValue();
    },
    // 处理错误提示信息
    _showError(msg) {
      // this.validateState = "error";
      this.localError = msg || '请输入正确的' + this.label;
    },
    _diyValidate() {},
    // 验证输入的内容
    _isCheckValue() {
      console.log(this.$props);
      let newArr = this?.rules.split('|');
      const localRules = newArr;

      this.localError = '';
      // this.validateState = "";

      for (let i in localRules) {
        let itemRuleStr = localRules[i];
        let itemRuleArr = itemRuleStr.split(':');
        let itemRuleType = itemRuleArr[0] || '';
        let itemRuleCheck = itemRuleArr[1] || '';

        switch (itemRuleType) {
          case 'require': // 是否必填
            if (this.localVal == '') {
              this._showError(this.label + '不能为空');
              return;
            }
            break;
          case 'max':
            if (this.localVal.length > itemRuleCheck) {
              this._showError(this.label + '最大长度不超过' + itemRuleCheck);
              return;
            }
            break;
          case 'min':
            if (this.localVal.length < itemRuleCheck) {
              this._showError(this.label + '最小长度不小于' + itemRuleCheck);
              return;
            }
            break;
          default:
            this._diyValidate(itemRuleType);
            break;
        }
      }
    }
  }
};
