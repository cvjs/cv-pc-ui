import cvInputText from './src/text.vue';
import cvInputMobile from './src/mobile.vue';
import cvInputEmail from './src/email.vue';
import cvInputPassword from './src/password.vue';
import cvInputNumber from './src/number.vue';
import cvInputIdcard from './src/idcard.vue';
import cvInputDigit from './src/digit.vue';
import cvTextarea from './src/textarea.vue';
import cvTextareabtn from './src/textareabtn.vue';
import cvInputBtn from './src/btn.vue';
import cvInputEdit from './src/edit.vue';

/* istanbul ignore next */
cvInputMobile.install = function (Vue) {
  Vue.component(cvInputMobile.name, cvInputMobile);
};
cvInputEmail.install = function (Vue) {
  Vue.component(cvInputEmail.name, cvInputEmail);
};
cvInputPassword.install = function (Vue) {
  Vue.component(cvInputPassword.name, cvInputPassword);
};
cvInputNumber.install = function (Vue) {
  Vue.component(cvInputNumber.name, cvInputNumber);
};
cvInputIdcard.install = function (Vue) {
  Vue.component(cvInputIdcard.name, cvInputIdcard);
};
cvInputDigit.install = function (Vue) {
  Vue.component(cvInputDigit.name, cvInputDigit);
};
cvTextarea.install = function (Vue) {
  Vue.component(cvTextarea.name, cvTextarea);
};
cvTextareabtn.install = function (Vue) {
  Vue.component(cvTextareabtn.name, cvTextareabtn);
};
cvInputBtn.install = function (Vue) {
  Vue.component(cvInputBtn.name, cvInputBtn);
};
cvInputEdit.install = function (Vue) {
  Vue.component(cvInputEdit.name, cvInputEdit);
};
export {
  cvInputText,
  cvInputMobile,
  cvInputEmail,
  cvInputPassword,
  cvInputNumber,
  cvInputIdcard,
  cvInputDigit,
  cvTextarea,
  cvTextareabtn,
  cvInputBtn,
  cvInputEdit
};
