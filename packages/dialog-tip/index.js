import cvDialogTip from './src/dialog.vue';

cvDialogTip.install = function (Vue) {
  Vue.component(cvDialogTip.name, cvDialogTip);
};
export { cvDialogTip };
