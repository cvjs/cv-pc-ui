import Vue from 'vue';
import cvDialogVue from './src/popup.vue';
const cvDialogClass = Vue.extend(cvDialogVue);

import { isVNode } from '@/utils/utils';

let instance;
let instances = [];
let seed = 1;

const cvDialog = function (options) {
  //    if (Vue.prototype.$isServer) return;
  options = options || {};
  if (typeof options === 'string') {
    // options = {
    //     message: options
    // };
  }
  let userOnClose = options.onClose;
  let id = 'cv_dialog_' + seed++;
  options.onClose = function () {
    cvDialog.close(id, userOnClose);
  };
  instance = new cvDialogClass({
    data: options
  });
  instance.id = id;
  if (isVNode(instance.message)) {
    instance.$slots.default = [instance.message];
    instance.message = null;
  }
  instance.$mount();
  document.body.appendChild(instance.$el);
  let verticalOffset = options.offset || 20;
  instances.forEach(item => {
    verticalOffset += item.$el.offsetHeight + 16;
  });
  instance.verticalOffset = verticalOffset;
  // Vue.nextTick(() => {
  //     instance.show = true
  //     // show 和弹窗组件里的show对应，用于控制显隐
  // });
  instance.show = true;
  instance.visible = true;
  // instance.$el.style.zIndex = PopupManager.nextZIndex(); 获取 zindex
  instances.push(instance);
  return instance;
};

cvDialog.close = function (id, userOnClose) {
  let len = instances.length;
  let index = -1;
  let removedHeight;
  for (let i = 0; i < len; i++) {
    if (id === instances[i].id) {
      removedHeight = instances[i].$el.offsetHeight;
      index = i;
      if (typeof userOnClose === 'function') {
        userOnClose(instances[i]);
      }
      instances.splice(i, 1);
      break;
    }
  }
  if (len <= 1 || index === -1 || index > instances.length - 1) return;
  for (let i = index; i < len - 1; i++) {
    let dom = instances[i].$el;
    dom.style['top'] =
      parseInt(dom.style['top'], 10) - removedHeight - 16 + 'px';
  }
};

cvDialog.closeAll = function () {
  for (let i = instances.length - 1; i >= 0; i--) {
    instances[i].close();
  }
};
export default cvDialog;

