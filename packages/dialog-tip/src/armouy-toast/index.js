// armouy-toast/index.js
import Vue from 'vue';
import ArmouyToast from './index.vue';

const Toast = Vue.extend(ArmouyToast);

let instance;
const toast = (options = {}) => {
  instance = new Toast({
    data: options,
  });
  instance.vm = instance.$mount();
  document.body.appendChild(instance.vm.$el);
  return instance.vm;
};

export default toast;