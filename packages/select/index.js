import cvSelectOpt from './src/option.vue';
import cvSelectBase from './src/select-base.vue';
import cvSelectTree from './src/select-tree.vue';

cvSelectOpt.install = function (Vue) {
  Vue.component(cvSelectOpt.name, cvSelectOpt);
};
cvSelectBase.install = function (Vue) {
  Vue.component(cvSelectBase.name, cvSelectBase);
};
cvSelectTree.install = function (Vue) {
  Vue.component(cvSelectTree.name, cvSelectTree);
};

export { cvSelectOpt, cvSelectBase, cvSelectTree };
