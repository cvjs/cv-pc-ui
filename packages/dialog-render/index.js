// import { useDialogRender } from './src/dialog.js';
import cvDialogRenderClass from './src/dialog.js';
import cvDrawerRenderClass from './src/drawer.js';

function cvDialogRender(WinVue, bodyComponent = null, diyOptions = {}, propsOption = {}, onFunc = {}) {
  // const dialogObj = useDialogRender(WinVue, {
  console.log('---WinVue---', WinVue, bodyComponent);
  const dialogObj = new cvDialogRenderClass(WinVue, {
    component: bodyComponent,
    diyOpts: diyOptions,
    props: propsOption,
    onFunc: onFunc,
    onOK() {
      // 能取到dialogBody的引用
      const innerObj = dialogObj.getInner();
      dialogObj.close();
    },
    onCancel() {
      // 弹框关闭
      dialogObj.close();
    }
  });
  return dialogObj;
}

function cvDrawerRender(WinVue, bodyComponent = '', diyOptions = {}, propsOption = {}, onFunc = null) {
  const dialogObj = new cvDrawerRenderClass(WinVue, {
    component: bodyComponent,
    diyOpts: { ...diyOptions },
    props: propsOption,
    onFunc: onFunc,
    onOK() {
      // 能取到dialogBody的引用
      const innerObj = dialogObj.getInner();
      dialogObj.close();
    },
    onCancel() {
      // 弹框关闭
      dialogObj.close();
    }
  });
  return dialogObj;
}

export { cvDialogRender, cvDrawerRender };
