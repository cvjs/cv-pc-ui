function bind(el, binding, vnode, oldVnode) {
  const dialogHeaderEl = el.querySelector('.el-dialog__header');
  const dragDom = el.querySelector('.el-dialog');
  dialogHeaderEl.style.cssText += ';cursor:move;';
  // dialogHeaderEl.style.cursor = 'move';
  dragDom.style.cssText += ';top:0px;';
  // 获取原有属性 ie dom元素.currentStyle 火狐谷歌 window.getComputedStyle(dom元素, null);
  const getStyle = (function () {
    if (window.document.currentStyle) {
      return (dom, attr) => dom.currentStyle[attr];
    } else {
      return (dom, attr) => getComputedStyle(dom, false)[attr]; // window.getComputedStyle(dom, null);
    }
  })();
  const moveDown = (e) => {
    // 鼠标按下，计算当前元素距离可视区的距离
    const disX = e.clientX - dialogHeaderEl.offsetLeft;
    const disY = e.clientY - dialogHeaderEl.offsetTop;
    const dragDomWidth = dragDom.offsetWidth;
    const dragDomHeight = dragDom.offsetHeight;
    // const screenWidth = document.body.clientWidth
    const screenWidth = document.documentElement.clientWidth;
    // const screenHeight = document.body.clientHeight
    const screenHeight = document.documentElement.clientHeight;
    const minDragDomLeft = dragDom.offsetLeft;
    const maxDragDomLeft = screenWidth - dragDom.offsetLeft - dragDomWidth;
    const minDragDomTop = dragDom.offsetTop;
    const maxDragDomTop = screenHeight - dragDom.offsetTop - dragDomHeight;
    // 获取到的值带px 正则匹配替换
    let styL = getStyle(dragDom, 'left');
    let styT = getStyle(dragDom, 'top');

    // 注意在ie中 第一次获取到的值为组件自带50% 移动之后赋值为px
    if (styL.includes('%')) {
      styL = +document.body.clientWidth * (+styL.replace(/\%/g, '') / 100);
      styT = +document.body.clientHeight * (+styT.replace(/\%/g, '') / 100);
    } else {
      styL = +styL.replace(/\px/g, '');
      styT = +styT.replace(/\px/g, '');
    }

    document.onmousemove = function (e) {
      // 通过事件委托，计算移动的距离
      let left = e.clientX - disX;
      let top = e.clientY - disY;
      // 边界处理
      if (-left > minDragDomLeft) {
        left = -minDragDomLeft;
      } else if (left > maxDragDomLeft) {
        left = maxDragDomLeft;
      }
      if (-top > minDragDomTop) {
        top = -minDragDomTop;
      } else if (top > maxDragDomTop) {
        top = maxDragDomTop;
      }
      // 移动当前元素
      dragDom.style.cssText += `;left:${left + styL}px;top:${top + styT}px;`;
      // emit onDrag event
      vnode.child.$emit('dragDialog');
      //dragDom.style.left = `${left + styL}px`;
      //dragDom.style.top = `${top + styT}px`;

      // 将此时的位置传出去
      // binding.value({x:e.pageX,y:e.pageY})
    };

    document.onmouseup = (e) => {
      document.onmousemove = null;
      document.onmouseup = null;
    };
  };
  dialogHeaderEl.onmousedown = moveDown;
}

// 指令1 仅实现拖拽的效果
export default {
  bind: bind
};

import Vue from 'vue';

// v-dialogDrag: 弹窗拖拽
Vue.directive('dialogDrag', {
  bind: bind
});
