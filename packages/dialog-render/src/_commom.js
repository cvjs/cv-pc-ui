function checkType(any) {
  return Object.prototype.toString.call(any).slice(8, -1);
}
/**
 * 深度拷贝，包含拷贝方法
 */
function deepClone(any) {
  if (checkType(any) === 'Object') {
    // 拷贝对象
    let o = {};
    for (let key in any) {
      o[key] = deepClone(any[key]);
    }
    return o;
  } else if (checkType(any) === 'Array') {
    // 拷贝数组
    var arr = [];
    for (let i = 0, leng = any.length; i < leng; i++) {
      arr[i] = deepClone(any[i]);
    }
    return arr;
  } else if (checkType(any) === 'Function') {
    // 拷贝函数
    return new Function('return ' + any.toString()).call(this);
  } else if (checkType(any) === 'Date') {
    // 拷贝日期
    return new Date(any.valueOf());
  } else if (checkType(any) === 'RegExp') {
    // 拷贝正则
    return new RegExp(any);
  } else if (checkType(any) === 'Map') {
    // 拷贝Map 集合
    let m = new Map();
    any.forEach((v, k) => {
      m.set(k, deepClone(v));
    });
    return m;
  } else if (checkType(any) === 'Set') {
    // 拷贝Set 集合
    let s = new Set();
    for (let val of any.values()) {
      s.add(deepClone(val));
    }
    return s;
  }
  return any;
}

function isObject(obj) {
  return Object.prototype.toString.call(obj) === '[object Object]';
}
function isArray(arr) {
  return Array.isArray(arr);
}
function deepMerge(target, ...arg) {
  return arg.reduce((acc, cur) => {
    return Object.keys(cur).reduce((subAcc, key) => {
      const srcVal = cur[key];
      if (isObject(srcVal)) {
        subAcc[key] = deepMerge(subAcc[key] ? subAcc[key] : {}, srcVal);
      } else if (isArray(srcVal)) {
        // series: []，下层数组直接赋值
        subAcc[key] = srcVal.map((item, idx) => {
          if (isObject(item)) {
            const curAccVal = subAcc[key] ? subAcc[key] : [];
            return deepMerge(curAccVal[idx] ? curAccVal[idx] : {}, item);
          } else {
            return item;
          }
        });
      } else {
        subAcc[key] = srcVal;
      }
      return subAcc;
    }, acc);
  }, target);
}

function omitKeysES6(obj, keysToRemove) {
  const { [keysToRemove[0]]: omitted1, ...rest } = obj;
  // 注意：上面的方法只能排除一个key。为了排除多个，我们需要递归或使用其他方法。
  // 这里我们使用另一种方式，通过动态地构建一个新对象：
  return Object.keys(obj).reduce((acc, key) => {
    if (!keysToRemove.includes(key)) {
      acc[key] = obj[key];
    }
    return acc;
  }, {});
  // 或者使用解构和剩余属性（对于简单的场景，仅当你知道所有要排除的键时）
  // const { [keysToRemove[0]]: _, [keysToRemove[1]]: __, ...rest } = obj;
  // 但这种方式不适合动态或未知数量的键
}
export { deepClone, deepMerge, omitKeysES6 };
