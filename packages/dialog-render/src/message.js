class ModalService {
  /**
   * 创建一个弹出框
   * @param component 组件
   * @param opt 配置
   */
  static Create(component, props, opt) {
    return new Promise((resolve, reject) => {
      const close = () => {
        render(null, container);
        document.body.removeChild(container);
        resolve(true);
      };

      const modelValue = true;

      const dialogProps = {
        modelValue: modelValue,
        title: opt?.title,
        draggable: opt?.draggable,
        closeIsDestroy: true,
        onClosed: close
      };

      const closeHandler = () => {
        // if (vNode.component?.props.modelValue) {
        //     vNode.component!.props.modelValue = false
        // }
      };

      const container = document.createElement('div');
      document.body.appendChild(container);
      var vNode = h(ElDialog, dialogProps, {
        default: () => {
          let type = typeof component;
          if (type == 'string' || type == 'number') {
            return h('div', component);
          } else {
            return h(component, {
              ...props,
              onClose: closeHandler
            });
          }
        },
        header: () => {
          if (dialogProps) {
            let type = typeof dialogProps.title;
            if (type == 'string' || type == 'number') {
              return h('span', dialogProps.title);
            }
            return h(dialogProps.title, null);
          }
        }
      });

      render(vNode, container);
      return vNode.component;
    });
  }
}
