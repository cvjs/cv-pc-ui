import { createVNode, render, h, ref } from 'vue';
import { deepClone, deepMerge, omitKeysES6 } from './_commom.js';
import { ElButton, ElDialog, ElMessageBox } from 'element-plus';

// 按钮配置项构造器
function btnBuilder(options) {
  const defaultBtn = {
    text: '按钮', // 显示文本
    clickFn: null, // 点击回调
    type: 'default', // 样式
    isHide: false, // 是否隐藏
    order: 2 // 顺序
  };
  return { ...defaultBtn, ...options };
}

export default class cvDialogRenderClass {
  constructor(WinVue, originOptions) {
    this.WinVue = WinVue;
    this.vmCompObj = null;
    this.vmComponent = null;
    this.vmApp = null;
    this.dialogObj = null;
    this.boxShow = ref(false);
    this.boxProps = ref({});

    /**
     * 可扩展el-dialog官方api所有配置项,小驼峰aaaBbbCcc
     */
    this.boxOptions = deepMerge(
      {
        closeIsConfirm: false,
        closeIsDestroy: true,
        width: '40%',
        title: '默认标题'
        // appendToBody: true
      },
      originOptions.diyOpts
    );
    // this.boxProps.value = this.boxOptions;
    this.boxProps.value = omitKeysES6(this.boxOptions, ['closeIsConfirm', 'closeIsDestroy']);

    /**
     * 主体组件
     * 弹框主体vue页面
     */
    this.bodyComponent = originOptions.component || '';
    /**
     * 主体参数
     * 传入弹框主体vue组件的参数
     */
    this.bodyOptions = deepMerge({}, originOptions.props);
    /**
     * 绑定的方法
     * 传入绑定的方法，给 内容组件调用
     */
    this.bodyFunc = deepMerge({}, originOptions.onFunc);
    // 遍历 this.bodyFunc 中的方法
    for (const methodName in this.bodyFunc) {
      if (this.bodyFunc.hasOwnProperty(methodName)) {
        // 检查方法名是否以 'on' 开头，如果不是，则添加 'on' 前缀
        if (!methodName.startsWith('on')) {
          const onPrefixedMethodName = 'on' + methodName.charAt(0).toUpperCase() + methodName.slice(1);
          // 添加带有 'on' 前缀的方法到 this.bodyFunc
          this.bodyFunc[onPrefixedMethodName] = this.bodyFunc[methodName];
          // 删除原始的方法
          delete this.bodyFunc[methodName];
        }
      }
    }
    /**
     * 默认绑定的一些方法
     */
    this.defaultFunc = {
      // 点击确定回调
      onOK: () => {
        console.log('cvDialogRenderClass default OK');
        this.close();
      },
      // 点击取消回调
      onCancel: () => {
        console.log('cvDialogRenderClass default cancel');
        this.close();
      }
    };
    // 参数合并到
    // this.options = Object.assign(this.options, defaultOptions, originOptions);
    // this.options = { ...defaultOptions, ...originOptions };

    this._initVm();
  }
  _initVm() {
    var classSelf = this;

    const vmComponent = {
      data() {
        return {};
      },
      methods: {
        clearVm() {
          // 清除vm实例
          console.log('----clearVm--');
          if (classSelf.vmApp) {
            classSelf.close();
          }
        }
      },
      mounted() {
        // 挂载到body上
        console.log('---mounted---');
        // document.body.appendChild(this);
      },
      unmounted() {
        console.log('---unmounted---');
        // 从body上移除
        // document.body.removeChild(this.$el);
      },
      render() {
        /**
         * 内容主体
         * 主体内容
         */
        const slotInner = createVNode(
          // 传递参数
          classSelf.bodyComponent,
          {
            ...classSelf.bodyOptions, // 传递参数
            ref: 'inner', // 引用
            onSetTitle(newTitle) {
              console.log('---到这里了吗？？---', newTitle);
              // 传递给【主体】修改
              classSelf.boxProps.value.title = newTitle;
            },
            onClose: () => classSelf.close(),
            ...classSelf.bodyFunc
          },
          {
            default: () => ''
          }
        );
        /**
         * 底部内容
         */
        // 这边要做处理
        // let bindFunc = deepClone(options.onFunc);
        // console.log(bindFunc, options.onFunc);
        //
        let footerDefault = {
          ok: btnBuilder({
            text: '确定',
            type: 'primary',
            order: 0
          }),
          cancel: btnBuilder({
            text: '取消',
            order: 1
          })
        };
        Object.entries(footerDefault).forEach(([key, btnOptions]) => {
          // 确定和取消默认按钮
          if (['ok', 'cancel'].includes(key)) {
            const clickFn = key === 'ok' ? classSelf.defaultFunc.onOK : classSelf.defaultFunc.onCancel;
            // 默认按钮回调优先级: footerDefault 配置的clickFn > options配置的onOK和onCancel
            btnOptions.clickFn = btnOptions.clickFn || clickFn;
          } else {
            // 新增按钮
            // 完善配置
            footerDefault[key] = btnBuilder(btnOptions);
          }
        });

        // 控制按钮显示隐藏
        const showBtns = Object.values(footerDefault).filter((btn) => !btn.isHide);
        // 控制按钮顺序 // lodash/orderBy(showBtns, ['order'], ['desc'])
        const sortBtns = showBtns;

        // 底部按钮 jsx 写法
        const slotFooter = createVNode(
          'div',
          {},
          sortBtns.map((btn) => createVNode(ElButton, { type: btn.type, onClick: btn.clickFn }, btn.text))
        );

        /**
         * 弹框右上角关闭按钮回调
         */
        // const beforeClose = footerDefault['cancel'].clickFn;
        const defBeforeClose = function (done) {
          // done();
          classSelf.close();
        };
        const selfBeforeClose =
          typeof classSelf.boxOptions['beforeClose'] == 'function' ? classSelf.boxOptions['beforeClose'] : defBeforeClose;

        // 弹框主体
        const cvRenderMain = createVNode(
          ElDialog,
          {
            // 配置项
            ...classSelf.boxProps.value,
            // appendToBody: true,
            // ref: 'cvElDialog',
            modelValue: classSelf.boxShow.value,
            beforeClose: (done) => {
              if (classSelf.boxOptions.closeIsConfirm) {
                ElMessageBox.confirm('确认关闭？', '提示')
                  .then((_) => {
                    selfBeforeClose(defBeforeClose);
                  })
                  .catch((_) => {
                    defBeforeClose();
                  });
              } else {
                selfBeforeClose(defBeforeClose);
              }
            },
            // **** 看这里，visible置为false后，el-dialog销毁后回调 *****
            onClosed2(val) {
              console.loeg('===----- on closed2 ', val);
              classSelf.close();
            },
            onClosed: classSelf.close
          },
          // 弹框内容：弹框主体和按钮
          // [slotInner, slotFooter]
          {
            default: () => slotInner
            // footer: () => slotFooter
          }
        );
        return cvRenderMain;
      }
    };
    this.vmComponent = vmComponent;
    /**
     * 创建一个div容器
     * 手动指定要挂载的目标元素
     */
    // const vmTargetElement = document.getElementsByTagName('body')[0];
    const vmTargetElement = document.createElement('div');
    document.body.appendChild(vmTargetElement);
    this.vmTargetElement = vmTargetElement;
    /**
     * 创建一个dom虚拟节点
     */
    const vnode = createVNode(vmComponent);
    // 这样绑定，才能使用其他注册的组件
    vnode.appContext = this.WinVue._context;
    // 渲染虚拟节点到 DOM 中，将弹窗组件挂载到目标元素上
    render(vnode, this.vmTargetElement);
    // 赋予
    this.vmApp = vnode;
  }
  // 封装API
  // 关闭弹框
  close() {
    if (this.boxShow) {
      this.boxShow.value = false;
    }
    // if (this.boxOptions['closeIsDestroy']) {
    console.log('销毁弹窗');
    if (this.vmTargetElement) {
      render(null, this.vmTargetElement);
      document.body.removeChild(this.vmTargetElement);
      this.vmApp = null;
    }
    // }
  }
  // 显示弹框
  show() {
    this.boxShow.value = true;
  }
  // 获取弹框主体实例，可访问实例上的方法
  getInner() {
    return this.vmCompObj.$refs.inner;
  }
}
