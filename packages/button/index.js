import cvBtnBase from './src/base.vue';
import cvBtnText from './src/text.vue';

/* istanbul ignore next */
cvBtnBase.install = function (Vue) {
  Vue.component(cvBtnBase.name, cvBtnBase);
};
cvBtnText.install = function (Vue) {
  Vue.component(cvBtnText.name, cvBtnText);
};
export { cvBtnBase, cvBtnText };
