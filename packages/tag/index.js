import cvTagBase from './src/base.vue';

cvTagBase.install = function (Vue) {
  Vue.component(cvTagBase.name, cvTagBase);
};

export { cvTagBase };
