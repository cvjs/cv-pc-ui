import cvCascaderBase from './src/base.vue';

cvCascaderBase.install = function (Vue) {
  Vue.component(cvCascaderBase.name, cvCascaderBase);
};

export { cvCascaderBase };
