import cvRadioOpt from './src/option-base.vue';
import cvRadioBtn from './src/option-button.vue';
import cvRadioGroup from './src/group.vue';

cvRadioOpt.install = function (Vue) {
  Vue.component(cvRadioOpt.name, cvRadioOpt);
};
cvRadioBtn.install = function (Vue) {
  Vue.component(cvRadioBtn.name, cvRadioBtn);
};

cvRadioGroup.install = function (Vue) {
  Vue.component(cvRadioGroup.name, cvRadioGroup);
};

export { cvRadioOpt, cvRadioBtn, cvRadioGroup };
