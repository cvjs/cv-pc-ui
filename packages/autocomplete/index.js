import cvAutocompeteBase from './src/base.vue';
import cvAutocompeteLoad from './src/load.vue';

cvAutocompeteBase.install = function (Vue) {
  Vue.component(cvAutocompeteBase.name, cvAutocompeteBase);
};
cvAutocompeteLoad.install = function (Vue) {
  Vue.component(cvAutocompeteLoad.name, cvAutocompeteLoad);
};
export { cvAutocompeteBase, cvAutocompeteLoad };
