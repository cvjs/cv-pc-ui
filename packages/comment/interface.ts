import { EmojiApi } from '../emoji/interface'
import { InjectionKey } from 'vue'

export interface Emoji {
  [key: string]: string
}
export interface EmojiApi {
  faceList: string[]
  emojiList: Emoji[]
  allEmoji: Emoji
}

export const InjectionEmojiApi: InjectionKey<EmojiApi> = Symbol()

export interface CommentApi {
  id: string | number
  parentId: string | number | null
  uid: string | number
  address: string
  content: string
  likes: number
  contentImg?: string
  createTime: string
  user: CommentUserApi
  reply?: ReplyApi | null
}

export interface ReplyApi {
  total: number
  list :any []
}

export interface CommentUserApi {
  username: string
  avatar: string
  level: number
  homeLink: string
}

export interface UserApi {
  id: string | number
  username: string
  avatar: string
  likeIds: string[] | number[]
}

export interface ConfigApi {
  user: UserApi
  emoji: EmojiApi
  comments []
  total: number
  showSize?: number
  replyShowSize?: number
}

export interface SubmitParamApi {
  content: string
  parentId: string | null
  files?: any[]
  reply? 
  finish: (comment ) => void
}

export interface ReplyPageParamApi {
  parentId: string
  pageNum: number
  pageSize: number
  finish: (reply: ReplyApi) => void
}