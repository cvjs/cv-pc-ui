import cvCommentBase from './src/comment.vue';
import cvCommentScroll from './src/comment-scroll.vue';
import cvCommentNav from './src/comment-nav.vue';

cvCommentBase.install = function (Vue) {
  Vue.component(cvCommentBase.name, cvCommentBase);
};
cvCommentScroll.install = function (Vue) {
  Vue.component(cvCommentScroll.name, cvCommentScroll);
};
cvCommentNav.install = function (Vue) {
  Vue.component(cvCommentNav.name, cvCommentNav);
};
export { cvCommentBase, cvCommentScroll, cvCommentNav };
