import CommentIconClear from './icon-clear.vue';
import CommentIconEmoji from './icon-emoji.vue';
import CommentIconHot from './icon-hot.vue';
import CommentIconImage from './icon-image.vue';
import CommentIconItemopt from './icon-itemopt.vue';
import CommentIconLikeOn from './icon-like-on.vue';
import CommentIconLikeOff from './icon-like-off.vue';
import CommentIconLook from './icon-look.vue';
import CommentIconNew from './icon-new.vue';
import CommentIconReply from './icon-reply.vue';

export {
  CommentIconClear,
  CommentIconEmoji,
  CommentIconHot,
  CommentIconImage,
  CommentIconItemopt,
  CommentIconLikeOn,
  CommentIconLikeOff,
  CommentIconLook,
  CommentIconNew,
  CommentIconReply
};
