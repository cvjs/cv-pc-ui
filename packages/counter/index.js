import counterVue from './counter.vue';

counterVue.install = function (Vue) {
  Vue.component(counterVue.name, counterVue);
};
export { counterVue };
