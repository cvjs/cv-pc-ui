import cvInputCounter from './src/base.vue';

cvInputCounter.install = function (Vue) {
  Vue.component(cvInputCounter.name, cvInputCounter);
};

export { cvInputCounter };
