import cvBadge from './src/badge.vue';

cvBadge.install = function (Vue) {
  Vue.component(cvBadge.name, cvBadge);
};

export { cvBadge };
