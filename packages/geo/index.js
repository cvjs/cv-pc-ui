import cvGeoCity from './src/geo-city.vue';
import cvGeoCity2 from './src/geo-city2.vue';

cvGeoCity.install = function (Vue) {
  Vue.component(cvGeoCity.name, cvGeoCity);
};
cvGeoCity2.install = function (Vue) {
  Vue.component(cvGeoCity2.name, cvGeoCity2);
};

export { cvGeoCity, cvGeoCity2 };
