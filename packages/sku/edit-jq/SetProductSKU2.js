﻿var AllModuleInfo;
var ProductModuleInfo;
var AllProductModuleInfo;
var skudata;
/* 加载所有规格 */
InitSpec(4);
InitSpec('4', '168');
var $this = {};
function Save() {
	var query = $(".valid").GetPostData();
	var productid = '156';
	query.ProductID = productid;
	query.ProductSpecList = JSON.stringify(GetSkuParam(productid));
	var url = "/Product/SaveProductSKU";
	$.post(url, query, function(result) {
		$.ligerDialog.warn(result.msg);
		if (result.state) {
			setTimeout(function() {
				window.parent.grid.loadData(true);
				dialog.close();
			}, 1500);
		}
	});
}

function InitSpec(ProductTypeID, productId) {
	$("#td_Property").empty();
	var htmlstr = "";
	$.post("test.json?ProductTypeID=" + ProductTypeID, null, function(result) {
		if (result) {
			AllModuleInfo = result.Rows;
			var tabHtml = new Array();
			var list = result.Rows;
			$.each(result.Rows, function(i, op) {
				tabHtml.push("<tr><td class='title' align='left'>" + op.Name + ":</td><td>" + Box(op) + "</td></tr>");
			});
			$("#td_Property").html(tabHtml.join(""));
			SpecChange();
		}
		if (productId != undefined) {
			$.post("GetProductSpecInfo.json?id=" + productId, null, function(result) {
				if (result) {
					ProductModuleInfo = result.Rows;
					AllProductModuleInfo = result.allRows;
					InitSku();
				}
			});
		}
	});
}
function InitSku() {
	if (ProductModuleInfo != undefined) {
		$.each(ProductModuleInfo, function(i, op) {
			$.each($("#td_Property tr td input:checkbox"), function(j, cb) {
				if (op.SpecDetailName == cb.value && op.SpecID == $(cb).attr('fid')) {
					$(cb).click(); // .attr('checked', true);
					$(cb).change();
				}
			});
		});
		var trs = $("#td_sku tr");
		for (var k = 1; k < trs.length; k++) {
			var itemVal = $(trs[k]).find(":input[name='SkuDetail']").val() + ";";
			for (var i = 0; i < AllProductModuleInfo.length; i++) {
				var bol = true;
				var minSpec = AllProductModuleInfo[i].ProductSpecDetailList;
				for (var n = 0; n < minSpec.length; n++) {
					var specId = minSpec[n].SpecID;
					specId = specId == "" ? "0" : specId;
					// [0]规格项名称^[1]规格项id^[2]规格名称^[3]规格id
					var str = minSpec[n].SpecDetailName + "^" + minSpec[n].SpecDetailID + "^" + minSpec[n].SpecName + "^" + specId + ";";
					if (itemVal.indexOf(str) == -1) {
						bol = false;
						break;
					}
				}
				// 赋值
				if (bol) {
					$(trs[k]).find(":input[name='ProductSpecID']").val(AllProductModuleInfo[i].ID);
					$(trs[k]).find(":input[name='ProductCode']").val(AllProductModuleInfo[i].ProductCode);
					$(trs[k]).find(":input[name='CostPrice']").val(AllProductModuleInfo[i].CostPrice);
					$(trs[k]).find(":input[name='RetailPrice']").val(AllProductModuleInfo[i].RetailPrice);
					$(trs[k]).find(":input[name='StockCount']").val(AllProductModuleInfo[i].StockCount);

					$(trs[k]).find("img[name='Image']").attr("src", AllProductModuleInfo[i].Image);
					// $(trs[k]).find("img[name='Image']").attr("id",
					// "proSpecImg_" + AllProductModuleInfo[i].ID);
					// $(trs[k]).find("img[name='Image']").attr("data_productSpecId",
					// AllProductModuleInfo[i].ID);
					break;
				}
			}
		}
	}

}

/* 加载规格项 */
function Box(type) {
	var res = Array();
	$.each(type.SpecDetailList, function(i, op) {
		res.push("&nbsp;&nbsp;<input type='checkbox' value='" + op.Name + "' fid='" + op.SpecID + "' />");
		res.push("&nbsp;&nbsp;<input type='text' value='" + op.Name + "' readonly='readonly' class='td_input textbox' />");
		if ((i + 1) % 9 == 0) {
			res.push("</br>");
		}
	});
	return res.join("");
}
/* 点击规格项 */
function SpecChange() {

	$("#td_Property :checkbox").change(function() {
		console.log('SpecChange  - 2');

		if ($(this).attr("checked")) {
			$(this).next("input").addClass("SelectInput");
		} else {
			$(this).next("input").removeClass("SelectInput");
		}
		store_Sku();
		rload_sku();
		reloadData_Sku();
	});
}
function store_Sku() {
	$this.Cache = [];
	$("#td_sku tr:gt(0)").each(function(i, item) {
		$this.Cache[$(item).find("input:hidden").val()] = $(item).clone();
		$this.Cache.length = $this.Cache.length + 1;
	});
}
/* 生存sku的table */
function rload_sku() {
	skudata = new Array();
	$.each(AllModuleInfo, function(i, op) {
		var list = $("#td_Property :checkbox:checked[fid='" + op.ID + "']");
		if (list.length > 0) {
			var p = $.extend({}, op);
			p.children = new Array();
			$.each(op.SpecDetailList, function(i, item) {
				$.each(list, function(i, ckitem) {
					if (ckitem.value == item.Name) {
						p.children.push(item);
					}
				});
			});
			skudata.push(p);
		}
	});
	$this.tabData = [];
	$this.tabTitle = [];
	$.each(skudata, function(i, op) {
		var arr = [];
		$.each(op.children, function(j, item) {
			arr.push(item.Name + "^" + item.ID + "^" + op.Name + "^" + op.ID + ";"); // [0]规格项名称^[1]规格项id^[2]规格名称^[3]规格id
		});
		$this.tabData.push(arr);
		$this.tabTitle.push(op.Name + "^" + arr.length);
	});
	if ($this.tabData.length > 0) {
		$this.tabData = oderbySku($this.tabData);
		$this.tabTitle = oderbyTitle($this.tabTitle);
		$this.res = doExchange($this.tabData);
		CreateProSku($this.res);
		for (var i = 1; i <= (skudata.length); i++) {
			MergeTableRow("#td_sku", i);
		}
	} else {
		$("#td_sku").empty();
		$("#td_skuImg").empty();
	}
}
function reloadData_Sku() {
	$("#td_sku tr:gt(0)").each(function(i, item) {
		var key = $(item).find("input:hidden").val();
		var curTr = getCurTrInCache(key);
		if (curTr) {
			$(item).find(":input[name='ProductSpecID']").val($(curTr).find(":input[name='ProductSpecID']").val());
			$(item).find(":input[name='ProductCode']").val($(curTr).find(":input[name='ProductCode']").val());
			$(item).find(":input[name='CostPrice']").val($(curTr).find(":input[name='CostPrice']").val());
			$(item).find(":input[name='RetailPrice']").val($(curTr).find(":input[name='RetailPrice']").val());
			$(item).find(":input[name='StockCount']").val($(curTr).find(":input[name='StockCount']").val());
			$(item).find("img[name='Image']").attr("src", $(curTr).find("img[name='Image']").attr("src"));
		}
	});
}
// 创建SKU
function CreateProSku(items) {
	$("#td_sku").empty();
	var html = new Array();
	html.push("<tr>");
	$.each($this.tabTitle, function(i, op) {
		html.push("<td class='td_item'>" + op.split('^')[0] + "</td>");
	});
	// html.push("<td class='sku_title'><span style='color:
	// Red;'>*</span>产品编号</td><td class='sku_title'><span style='color:
	// Red;'>*</span>厂家价</td><td class='sku_title'><span style='color:
	// Red;'>*</span>销售价</td><td class='sku_title'><span style='color:
	// Red;'>*</span>库存</td></tr>");
	html.push("<td class='sku_title'><span style='color: Red;'>*</span>库存</td>");
	html.push("<td class='sku_title'><span style='color: Red;'>*</span>图片</td>");
	html.push("</tr>");
	$.each(items, function(i, op) {
		var str = op.substring(0, op.length - 1);
		var tr = str.split(';');
		html.push("<tr>");
		$.each(tr, function(n, td) {
			var tddata = td.split('^'); // [0]规格项名称^[1]规格项id^[2]规格名称^[3]规格id
			html.push("<td>" + tddata[0] + "</td>");
		});
		html.push(getSupItem(str, i));
		html.push("</tr>");
	});
	$("#td_sku").html(html.join(""));
}
function getSupItem(item, i) {
	// var e = "[`~!@#$^&*()=|{}':;',\\[\\].<>/?~！@#￥……&*（）——|{}【】‘；：”“'。，、？]";
	var e = "";
	// var str = '<td><input type="hidden" name="SkuDetail" value="' + item + '"
	// /><input type="hidden" name="ProductSpecID" value="0"><input type="text"
	// value="001" name="ProductCode" class="td_input_sku textbox"
	// maxlength="25" /></td>';
	// var str = '<td><input type="hidden" name="SkuDetail" value="' + item + '"
	// /><input type="hidden" name="ProductSpecID" value="0"><input type="text"
	// value="001" name="ProductCode" class="td_input_sku textbox"
	// maxlength="25" /></td>';
	// str += '<td><input type="text" name="CostPrice" value="0" class="td_input
	// textbox"
	// onkeyup="this.value=this.value.replace(/[^0-9.]/g,\'\');if(this.value.replace(/.\d*/,\'\').length>7){this.value=this.value.substring(0,7);}"
	// /></td>';
	// str += '<td><input type="text" name="RetailPrice" value="0"
	// class="td_input textbox"
	// onkeyup="this.value=this.value.replace(/[^0-9.]/g,\'\');if(this.value.replace(/.\d*/,\'\').length>7){this.value=this.value.substring(0,7);}"
	// /></td>';
	var idNum = Number(i + 1);
	// alert(idNum);
	var str = '<td>' + '<input type="hidden" name="SkuDetail" value="' + item + '" />' + '<input type="hidden" name="ProductSpecID" value="0">'
			+ '<input type="text" name="StockCount" value="1000" class="td_input textbox" style="width:50px" onkeyup="this.value=this.value.replace(/[^0-9]/g,\'\');if(this.value.length>8)this.value=this.value.substring(0,8);" />' + '</td>';
	str += '<td>' +
	// '<input value="' + idNum + '" />' +
	'<img id="img_' + idNum + '" data_productSpecId="" name="Image" src="" style="width:30px;height:30px;vertical-align: middle;" onclick="selectImg(this)" />' +
	// '<input type="text" name="Image" style="width:100px;"/>' +
	// '<input type="file" style="width:180px;"/>' +
	'</td>';
	return str;
}

function getCurTrInCache(key) {
	var keyArr = key.split(';');
	var curTr;
	for ( var property in $this.Cache) {

		var cacheKeyArr = property.split(";");
		var isTarget = false;
		$(keyArr).each(function(j, min) {
			if ($.inArray(min, cacheKeyArr) == -1) {
				isTarget = false;
				return false;
			}
			isTarget = true;
		});
		if (isTarget == true) {
			curTr = $this.Cache[property];
			break;
		}
	}
	;
	return curTr;
}
// sku排序
function oderbySku(a) {
	for (var i = 0; i < a.length; i++) {
		for (var j = i + 1; j < a.length; j++) {
			if (a[i].length > a[j].length) {
				var n;
				n = a[j];
				a[j] = a[i];
				a[i] = n;
			}
		}
	}
	return a;
}
// 标题排序
function oderbyTitle(a) {
	for (var i = 0; i < a.length; i++) {
		for (var j = i + 1; j < a.length; j++) {
			if (parseInt(a[i].split('^')[1]) > parseInt(a[j].split('^')[1])) {
				var n;
				n = a[j];
				a[j] = a[i];
				a[i] = n;
			}
		}
	}
	return a;
}

// sku生成
function doExchange(doubleArrays) {
	var len = doubleArrays.length;
	if (len >= 2) {
		var len1 = doubleArrays[0].length;
		var len2 = doubleArrays[1].length;
		var newlen = len1 * len2;
		var temp = new Array(newlen);
		var index = 0;
		for (var i = 0; i < len1; i++) {
			for (var j = 0; j < len2; j++) {
				temp[index] = doubleArrays[0][i] + doubleArrays[1][j];
				index++;
			}
		}
		var newArray = new Array(len - 1);
		for (var i = 2; i < len; i++) {
			newArray[i - 1] = doubleArrays[i];
		}
		newArray[0] = temp;
		return doExchange(newArray);
	} else {
		return doubleArrays[0];
	}
}
// 合并表格
function MergeTableRow(_w_table_id, _w_table_colnum) {
	_w_table_firsttd = "";
	_w_table_currenttd = "";
	_w_table_SpanNum = 0;
	_w_table_Obj = $(_w_table_id + " tr td:nth-child(" + _w_table_colnum + ")");
	_w_table_Obj.each(function(i) {
		if (i == 0) {
			_w_table_firsttd = $(this);
			_w_table_SpanNum = 1;
		} else {
			_w_table_currenttd = $(this);
			var isSameGroup = mergetableCell_sameGroup(_w_table_firsttd, _w_table_currenttd);

			if (_w_table_firsttd.text() == _w_table_currenttd.text() && (_w_table_colnum == 1 || isSameGroup == true)) {
				_w_table_SpanNum++;
				_w_table_currenttd.hide(); // remove();
				_w_table_firsttd.attr("rowSpan", _w_table_SpanNum);
			} else {
				_w_table_firsttd = $(this);
				_w_table_SpanNum = 1;
			}
		}
	});
};
function mergetableCell_sameGroup(_w_table_firsttd, _w_table_currenttd) {
	var isSameGroup = true;
	var firstPrevAll = $(_w_table_firsttd).prevAll();
	var curPrevAll = $(_w_table_currenttd).prevAll();
	firstPrevAll.each(function(i, item) {
		if ($(curPrevAll[i]).text() != $(item).text()) {
			isSameGroup = false;
			return false;
		}
	});
	return isSameGroup;
};

/* 获取 */
function GetSkuParam(productid) {
	var dataStr = new Array();
	$("#td_sku tr").each(function(index, row) {
		if (index > 0) {
			var skuDetail = $(row).find("input[name='SkuDetail']").val();
			var productSpecID = $(row).find("input[name='ProductSpecID']").val();
			var productCode = $(row).find("input[name='ProductCode']").val();
			var costPrice = $(row).find("input[name='CostPrice']").val();
			var retailPrice = $(row).find("input[name='RetailPrice']").val();
			var stockCount = $(row).find("input[name='StockCount']").val();
			var image = $(row).find("img[name='Image']").attr("src");
			if (productSpecID == "" || productSpecID == undefined) {
				productSpecID = 0;
			}
			dataStr.push({
				SkuDetail : skuDetail,
				ID : productSpecID,
				ProductCode : productCode,
				CostPrice : costPrice,
				RetailPrice : retailPrice,
				StockCount : stockCount,
				ProductID : productid,
				Image : image
			});
		}
	});
	return dataStr;
}