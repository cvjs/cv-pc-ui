// TODO 自定义样式，可以通过 link 来请求,返回
var ctoSkuColorCatData = {
	"attr_catid" : 1,
	"public_name" : "颜色",
	"attr_data" : [ {
		"attr_id" : "10002",
		"tabs_name" : "白色系",
		"attr_name" : "颜色分类",
		"styleHtml" : "background:rgb(255,255,255);",
		"classHtml" : "",
		"attr_catid" : "10001",
		"cat_name" : "其他颜色",
		"attr_values_arr" : [ {
			"val_id" : "1",
			"attr_id" : "10002",
			"val_name" : "乳白色",
			"color" : "fffbf0",
			"style" : "background: rgb(255, 251, 240);"
		}, {
			"val_id" : "2",
			"attr_id" : "10002",
			"val_name" : "其他颜色",
			"color" : "",
			"styleHtml" : ""
		}, {
			"val_id" : "3",
			"attr_id" : "10002",
			"val_name" : "白色",
			"color" : "",
			"styleHtml" : ""
		}, {
			"val_id" : "4",
			"attr_id" : "10002",
			"val_name" : "米白色",
			"color" : "eedeb0",
			"styleHtml" : "background: rgb(238, 222, 176);"
		}, ]
	}, {
		"attr_id" : "10002",
		"tabs_name" : "灰色系",
		"attr_name" : "颜色分类",
		"styleHtml" : "background:rgb(128,128,128);",
		"classHtml" : "",
		"attr_catid" : "10001",
		"cat_name" : "其他颜色",
		"attr_values_arr" : [ {
			"val_id" : "5",
			"attr_id" : "10002",
			"val_name" : "浅灰色",
			"color" : "e4e4e4",
			"styleHtml" : "background: rgb(228, 228, 228);"
		}, {
			"val_id" : "6",
			"attr_id" : "10002",
			"val_name" : "深灰色",
			"color" : "666666",
			"styleHtml" : "background: rgb(102, 102, 102);"
		}, {
			"val_id" : "7",
			"attr_id" : "10002",
			"val_name" : "灰色",
			"color" : "808080",
			"styleHtml" : "background: rgb(128, 128, 128);"
		}, {
			"val_id" : "8",
			"attr_id" : "10002",
			"val_name" : "银色",
			"color" : "c0c0c0",
			"styleHtml" : "background: rgb(192, 192, 192);"
		}, ]
	}, {
		"attr_id" : "10002",
		"tabs_name" : "黑色系",
		"attr_name" : "颜色分类",
		"styleHtml" : "background:rgb(0,0,0);",
		"classHtml" : "",
		"attr_catid" : "10001",
		"cat_name" : "其他颜色",
		"attr_values_arr" : [ {
			"val_id" : "9",
			"attr_id" : "10002",
			"val_name" : "黑色",
			"color" : "000000",
			"styleHtml" : "background: rgb(0, 0, 0);"
		} ]
	}, {
		"attr_id" : "10002",
		"tabs_name" : "红色系",
		"attr_name" : "颜色分类",
		"styleHtml" : "background:rgb(255,0,0);",
		"classHtml" : "",
		"attr_catid" : "10001",
		"cat_name" : "其他颜色",
		"attr_values_arr" : [ {
			"val_id" : "10",
			"attr_id" : "10002",
			"val_name" : "桔红色",
			"color" : "ff7500",
			"styleHtml" : "background: rgb(255, 117, 0);"
		}, {
			"val_id" : "11",
			"attr_id" : "10002",
			"val_name" : "玫红色",
			"color" : "df1b76",
			"styleHtml" : "background: rgb(223, 27, 118);"
		}, {
			"val_id" : "12",
			"attr_id" : "10002",
			"val_name" : "粉红色",
			"color" : "ffb6c1",
			"styleHtml" : "background: rgb(255, 182, 193);"
		}, {
			"val_id" : "13",
			"attr_id" : "10002",
			"val_name" : "红色",
			"color" : "ff0000",
			"styleHtml" : "background: rgb(255, 0, 0);"
		}, {
			"val_id" : "14",
			"attr_id" : "10002",
			"val_name" : "藕色",
			"color" : "eed0d8",
			"styleHtml" : "background: rgb(238, 208, 216);"
		}, {
			"val_id" : "15",
			"attr_id" : "10002",
			"val_name" : "西瓜红",
			"color" : "f05654",
			"styleHtml" : "background: rgb(240, 86, 84);"
		}, {
			"val_id" : "16",
			"attr_id" : "10002",
			"val_name" : "酒红色",
			"color" : "990000",
			"styleHtml" : "background: rgb(153, 0, 0);"
		} ]
	}, {
		"attr_id" : "10002",
		"tabs_name" : "黄色系",
		"attr_name" : "颜色分类",
		"styleHtml" : "background:rgb(255,255,0);",
		"classHtml" : "",
		"attr_catid" : "10001",
		"cat_name" : "其他颜色",
		"attr_values_arr" : [ {
			"val_id" : "17",
			"attr_id" : "10002",
			"val_name" : "卡其色",
			"color" : "c3b091",
			"styleHtml" : "background: rgb(195, 176, 145);"
		}, {
			"val_id" : "18",
			"attr_id" : "10002",
			"val_name" : "姜黄色",
			"color" : "ffc773",
			"styleHtml" : "background: rgb(255, 199, 115);"
		}, {
			"val_id" : "19",
			"attr_id" : "10002",
			"val_name" : "明黄色",
			"color" : "ffff01",
			"styleHtml" : "background: rgb(255, 255, 1);"
		}, {
			"val_id" : "20",
			"attr_id" : "10002",
			"val_name" : "杏色",
			"color" : "f7eed6",
			"styleHtml" : "background: rgb(247, 238, 214);"
		}, {
			"val_id" : "21",
			"attr_id" : "10002",
			"val_name" : "柠檬黄",
			"color" : "ffec43",
			"styleHtml" : "background: rgb(255, 236, 67);"
		}, {
			"val_id" : "22",
			"attr_id" : "10002",
			"val_name" : "桔色",
			"color" : "ffa500",
			"styleHtml" : "background: rgb(255, 165, 0);"
		}, {
			"val_id" : "23",
			"attr_id" : "10002",
			"val_name" : "浅黄色",
			"color" : "faff72",
			"styleHtml" : "background: rgb(250, 255, 114);"
		}, {
			"val_id" : "24",
			"attr_id" : "10002",
			"val_name" : "荧光黄",
			"color" : "eaff56",
			"styleHtml" : "background: rgb(234, 255, 86);"
		}, {
			"val_id" : "25",
			"attr_id" : "10002",
			"val_name" : "金色",
			"color" : "ffd700",
			"styleHtml" : "background: rgb(255, 215, 0);"
		}, {
			"val_id" : "26",
			"attr_id" : "10002",
			"val_name" : "香槟色",
			"color" : "",
			"styleHtml" : ""
		}, {
			"val_id" : "27",
			"attr_id" : "10002",
			"val_name" : "黄色",
			"color" : "ffff00",
			"styleHtml" : "background: rgb(255, 255, 0);"
		} ]
	}, {
		"attr_id" : "10002",
		"tabs_name" : "绿色系",
		"attr_name" : "颜色分类",
		"styleHtml" : "background:rgb(0,128,0);",
		"classHtml" : "",
		"attr_catid" : "10001",
		"cat_name" : "其他颜色",
		"attr_values_arr" : [ {
			"val_id" : "28",
			"attr_id" : "10002",
			"val_name" : "军绿色",
			"color" : "5d762a",
			"styleHtml" : "background: rgb(93, 118, 42);"
		}, {
			"val_id" : "29",
			"attr_id" : "10002",
			"val_name" : "墨绿色",
			"color" : "057748",
			"styleHtml" : ""
		}, {
			"val_id" : "30",
			"attr_id" : "10002",
			"val_name" : "浅绿色",
			"color" : "98fb98",
			"styleHtml" : "background: rgb(152, 251, 152);"
		}, {
			"val_id" : "31",
			"attr_id" : "10002",
			"val_name" : "绿色",
			"color" : "008000",
			"styleHtml" : "background: rgb(0, 128, 0);"
		}, {
			"val_id" : "32",
			"attr_id" : "10002",
			"val_name" : "翠绿色",
			"color" : "0aa344",
			"styleHtml" : "background: rgb(10, 163, 68);"
		}, {
			"val_id" : "33",
			"attr_id" : "10002",
			"val_name" : "荧光绿",
			"color" : "ffa500",
			"styleHtml" : "background: rgb(35, 250, 7);"
		}, {
			"val_id" : "34",
			"attr_id" : "10002",
			"val_name" : "青色",
			"color" : "00e09e",
			"styleHtml" : "background: rgb(0, 224, 158);"
		}, ]
	}, {
		"attr_id" : "10002",
		"tabs_name" : "蓝色系",
		"attr_name" : "颜色分类",
		"styleHtml" : "background:rgb(0,0,254);",
		"class" : "",
		"attr_catid" : "10001",
		"cat_name" : "其他颜色",
		"attr_values_arr" : [ {
			"val_id" : "35",
			"attr_id" : "10002",
			"val_name" : "天蓝色",
			"color" : "44cef6",
			"styleHtml" : "background: rgb(68, 206, 246);"
		}, {
			"val_id" : "36",
			"attr_id" : "10002",
			"val_name" : "孔雀蓝",
			"color" : "00a4c5",
			"styleHtml" : "background: rgb(0, 164, 197);"
		}, {
			"val_id" : "37",
			"attr_id" : "10002",
			"val_name" : "宝蓝色",
			"color" : "4b5cc4",
			"styleHtml" : "background: rgb(75, 92, 196);"
		}, {
			"val_id" : "38",
			"attr_id" : "10002",
			"val_name" : "浅蓝色",
			"color" : "d2f0f4",
			"styleHtml" : "background: rgb(210, 240, 244);"
		}, {
			"val_id" : "39",
			"attr_id" : "10002",
			"val_name" : "深蓝色",
			"color" : "041690",
			"styleHtml" : "background: rgb(4, 22, 144);"
		}, {
			"val_id" : "40",
			"attr_id" : "10002",
			"val_name" : "湖蓝色",
			"color" : "30dff3",
			"styleHtml" : "background: rgb(48, 223, 243);"
		}, {
			"val_id" : "41",
			"attr_id" : "10002",
			"val_name" : "蓝色",
			"color" : "0000fe",
			"styleHtml" : "background: rgb(0, 0, 254);"
		}, {
			"val_id" : "42",
			"attr_id" : "10002",
			"val_name" : "藏青色",
			"color" : "2e4e7e",
			"styleHtml" : "background: rgb(46, 78, 126);"
		}, ]
	}, {
		"attr_id" : "10002",
		"tabs_name" : "紫色系",
		"attr_name" : "颜色分类",
		"styleHtml" : "background:rgb(128,0,128);",
		"classHtml" : "",
		"attr_catid" : "10001",
		"cat_name" : "其他颜色",
		"attr_values_arr" : [ {
			"val_id" : "43",
			"attr_id" : "10002",
			"val_name" : "浅紫色",
			"color" : "ede0e6",
			"styleHtml" : "background: rgb(237, 224, 230);"
		}, {
			"val_id" : "44",
			"attr_id" : "10002",
			"val_name" : "深紫色",
			"color" : "430653",
			"styleHtml" : "background: rgb(67, 6, 83);"
		}, {
			"val_id" : "45",
			"attr_id" : "10002",
			"val_name" : "紫红色",
			"color" : "8b0062",
			"styleHtml" : "background: rgb(139, 0, 98);"
		}, {
			"val_id" : "46",
			"attr_id" : "10002",
			"val_name" : "紫罗兰",
			"color" : "b7ace4",
			"styleHtml" : "background: rgb(183, 172, 228);"
		}, {
			"val_id" : "47",
			"attr_id" : "10002",
			"val_name" : "紫色",
			"color" : "800080",
			"styleHtml" : "background: rgb(128, 0, 128);"
		} ]
	}, {
		"attr_id" : "10002",
		"tabs_name" : "棕色系",
		"attr_name" : "颜色分类",
		"styleHtml" : "background:rgb(124,75,0)",
		"classHtml" : "",
		"attr_catid" : "10001",
		"cat_name" : "其他颜色",
		"attr_values_arr" : [ {
			"val_id" : "48",
			"attr_id" : "10002",
			"val_name" : "咖啡色",
			"color" : "603912",
			"styleHtml" : "background: rgb(96, 57, 18);"
		}, {
			"val_id" : "49",
			"attr_id" : "10002",
			"val_name" : "巧克力色",
			"color" : "d2691e",
			"styleHtml" : "background: rgb(210, 105, 30);"
		}, {
			"val_id" : "50",
			"attr_id" : "10002",
			"val_name" : "栗色",
			"color" : "60281e",
			"styleHtml" : "background: rgb(96, 40, 30);"
		}, {
			"val_id" : "51",
			"attr_id" : "10002",
			"val_name" : "浅棕色",
			"color" : "b35c44",
			"styleHtml" : "background: rgb(179, 92, 68);"
		}, {
			"val_id" : "52",
			"attr_id" : "10002",
			"val_name" : "深卡其布色",
			"color" : "bdb76b",
			"styleHtml" : "background: rgb(189, 183, 107);"
		}, {
			"val_id" : "53",
			"attr_id" : "10002",
			"val_name" : "深棕色",
			"color" : "7c4b00",
			"styleHtml" : "background: rgb(124, 75, 0);"
		}, {
			"val_id" : "54",
			"attr_id" : "10002",
			"val_name" : "褐色",
			"color" : "855b00",
			"styleHtml" : "background: rgb(133, 91, 0);"
		}, {
			"val_id" : "55",
			"attr_id" : "10002",
			"val_name" : "驼色",
			"color" : "a88462",
			"styleHtml" : "background: rgb(168, 132, 98);"
		} ]
	}, {
		"attr_id" : "10002",
		"tabs_name" : "花色系",
		"attr_name" : "颜色分类",
		"styleHtml" : "",
		"classHtml" : "color-img-hs",
		"attr_catid" : "10001",
		"cat_name" : "其他颜色",
		"attr_values_arr" : [ {
			"val_id" : "56",
			"attr_id" : "10002",
			"val_name" : "花色",
			"color" : "",
			"styleHtml" : ""
		} ]
	}, {
		"attr_id" : "10002",
		"tabs_name" : "透明系",
		"attr_name" : "颜色分类",
		"styleHtml" : "",
		"classHtml" : "color-img-tm",
		"attr_catid" : "10001",
		"cat_name" : "其他颜色",
		"attr_values_arr" : [ {
			"attr_id" : "10002",
			"val_id" : "57",
			"val_name" : "透明",
			"color" : "transparent",
			"styleHtml" : "background: url('//img.alicdn.com/tps/i3/TB1h8_PFVXXXXcAXFXX8DnLHFXX-60-20.png');"
		} ]
	}, {
		"attr_id" : "10002",
		"tabs_name" : "其他",
		"attr_name" : "颜色分类",
		"styleHtml" : "",
		"classHtml" : "",
		"attr_catid" : "10001",
		"cat_name" : "其他颜色",
		"attr_values_arr" : [ {
			"attr_id" : "10002",
			"val_id" : "58",
			"val_name" : "深空灰色",
			"color" : "",
			"styleHtml" : ""
		}, {
			"val_id" : "59",
			"attr_id" : "10002",
			"val_name" : "粉色",
			"color" : "",
			"styleHtml" : ""
		} ]
	} ]
};
ctoSkuColorData2 = ctoSkuColorCatData.attr_data;
function hebingColorData() {
	var new_arry = new Array();
	for ( var i in ctoSkuColorData2) {
		if (i == 0) {
			new_arry = {
				"attr_id" : ctoSkuColorData2[0].attr_id,
				"tabs_name" : ctoSkuColorData2[0].tabs_name,
				"attr_name" : ctoSkuColorData2[0].attr_name,
				"attr_catid" : ctoSkuColorData2[0].attr_catid,
				"cat_name" : ctoSkuColorData2[0].cat_name,
			};
			new_arry.attr_values_arr = new Array();
		}
		var rows = ctoSkuColorData2[i];
		for ( var j in rows.attr_values_arr) {
			var items = rows.attr_values_arr[j];
			new_arry.attr_values_arr.push(items);
		}
	}
	var new_arry2= new Array();
	new_arry2[0]= new_arry;
	return new_arry2;
}
ctoSkuColorData = hebingColorData();
/**
 * @action 系统默认提供的默认属性--【颜色】
 * @author ctocode-zhw
 * @version 2017-03-17
 */
function __initSkuProvideColor() {
	var htmls = '';
	htmls += '<div class="cto-sku-group group_10001" data-caption="颜色分类" id="skuColorGroup">';
	htmls += ' <label class="cto-sku-label">颜色分类：</label>';
	htmls += ' <div class="cto-sku-clear"></div>';
	htmls += ' <div class="cto-sku-tabs" data-tip="切换tabs">';
	htmls += '   <ul class="clearfix" id="sku-color-tab-header"></ul>';
	htmls += ' </div>';
	htmls += ' <div class="cto-sku" data-tip="属性内容">';
	htmls += '  <div id="sku-color-tab-contents"></div>';
	htmls += ' </div>';
	htmls += '</div>';
	return htmls;
}

// 初始化 颜色头部 2017-02-13
function skuColorInitHeader() {
	$('#sku-color-tab-header').html('');
	var sku_color_tab_header_html = '';
	for ( var i in ctoSkuColorData2) {
		var rows = ctoSkuColorData2[i];
		if (i == 0) {
			sku_color_tab_header_html += '<li data-index="' + i + '" class="selected">';
		} else {
			sku_color_tab_header_html += '<li data-index="' + i + '">';
		}
		var class_html = '';
		if (rows.classHtml != '') {
			class_html = rows.classHtml;
		}
		var style_html = '';
		if (rows.styleHtml != '') {
			style_html = ' style="' + rows.styleHtml + '" ';
		}
		sku_color_tab_header_html += ' <div class="rgb-box ' + class_html + '" ' + style_html + '></div>';
		sku_color_tab_header_html += ' <div class="color-text">' + rows.tabs_name + '</div>';
		sku_color_tab_header_html += '</li>';
	}
	$('#sku-color-tab-header').html(sku_color_tab_header_html);
}
// 初始化 颜色系列 对应的颜色 2017-02-13
function skuColorInitContents() {
	$('#sku-color-tab-contents').html('');
	var sku_color_tab_contents_html = '';
	for ( var i in ctoSkuColorData2) {
		var rows = ctoSkuColorData2[i];
		if (i == 0) {
			sku_color_tab_contents_html += '<ul class="sku-list clearfix" data-index="' + i + '" style="display: block;">';
		} else {
			sku_color_tab_contents_html += '<ul class="sku-list clearfix" data-index="' + i + '" style="display: none;">';
		}
		var items_html = '';
		for ( var j in rows.attr_values_arr) {
			var items = rows.attr_values_arr[j];
			items_html += '<li class="sku-item">';
			items_html += ' <label data-text="' + items.val_name + '" data-flag="' + items.attr_id + '">';
			items_html += '  <input class="J_Checkbox" type="checkbox" data-text="' + items.val_name + '" data-flag="' + items.attr_id + '" data-color="' + items.color + '" >';
			items_html += '  <i class="color-box" style="' + items.styleHtml + '"></i>' + items.val_name + '</label>';
			items_html += '</li>';
		}
		sku_color_tab_contents_html += items_html;
		sku_color_tab_contents_html += '</ul>';
	}
	$('#sku-color-tab-contents').html(sku_color_tab_contents_html);
}
function skuColorInitBindFunc() {
	// 绑定方法
	$('#sku-color-tab-header li').on('click', function() {
		var tabs_index = $(this).data('index');
		var _self = $(this);
		$('#sku-color-tab-header li').removeClass('selected');
		$(this).addClass('selected');
		$('#sku-color-tab-contents ul').hide();
		$('#sku-color-tab-contents ul').each(function() {
			if ($(this).data('index') == tabs_index) {
				$(this).show();
			}
		});
	});
}