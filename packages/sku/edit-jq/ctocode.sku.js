/**
 * @action 通用 商品属性组件-JS
 * @version 2017-02-17
 * @author ctocode-zhw
 * @link http://www.ctocode.com
 */
var ctoSkuClass = function (options) {
	this.defaults = {
		"tips": {},/* 提示 */
		"tabs": {},/* tabs切换 */
		"diy": {},/* 底部-自定义 */
		"specUrl": ["", ""],
		"specPublic": [{
			"url": "",
			"type": "color"
		}, {
			"url": "",
			"type": "color"
		}],
		"ctoSkuInitHtmlFlag": "",
	};
	this.settings = $.extend({}, this.defaults, options);
};
ctoSkuClass.prototype = {
	constructor: ctoSkuClass,
	__loadHtml: function () {
		var initHtmlObj = this.settings.ctoSkuInitHtmlFlag;
		// initHtmlObj.html('');
	},
	__loadAttrDefault: function () {
		// 加载默认属性 ：系统提供的默认属性 2017-03-17 zhw
		$('#defaultAttrSkuBox').html('');
		var colorHtmls = __initSkuProvideColor
		$('#defaultAttrSkuBox').append(colorHtmls);
		skuColorInitHeader();
		skuColorInitContents();
		skuColorInitBindFunc();
		/**
		 * 
		 * ******更多默认属性
		 */
	},
	__loadAttrCategory: function () {
		// 加载分类属性
	},
	reset: function (options) {
		/* 重置 */
		$('#sku-color-tab-contents').find('input[type="checkbox"]').prop('checked', false);
		ctoSkuStart(options);
	},
	start: function () {
		this.__loadHtml();
		this.__loadAttrDefault();
	},
};

// 总后台 属性规格设置的数据
var ctoSkuCategoryAttrData;
var skudata;
var $this = {};

function Save() {
	var query = $(".valid").GetPostData();
	var productid = '156';
	query.ProductID = productid;
	query.ProductSpecList = JSON.stringify(ctoSkuGetParam(productid));
	var url = "/Product/SaveProductSKU";
	$.post(url, query, function (result) {
		$.ligerDialog.warn(result.msg);
		if (result.state) {
			setTimeout(function () {
				window.parent.grid.loadData(true);
				dialog.close();
			}, 1500);
		}
	});
}
/* 获取 */
function ctoSkuGetParam(goods_id) {
	var dataStr = new Array();
	$("#td_sku tr").each(function (index, row) {
		if (index > 0) {
			var skuDetail = $(row).find('input[name="attr_value[]"]').val();
			var goods_attr_sysid = $(row).find('input[name="spec_idID"]').val();
			var attr_costprice = $(row).find('input[name="attr_costprice[]"]').val();
			var attr_wholesale = $(row).find('input[name="attr_wholesale[]"]').val();
			var attr_price = $(row).find('input[name="attr_price[]"]').val();
			var attr_stock = $(row).find('input[name="attr_stock[]"]').val();
			var attr_sn = $(row).find('input[name="attr_sn[]"]').val();

			var image = $(row).find('img[name="Image"]').attr('src');
			if (goods_attr_sysid == "" || goods_attr_sysid == undefined) {
				goods_attr_sysid = 0;
			}
			dataStr.push({
				sysid: goods_attr_sysid,
				goods_id: goods_id,
				attr_catid: 0,
				attr_ids: 0,
				attr_value: skuDetail,
				attr_costprice: attr_costprice,
				attr_wholesale: attr_wholesale,
				attr_price: attr_price,
				attr_stock: attr_stock,
				attr_sn: attr_sn,
				Image: image
			});
		}
	});
	return dataStr;
}

function ctoSkuStart(options) {
	var defaults = {
		"getCategoryDataUrl": "attribute/getAttrParam?attr_catid=",
		"attr_catid": 0,
		"getGoodsAttrDataUrl": "sku/GetProductSpecInfo.json?goods_id=",
		"goods_id": 0
	};
	var settings = Object.assign({}, defaults, options);
	if (!settings.attr_catid) {
		console.log('===属性分类ID不能为空~');
		return false;
	}
	if (settings.attr_catid == 0) {
		$('#td_sku').empty();
		$('#catAttrSkuBox').html('');
		return false;
	}
	$('#td_sku').empty();
	$('#td_sku').html('<caption>销售属性匹配表</caption><thead></thead><tbody></tbody>');
	$.ajax({
		url: settings.getCategoryDataUrl + settings.attr_catid,
		type: "GET",
		async: false,
		dataType: 'json',
		success: function (result) {
			if (result) {
				ctoSkuCategoryAttrData = result.data;
				__initSkuGroupHtml(ctoSkuCategoryAttrData);
			}
			// 当goods_id不为空的时候，请求已经设置的商品 规格参数数据
			if (settings.goods_id != undefined && settings.goods_id != 0) {
				$.ajax({
					url: settings.getGoodsAttrDataUrl + settings.goods_id,
					type: "GET",
					async: false,
					dataType: 'json',
					success: function (result) {
						if (result) {
							// 商户已经选中的数据 input
							var goodsSkuAttrData;
							// 商户设置的sku数据
							var goodsSkuMapData;
							goodsSkuAttrData = result.selectData;
							goodsSkuMapData = result.allRows;
							InitSku(goodsSkuAttrData, goodsSkuMapData);
						}
					}
				});
			}
			return false;
		}
	});
}
function InitSku(goodsSkuAttrData, goodsSkuMapData) {
	// console.log('==goodsSkuAttrData===', goodsSkuAttrData);
	// console.log('==goodsSkuMapData===', goodsSkuMapData);
	// console.log('==ctoSkuCategoryAttrData===', ctoSkuCategoryAttrData);

	if (goodsSkuAttrData != undefined) {
		// 处理 类别规格属性 已选中数据
		for (var i in ctoSkuCategoryAttrData) {
			var attrData = ctoSkuCategoryAttrData[i];
			$('#catAttrSkuBox .group_' + attrData.attr_id).find('input:checkbox').each(function (j, cb) {
				for (var k in goodsSkuAttrData) {
					var selectAttrD = goodsSkuAttrData[k];
					if (selectAttrD.attr_val_item == $(cb).attr('data-text') && selectAttrD.attr_id == $(cb).attr('data-flag')) {
						// console.log('-----选择');
						$(cb).click(); // 
						$(cb).attr('checked', true);
						$(cb).prop('checked', true);
						$(cb).change();
					}
				}
			});
		}
		// 处理颜色
		for (var i in ctoSkuCategoryAttrData) {
			var attrData = ctoSkuCategoryAttrData[i];
			$('#sku-color-tab-contents').find('input:checkbox').each(function (j, cb) {
				for (var k in goodsSkuAttrData) {
					var selectAttrD = goodsSkuAttrData[k];
					if (selectAttrD.attr_val_item == $(cb).attr('data-text') && selectAttrD.attr_id == $(cb).attr('data-flag')) {
						// console.log('-----选择2');
						$(cb).click(); // 
						$(cb).attr('checked', true);
						$(cb).prop('checked', true);
						$(cb).change();
					}
				}
			});
		}
		// ====单独给颜色绑定 2017-02-18

		var trs = $("#td_sku tr");
		for (var k = 1; k < trs.length; k++) {
			var itemVal = $(trs[k]).find(":input[name='attr_value[]']").val() + ";";
			for (var i = 0; i < goodsSkuMapData.length; i++) {
				var bol = true;
				var minSpec = goodsSkuMapData[i].detail_list;
				for (var n = 0; n < minSpec.length; n++) {
					var specId = minSpec[n].attr_id;
					specId = specId == "" ? "0" : specId;
					// [0]规格分类名称^[1]规格分类id^[2]规格名称^[3]规格id
					var str = minSpec[n].attr_val_item + "^" + minSpec[n].val_id + "^" + minSpec[n].attr_name + "^" + specId + ";";
					// console.log(str);
					// console.log(itemVal);
					// console.log(itemVal.indexOf(str));

					if (itemVal.indexOf(str) == -1) {
						bol = false;
						break;
					}
				}

				// 赋值
				if (bol) {
					$(trs[k]).find(':input[name="attr_costprice[]"]').val(goodsSkuMapData[i].attr_costprice);
					$(trs[k]).find(':input[name="attr_wholesale[]"]').val(goodsSkuMapData[i].attr_wholesale);
					$(trs[k]).find(':input[name="attr_price[]"]').val(goodsSkuMapData[i].attr_price);
					$(trs[k]).find(':input[name="attr_stock[]"]').val(goodsSkuMapData[i].attr_stock);
					$(trs[k]).find(':input[name="attr_sn[]"]').val(goodsSkuMapData[i].attr_sn);
					$(trs[k]).find('img[name="Image"]').attr('src', goodsSkuMapData[i].Image);
					// $(trs[k]).find("img[name='Image']").attr("id",
					// "proSpecImg_" + goodsSkuMapData[i].ID);
					// $(trs[k]).find("img[name='Image']").attr("data_productSpecId",
					// goodsSkuMapData[i].ID);
					break;
				}
			}
		}
		// 处理 通用规格属性 已选中数据==》如：颜色

	}

}
/**
 * @action 加载处理 规格分类组 html
 * @author zhw
 * @version 2017-02-17
 * @returns
 */
function __initSkuGroupHtml(categoryAttrData) {
	$('#catAttrSkuBox').html('');
	for (var i in categoryAttrData) {
		var attrData = categoryAttrData[i];
		var htmls = '';
		htmls += '<div class="cto-sku-group group_' + attrData.attr_id + '" data-caption="' + attrData.attr_name + '">';
		htmls += ' <label class="cto-sku-label">' + attrData.attr_name + '：</label>';
		// htmls += ' <label><input type="checkbox" value="1">&nbsp;是否设置单独 价格库存</label>';
		/* 【提示框】 */
		// htmls += ' <div class="cto-sku-tips">';
		// htmls += ' 选择标准 ' + attrData.attr_name + ' 可增加搜索/导购机会，';
		// htmls += '标准 ' + attrData.attr_name + ' 还可填写备注信息！<a href="###" target="_blank">查看详情</a><span class="tips-close">x</span>';
		// htmls += ' </div>';
		// htmls += ' <div class="cto-sku-loading" data-tip="加载表格"></div>';
		htmls += ' <div class="cto-sku-clear"></div>';
		/* 【头部】切换tabs */
		// htmls += ' <div class="cto-sku-tabs" data-tip="切换tabs">';
		// htmls += ' <ul class="clearfix" id="sku-color-tab-header"></ul>';
		// htmls += ' </div>';
		/* 【中部】属性内容 */
		htmls += ' <div class="cto-sku cto-sku-other">';
		htmls += ' </div>';
		/* 【底部】自定义录入 */
		// htmls += ' <div class="cto-sku-diy" data-tip="底部自定义录入">';
		// htmls += ' <ul class="diy-list clearfix">';
		// htmls += ' <li class="diy-item"><input type="checkbox"><input class="diy-text skip" type="text" placeholder="其它' + attrData.attr_name + '" value="" maxlength="30"></li>';
		// htmls += ' <li class="diy-tip">没有合适的' + attrData.attr_name + ' ？可以自己输入最多24个' + attrData.attr_name + '</li>';
		// htmls += ' </ul>';
		// htmls += ' </div>';
		htmls += '</div>';
		$('#catAttrSkuBox').append(htmls);
		var item_htmls = __initSkuGroupItemHtml(attrData);
		$('#catAttrSkuBox .group_' + attrData.attr_id).find('div.cto-sku-other').append(item_htmls);
	}
	__initSkuGroupBindChange(categoryAttrData);
}
/**
 * @action 加载处理 规格分类组下 详细属性的数据拼接
 * @author zhw
 * @version 2017-02-17
 * @returns
 */
function __initSkuGroupItemHtml(attrData) {
	var res = Array();
	var itemData = attrData.attr_values_arr;
	for (var i in itemData) {
		var rows = itemData[i];
		var htmls = '';
		htmls += '<li class="sku-item">';
		htmls += ' <label data-text="' + rows.val_name + '" data-flag="' + rows.attr_id + '">';
		htmls += '  <input class="J_Checkbox" type="checkbox" data-text="' + rows.val_name + '" data-flag="' + rows.attr_id + '">' + rows.val_name;
		htmls += ' </label>';
		/* 自定义修改录入 */
		// htmls += ' <div class="note" style="visibility: hidden;">';
		// htmls += ' <span class="note-title">备注</span><input class="note-val" maxlength="16" type="text">';
		// htmls += ' </div>';
		htmls += '</li>';
		res.push(htmls);
	}
	return '<ul class="sku-list cto-sku-clear">' + res.join("") + '</ul>';
}

/**
 * @action 加载处理 规格分类组下 属性绑定 change方法
 * @author zhw
 * @version 2017-02-17
 * @returns
 */
function __initSkuGroupBindChange(categoryAttrData) {
	for (var i in categoryAttrData) {
		var attrData = categoryAttrData[i];
		$('#catAttrSkuBox .group_' + attrData.attr_id).find('input:checkbox').on('change', function () {
			if ($(this).attr("checked")) {
				$(this).next("input").addClass("SelectInput");
			} else {
				$(this).next("input").removeClass("SelectInput");
			}
			store_Sku();
			handleSkuMap();
			reloadData_Sku();
		});
	}
	// ====单独给颜色绑定 2017-02-18
	$('#sku-color-tab-contents').find('input:checkbox').on('change', function () {
		if ($(this).attr("checked")) {
			$(this).next("input").addClass("SelectInput");
		} else {
			$(this).next("input").removeClass("SelectInput");
		}
		store_Sku();
		handleSkuMap();
		reloadData_Sku();
	});
}
function store_Sku() {
	$this.Cache = [];
	$("#td_sku tr:gt(0)").each(function (i, item) {
		$this.Cache[$(item).find("input:hidden").val()] = $(item).clone();
		$this.Cache.length = $this.Cache.length + 1;
	});
}
function reloadData_Sku() {
	$('#td_sku tr:gt(0)').each(function (i, item) {
		var key = $(item).find('input:hidden').val();
		var curTr = getCurTrInCache(key);
		if (curTr) {
			$(item).find(':input[name="attr_costprice[]"]').val($(curTr).find(':input[name="attr_costprice[]"]').val());
			$(item).find(':input[name="attr_price[]"]').val($(curTr).find(':input[name="attr_price[]"]').val());
			$(item).find(':input[name="attr_stock[]"]').val($(curTr).find(':input[name="attr_stock[]"]').val());
			$(item).find(':input[name="attr_sn[]"]').val($(curTr).find(':input[name="attr_sn[]"]').val());
			$(item).find('img[name="Image"]').attr('src', $(curTr).find('img[name="Image"]').attr('src'));
		}
	});
}

/**
 * @action 2. 展示设置 库存、价格,创建SKU mapData 生成sku的table
 * @author zhw
 * @version 2017-02-17
 * @returns
 */
function handleSkuMap() {
	skudata = new Array();
	// ==========zhw 2017-02-16==Start=====
	for (var i in ctoSkuCategoryAttrData) {
		var attrData = ctoSkuCategoryAttrData[i];
		var group_list = $('#catAttrSkuBox .group_' + attrData.attr_id).find('input:checkbox:checked[data-flag="' + attrData.attr_id + '"]');
		if (group_list.length > 0) {
			var p = $.extend({}, attrData);
			p.children = new Array();
			for (var j in attrData.attr_values_arr) {
				var itemData = attrData.attr_values_arr[j];
				$.each(group_list, function (i, ckitem) {
					if ($(ckitem).attr('data-text') == itemData.val_name) {
						p.children.push(itemData);
					}
				});
			}
			skudata.push(p);
		}
	}

	// ====单独给颜色绑定 2017-02-18
	for (var i in ctoSkuColorData) {
		var attrData = ctoSkuColorData[i];
		var group_list = $('#sku-color-tab-contents').find('input:checkbox:checked[data-flag="' + attrData.attr_id + '"]');
		if (group_list.length > 0) {
			var p = $.extend({}, attrData);
			p.children = new Array();
			for (var j in attrData.attr_values_arr) {
				var itemData = attrData.attr_values_arr[j];
				$.each(group_list, function (i, ckitem) {
					if ($(ckitem).attr('data-text') == itemData.val_name) {
						p.children.push(itemData);
					}
				});
			}
			skudata.push(p);
		}
	}
	// ==========zhw 2017-02-16==End=====
	$this.tabData = [];
	$this.tabTitle = [];
	$.each(skudata, function (i, attrData) {
		var arr = [];
		$.each(attrData.children, function (j, itemData) {
			// [0]规格分类名称^[1]规格分类id^[2]规格名称^[3]规格id
			arr.push(itemData.val_name + "^" + itemData.val_id + "^" + attrData.attr_name + "^" + attrData.attr_id + ";");
		});
		$this.tabData.push(arr);
		$this.tabTitle.push(attrData.attr_name + "^" + arr.length);
	});
	if ($this.tabData.length > 0) {
		$this.tabData = oderbySku($this.tabData);
		$this.tabTitle = oderbyTitle($this.tabTitle);
		$this.res = doExchange($this.tabData);

		// handleSkuMap($this.res);
		$("#td_sku").show();
		// ==============设置头部 thead=============
		__handleMapTableHead($this.tabTitle);
		// ==============设置中部 tbody=============
		__handleMapTableBody($this.res);
		for (var i = 1; i <= (skudata.length); i++) {
			MergeTableRow("#td_sku", i);
		}
	} else {
		$('#td_sku').empty();
		$('#td_sku').html('<caption>销售属性匹配表</caption><thead></thead><tbody></tbody>');
		$('#td_sku').hide();
		$('#td_skuImg').empty();
	}
}
// 2.1展示头部 标题 thead,mapDataTitle规格类型title
function __handleMapTableHead(mapDataTitle) {
	$('#td_sku').find('thead').empty();
	var htmls = new Array();
	htmls.push('<tr>');
	for (var i in mapDataTitle) {
		htmls.push('<th class="td_item">' + mapDataTitle[i].split('^')[0] + '</th>');
	}
	htmls.push('<th class="sku_title"><span>*成本(厂家价)</span></th>');
	htmls.push('<th class="sku_title"><span>*批发价</span></th>');
	htmls.push('<th class="sku_title"><span>*销售价</span></th>');
	htmls.push('<th class="sku_title"><span>*库存</span></th>');
	// htmls.push('<th class="sku_title"><span>*产品编号</span></th>');
	// htmls.push('<th class="sku_title"><span>*图片</span></th>');
	// htmls.push('<th class="sku_title"><span>批量操作</span></th>');
	htmls.push('</tr>');
	$('#td_sku').find('thead').html(htmls.join(""));
}
// 2.2 展示主体 tbody ,mapData具体规格内容
function __handleMapTableBody(mapData) {
	$('#td_sku').find('tbody').empty();
	var htmls = new Array();
	for (var i in mapData) {
		var item = mapData[i];
		var str = item.substring(0, item.length - 1);
		var tr = str.split(';');
		htmls.push('<tr class="J_MapRow">');

		for (var j in tr) {
			// [0]规格分类名称^[1]规格分类id^[2]规格名称^[3]规格id
			var tddata = tr[j].split('^');
			htmls.push('<td>' + tddata[0] + '</td>');
		}
		htmls.push(__handleMapTableBodyTr(str, i));
		htmls.push('</tr>');
	}

	$('#td_sku').find('tbody').html(htmls.join(""));
}
// 2.2.1 展示主体 tbody 单独处理 单行
function __handleMapTableBodyTr(item, i, attr_ids, attr_catid) {
	// var e = "[`~!@#$^&*()=|{}':;',\\[\\].<>/?~！@#￥……&*（）——|{}【】‘；：”“'。，、？]";
	var formObj = $('#BoshFormEdit');
	var goods_price_cost = formObj.find('input[name="goods_price_purchase"]').val() || '0.00';// 成本价
	var goods_wholesale = formObj.find('input[name="wholesale_price"]').val() || '0.00';// 批发价
	var goods_price = formObj.find('input[name="market_price"]').val() || '0.00';// 市场价

	var goods_rest = formObj.find('input[name="goods_"]');

	var e = '';
	var str = '';
	/* 成本价 */
	str += '<td class="cost">';
	str += ' <input name="attr_costprice[]" value="' + goods_price_cost + '" data-type="cost" maxlength="9" type="text" ';
	str += '  onkeyup="this.value=this.value.replace(/[^0-9.]/g,\'\');if(this.value.replace(/.\d*/,\'\').length>7){this.value=this.value.substring(0,7);}">';
	str += '</td>';
	/* 批发价 */
	str += '<td class="wholesale">';
	str += ' <input name="attr_wholesale[]" value="' + goods_wholesale + '" data-type="wholesale" maxlength="9" type="text" ';
	str += '  onkeyup="this.value=this.value.replace(/[^0-9.]/g,\'\');if(this.value.replace(/.\d*/,\'\').length>7){this.value=this.value.substring(0,7);}">';
	str += '</td>';
	/* 销售价 */
	str += '<td class="price">';
	str += ' <input name="attr_price[]" value="' + goods_price + '" data-type="price" maxlength="9" type="text" ';
	str += '  onkeyup="this.value=this.value.replace(/[^0-9.]/g,\'\');if(this.value.replace(/.\d*/,\'\').length>7){this.value=this.value.substring(0,7);}">';
	str += '</td>';
	/* 库存 */
	str += '<td class="quantity">';
	str += ' <input type="hidden" name="attr_value[]" value="' + item + '">';
	str += ' <input type="hidden" name="attr_ids[]" value="' + attr_ids + '">';
	str += ' <input type="hidden" name="attr_catids[]" value="' + attr_catid + '">';
	str += ' <input name="attr_stock[]" value="0" data-type="quantity" maxlength="9" type="text" ';
	str += '  onkeyup="this.value=this.value.replace(/[^0-9]/g,\'\');if(this.value.length>8)this.value=this.value.substring(0,8);">';
	str += '</td>';
	/* 商家编码 */
	// str += '<td class="tsc">';
	// str += ' <input name="attr_sn[]" value="" data-type="tsc" maxlength="25" type="text" class="skip">';
	// str += '</td>';
	/* 商品条形码 */
	// str += '<td class="barcode">';
	// str += ' <input name="attr_barcode[]" value="" data-type="barcode" maxlength="25" type="text">';
	// str += '</td>';
	/* 图片 */
	// var idNum = Number(i + 1);
	// // console.log(idNum);
	// str += '<td>';
	// // <input value="' + idNum + '">
	// // <input type="text" name="Image" style="width:100px;">
	// // <input type="file" style="width:180px;">
	// str += ' <img id="img_' + idNum + '" data_productSpecId="" name="Image" src="" onclick="selectImg(this)">';
	// str += '</td>';
	/* 批量操作 */
	// str += '<td class="batch"><i class="sku-batch"></i></td>';
	return str;
}

function getCurTrInCache(key) {
	var keyArr = key.split(';');
	var curTr;
	for (var property in $this.Cache) {
		var cacheKeyArr = property.split(";");
		var isTarget = false;
		$(keyArr).each(function (j, min) {
			if ($.inArray(min, cacheKeyArr) == -1) {
				isTarget = false;
				return false;
			}
			isTarget = true;
		});
		if (isTarget == true) {
			curTr = $this.Cache[property];
			break;
		}
	}
	;
	return curTr;
}
// sku排序
function oderbySku(a) {
	for (var i = 0; i < a.length; i++) {
		for (var j = i + 1; j < a.length; j++) {
			if (a[i].length > a[j].length) {
				var n;
				n = a[j];
				a[j] = a[i];
				a[i] = n;
			}
		}
	}
	return a;
}
// 标题排序
function oderbyTitle(a) {
	for (var i = 0; i < a.length; i++) {
		for (var j = i + 1; j < a.length; j++) {
			if (parseInt(a[i].split('^')[1]) > parseInt(a[j].split('^')[1])) {
				var n;
				n = a[j];
				a[j] = a[i];
				a[i] = n;
			}
		}
	}
	return a;
}
// sku生成
function doExchange(doubleArrays) {
	var len = doubleArrays.length;
	if (len >= 2) {
		var len1 = doubleArrays[0].length;
		var len2 = doubleArrays[1].length;
		var newlen = len1 * len2;
		var temp = new Array(newlen);
		var index = 0;
		for (var i = 0; i < len1; i++) {
			for (var j = 0; j < len2; j++) {
				temp[index] = doubleArrays[0][i] + doubleArrays[1][j];
				index++;
			}
		}
		var newArray = new Array(len - 1);
		for (var i = 2; i < len; i++) {
			newArray[i - 1] = doubleArrays[i];
		}
		newArray[0] = temp;
		return doExchange(newArray);
	} else {
		return doubleArrays[0];
	}
}
// 合并表格
function MergeTableRow(_w_table_id, _w_table_colnum) {
	_w_table_firsttd = "";
	_w_table_currenttd = "";
	_w_table_SpanNum = 0;
	_w_table_Obj = $(_w_table_id + " tr td:nth-child(" + _w_table_colnum + ")");
	_w_table_Obj.each(function (i) {
		if (i == 0) {
			_w_table_firsttd = $(this);
			_w_table_SpanNum = 1;
		} else {
			_w_table_currenttd = $(this);
			var isSameGroup = mergetableCell_sameGroup(_w_table_firsttd, _w_table_currenttd);

			if (_w_table_firsttd.text() == _w_table_currenttd.text() && (_w_table_colnum == 1 || isSameGroup == true)) {
				_w_table_SpanNum++;
				_w_table_currenttd.hide(); // remove();
				_w_table_firsttd.attr("rowSpan", _w_table_SpanNum);
			} else {
				_w_table_firsttd = $(this);
				_w_table_SpanNum = 1;
			}
		}
	});
};
function mergetableCell_sameGroup(_w_table_firsttd, _w_table_currenttd) {
	var isSameGroup = true;
	var firstPrevAll = $(_w_table_firsttd).prevAll();
	var curPrevAll = $(_w_table_currenttd).prevAll();
	firstPrevAll.each(function (i, item) {
		if ($(curPrevAll[i]).text() != $(item).text()) {
			isSameGroup = false;
			return false;
		}
	});
	return isSameGroup;
};
