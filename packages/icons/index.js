import cvSvgs from './src/svg/svg.vue';
import cvIcons from './src/icons.vue';

cvIcons.install = function (Vue) {
  Vue.component(cvIcons.name, cvIcons);
};
cvSvgs.install = function (Vue) {
  Vue.component(cvSvgs.name, cvSvgs);
};
export { cvIcons, cvSvgs };
