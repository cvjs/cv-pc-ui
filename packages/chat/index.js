import cvChat from './src/chat.vue';

cvChat.install = function (Vue) {
  Vue.component(cvChat.name, cvChat);
};
export { cvChat };
