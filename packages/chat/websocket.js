// 下载依赖，启动 pnpm install ws
import WebSocket, { WebSocketServer } from '/usr/local/lib/node_modules/ws/wrapper.mjs';
// import WebSocket, { WebSocketServer } from 'ws';

// 创建一个WebSocket服务器实例
const wss = new WebSocketServer({ port: 8099 });

// 服务器连接事件
wss.on('connection', (ws) => {
  // 监听客户端发送的消息
  ws.on('message', (message) => {
    console.log('Received message:', message);

    // 向客户端发送消息
    ws.send('Server received your message: ' + message);
  });

  // 监听连接关闭事件
  ws.on('close', () => {
    console.log('Connection closed');
  });
});

console.log('WebSocket server started on port 8099');
