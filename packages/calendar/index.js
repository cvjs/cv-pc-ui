import cvCalendarBase from './src/base.vue';
import cvCalendarMore from './src/more.vue';

cvCalendarBase.install = function (Vue) {
  Vue.component(cvCalendarBase.name, cvCalendarBase);
};
cvCalendarMore.install = function (Vue) {
  Vue.component(cvCalendarMore.name, cvCalendarMore);
};

export { cvCalendarBase, cvCalendarMore };
