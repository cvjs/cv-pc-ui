import cvFormBase from './src/base.vue';
import cvFormInline from './src/inline.vue';
import cvFormItem from './src/item.vue';

cvFormBase.install = function (Vue) {
  Vue.component(cvFormBase.name, cvFormBase);
};
cvFormInline.install = function (Vue) {
  Vue.component(cvFormInline.name, cvFormInline);
};
cvFormItem.install = function (Vue) {
  Vue.component(cvFormItem.name, cvFormItem);
};

export { cvFormBase, cvFormInline, cvFormItem };
