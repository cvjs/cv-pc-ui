/**
 * @action [前端高级编程][插件]----可编辑table表单
 * @author ctocode-zhw
 * @version 2017-04-07 17:03
 * @link http://www.ctocode.com
 */
(function ($, window, document, undefined) {
  var ctoEdittableClass = function (ele, options) {
    this.$element = ele;
    // 合并默认参数，和传入的参数
    this.settings = $.extend(
      {},
      {
        title: '通用弹窗',
        formHtml: '',
        loadUrl: '' /* 显示方式html */,
        initNum: 5,
        initData: '',
        fillTr: 5,
        payInitData: '',
        openPayAllCalc: false, // 是否开启总折扣，总优惠
        openGiveNum: false,
        openOperateBtn: {
          add: true,
          del: true
        }, // 是否开启操作按钮
        tabID: 'edittableTsetId',
        appendID: 'body' /* 追加到哪里 */,
        button: [
          {
            type: 'close' /* 可选 【close 关闭、取消 】【submit 提交 确认】 */,
            name: '关闭',
            callback: function (diaObj) {
              diaObj.close();
            }
          }
        ],
        page: 1, // 加载页
        keyword: '', // 关键字
        apiUrl: '', // ajax 请求数据url
        apiParam: {}, // ajax 请求的附加参数,【前期要保证数据格式正确】
        appendHtmlObj: '', // 填充内容的对象
        source: 0, // 商品来源 ：0 向本商户请求商品 ，1 向平台供应商请求商品
        selectCallback: function (itemObj, inputObj, trObj) {},
        catSelectCallback: function () {},
        /* 商品相关 */

        /* 仓库相关 */
        depotOpen: false, // 是否开启仓库
        depotUrl: '' /* 显示方式html */,

        /* 价格相关 */
        priceOpen: false, // 是否开启价格修改
        priceUrl: '', // ajax 请求的附加参数,【前期要保证数据格式正确】商品历史价格
        priceChangeUrl: '', // 是否修改商品价格,成本、批发、售价
        priceParam: {} // ajax 请求的附加参数,【前期要保证数据格式正确】商品历史价格
      },
      options
    );
    /* 生成表单的对象 */
    this.edittableObj = '';
    /* 最后计算的对象 */
    this.payallcalcObj = '';
    /* 显示的搜索选择框的对象 */
    this.droplistBoxObj = '';

    /* 当前关键字 */
    this.currLoadKeyword = '';

    /* 当前参数(拼接) */
    this.currLoadParam = '';

    /* 每页显示的个数 */
    this.currLoadpageSize = 3;
    /* 当前页数 */
    this.currLoadPage = 1;
    /* 是否禁止加载 */
    this.isLoad = true;
    /* 开始，结束当前个数 */
    this.pageStart = 0;
    this.pageEnd = 0;
    /* 延迟加载 */
    this.searchTimeFlag = 0;
    /* 仓库拼接参数 */
    this.depot_param = '';
  };
  // 定义GoodsClass的方法
  ctoEdittableClass.prototype = {
    constructor: ctoEdittableClass,

    __loadHtml: function () {
      var bodyW = document.body.clientWidth;
      var bodyW2 = document.documentElement.clientWidth;
      var setWidth = 1356;
      if (bodyW < 1356) {
        setWidth = 1356;
      }
      var htmls = '';
      htmls += '<!-- B【 通用可编辑table表单插件@2017-04-07 zhw 】' + this.settings.tabID + ' -->';
      htmls += '<div class="cto-edittable-wrap cto-clear" id="' + this.settings.tabID + '">';

      if (this.settings.priceOpen == true) {
        htmls += '<div class="cto-edittable-comfirm-price-wrap" style="padding: 10px 13px 6px 16px;font-size:16px;">';
        htmls += '<label style="color:red"><input type="checkbox" name="comfirmPrice">';
        htmls += '&nbsp;&nbsp;修改单价时，每次显示同步价格弹窗</label>';
        htmls += '</div>';
      }
      htmls += ' <div style="overflow-x: scroll;">';

      htmls += ' <div class="cto-edittable" style="width:' + setWidth + 'px;">';
      htmls += '  <table class="edittable-table">';
      htmls += this.__loadTheadHtml();
      htmls += '   <tbody></tbody>';
      htmls += this.__loadTfootHtml();
      htmls += '  </table>';
      htmls += ' </div>';

      htmls += ' </div>';

      // TODO 总折扣，优惠，默认全部加载，只判断是否显示
      htmls += this.__loadPayAllCalcHtml();

      htmls += '</div>';
      htmls += '<!-- E【 通用可编辑table表单插件@2017-04-07 zhw 】 -->';
      $(this.settings.appendID).append(htmls);
    },
    __loadDroplistHtml: function () {
      var htmls = '';
      htmls += '<!-- B【 加载搜索选择弹窗 @2017-04-07 zhw】 -->';
      htmls += '<div class="cto-edittable-droplist-wrap" id="' + this.settings.tabID + 'Drop" style="display: none;">';
      // htmls += ' <div class="droplist-category-wrap">';
      // htmls += ' <div class="droplist-category">';
      // htmls += ' <ul>';
      // htmls += ' <li class="on"><div class="name">全部</div></li>';
      // htmls += ' <li><div class="name">分类</div></li>';
      // htmls += ' <li><div class="name">分类</div></li>';
      // htmls += ' <li><div class="name">分类</div></li>';
      // htmls += ' <li><div class="name">分类</div></li>';
      // htmls += ' <li><div class="name">分类</div></li>';
      // htmls += ' </ul>';
      // htmls += ' </div>';
      // htmls += ' </div>';
      htmls += ' <div class="droplist-goods-wrap" style="width: 100%;">';
      htmls += '  <div class="droplist-goods">';
      htmls += '   <div style="position:relative;overflow:auto;height:182px;" class="dropList">';
      htmls += '    </div>';
      htmls += '  </div>';
      htmls += ' </div>';
      htmls += ' <div class="cto-clear"></div>';
      htmls += ' <div class="droplist-extra">其他提示</div>';
      htmls += '</div>';
      $('body').append(htmls);
    },
    __loadTbodyTrHtml: function (items, row_num_str) {
      var htmls = '';
      htmls += '<tr class="item">';
      htmls += '<td>1</td>';
      htmls += '<td>';
      var opBtnGroup = this.settings.openOperateBtn;
      if (opBtnGroup.add == true) {
        htmls += ' <span class="add" title="新增行"></span>';
      }
      if (opBtnGroup.del == true) {
        htmls += ' <span class="del" title="删除行"></span>';
      }
      htmls += '  </td>';
      var attr_value = items.row_attr_value || '';
      if (attr_value !== '') {
        var goods_name = items.row_name + '<br>[ ' + items.row_attr_str + ' ]';
        htmls += '<td class="text droplist">' + goods_name + '</td>';
      } else {
        htmls += '<td class="text droplist">' + items.row_name + '</td>';
      }
      if (this.settings.depotOpen == true) {
        htmls += '<td class="depot">' + items.row_depot_name + '</td>';
      }
      htmls += '<td class="row_units">' + items.row_units + '</td>';
      htmls += '<td class="price row_price">' + items.row_price + '</td>';
      htmls += '<td class="num row_num">' + row_num_str + '</td>';
      if (this.settings.openGiveNum == true) {
        htmls += '<td class="num row_give_num">' + items.row_give_num + '</td>';
      }
      htmls += '<td class="price row_amount">' + items.row_amount + '</td>';
      htmls += '<td class="price row_discount">' + items.row_discount + '</td>';
      htmls += '<td class="price row_disamount">' + items.row_disamount + '</td>';
      htmls += '<td class="price row_youhui">' + items.row_youhui + '</td>';
      htmls += '<td class="price row_taxrate">' + items.row_taxrate + '</td>';
      htmls += '<td class="price row_tax">' + items.row_tax + '</td>';
      htmls += '<td class="price row_heji">' + items.row_heji + '</td>';
      htmls += '</tr>';
      return htmls;
    },
    __bindBtnFunc: function () {
      var _self = this;
      /* 绑定选中下拉选择弹窗单条数据 */
      _self.droplistBoxObj.find('.dropList').on('click', '.droplist-item', function () {
        /* 获取选中item参数 */
        var itemObj = $(this);
        var row_name = itemObj.data('row_name');
        var row_attr_str = itemObj.data('row_attr_str');
        var row_attr_value = itemObj.data('row_attr_value');
        var row_id = itemObj.data('row_id');
        var row_rest = itemObj.data('row_rest');
        if (row_rest < 1) {
          //					if (!confirm("库存不足~是否继续选择？")) {
          //						window.event.returnValue = false;
          //						return false;
          //					}
        }

        _self.depot_param = '?goods_id=' + row_id + '&goods_attr_value=' + row_attr_value;

        var row_units = itemObj.data('row_units');
        var row_price = itemObj.data('row_price');

        /* 查找对象，赋予参数 */
        var isKeyupTrObj = _self.edittableObj.find('tr.item.isKeyupTr');
        isKeyupTrObj.find('input[name="detail_name[]"]').val(row_name);
        isKeyupTrObj.find('input[name="detail_name[]"]').attr('data-row_name', row_name);
        isKeyupTrObj.find('input[name="detail_attr_str[]"]').val(row_attr_str);
        isKeyupTrObj.find('input[name="detail_attr_value[]"]').val(row_attr_value);
        isKeyupTrObj.find('input[name="detail_id[]"]').val(row_id);
        isKeyupTrObj.find('input[name="detail_num[]"]').val(1);
        // 商品价格
        isKeyupTrObj.find('input[name="detail_price[]"]').val(row_price);

        isKeyupTrObj.find('td.row_units').html(row_units);
        isKeyupTrObj.find('td.row_price').html(row_price);
        isKeyupTrObj.find('td.row_num').html(1);

        /* 赋值名字 */
        var isKeyupDropObj = isKeyupTrObj.find('.droplist');
        if (row_attr_str != '') {
          isKeyupDropObj.find('input').val(row_name + '<br>[ ' + row_attr_str + ' ]');
        } else {
          isKeyupDropObj.find('input').val(row_name);
        }
        _self.countTotal();
        /* 回调自定义选中其他的操作 */
        _self.settings.selectCallback(this);
        _self.fillTrBody();
        _self.droplistBoxObj.hide();

        // 多选框
        isKeyupTrObj
          .find('td.depot')
          .off()
          .editable(
            function (value, settings) {
              $(this).parents('tr').find('input[name="detail_depot[]"]').val(value);
              var sel_html = $(this).parents('tr').find('select option:checked').html();
              return sel_html;
            },
            {
              type: 'select',
              placeholder: '',
              cancel: '取消', // 取消编辑按钮的文字
              submit: '确定', // 确认提交按钮的文字
              indicator: '保存中...', // 提交处理过程中显示的提示文字
              tooltip: '单击编辑...', // 鼠标悬停时的提示信息 onblur : 'submit',
              // onblur : 'ignore',
              // "callback" : function(sValue, y) {
              // // var aPos = oTable.fnGetPosition(this);
              // // oTable.fnUpdate(sValue, aPos[0], aPos[1]);
              // },
              submitdata: function (value, settings) {
                // return {
                // "row_id" : this.parentNode.getAttribute("id"),
                // "column" : oTable.fnGetPosition(this)[2]
                // }
              },
              loadurl: _self.settings.depotUrl + _self.depot_param,
              width: 'calc(100% + 5px)',
              height: '100%'
            }
          );
      });
      /* 绑定添加按钮 */
      _self.edittableObj.find('tbody').on('click', 'span.add', function () {
        var trObjArr = _self.addTr('', 1);
        $(this).parents('tbody').find('tr.lastTr').find('span.del').show();
      });
      /* 绑定删除按钮 */
      _self.edittableObj.find('tbody').on('click', 'span.del', function () {
        var currObj = $(this);
        var tbodyObj = currObj.parents('tbody');
        var trObj = currObj.parents('tr');
        var trNum = tbodyObj.find('tr').length;
        trNum = parseInt(trNum);
        if (trNum <= 1) {
          return false;
        }
        var detail_id = trObj.find('input[name="detail_id[]"]').val() || 0;
        if (detail_id != 0) {
          if (!confirm('确认删除？')) {
            window.event.returnValue = false;
            return false;
          } else {
            if (trNum == 2) {
              trObj.siblings('tr').addClass('lastTr');
              trObj.siblings('tr').find('span.del').hide();
            }
            var detail_sysid = trObj.find('input[name="detail_sysid[]"]').val() || 0;

            // 获取删除的ids
            if (detail_sysid != 0) {
              var delete_sysids = _self.edittableObj.find('tfoot').find('input[name="detail_del_sysids"]').val();
              delete_sysids = delete_sysids + '^#^' + detail_sysid;
              _self.edittableObj.find('tfoot').find('input[name="detail_del_sysids"]').val(delete_sysids);
            }
            trObj.remove();
          }
        } else {
          if (trNum == 2) {
            trObj.siblings('tr').addClass('lastTr');
            trObj.siblings('tr').find('span.del').hide();
          }
          trObj.remove();
        }
        _self.countTotal();
        _self.__resetTable();
      });
    },

    fillTrBody: function () {
      // 自动填充补齐去除 body 前面的空行tr
      var _self = this;
      var trLength = this.edittableObj.find('tbody').find('tr').length;
      var fillTr = parseInt(this.settings.fillTr);
      if (fillTr != '' && fillTr > 0) {
        this.edittableObj
          .find('tbody')
          .find('tr')
          .each(function (i, row) {
            var detail_id = $(this).find('input[name="detail_id[]"]').val();
            if (detail_id == 0) {
              $(this).remove();
              trLength--;
              if (trLength < 5) {
                _self.addTr({}, 1);
              }
              _self.countTotal();
            }
          });
      }
    },
    clearBody: function () {
      this.edittableObj.find('tbody').html('');
      this.countTotal();
    },
    clearTr: function () {
      $('tr.isKeyupTr').remove();
      this.addTr({}, 1);
      this.countTotal();
    },
    __ifValDefault: function (valBegin, valDefaul) {
      if (valBegin == null || valBegin == undefined || valBegin == '') {
        return valDefaul;
      }
      return valBegin;
    },
    addTr: function (initData, loadNum) {
      loadNum = parseInt(loadNum);
      var _self = this;
      var trObjArr = new Array();
      for (var i = 0; i < loadNum; i++) {
        var rows = initData[i];
        var items = $.extend(
          {
            row_sysid: 0,
            row_id: 0,
            row_name: '',
            row_attr_value: 0,
            row_attr_str: 0,
            row_num: 1,
            row_units: '',
            row_price: '0.00',
            row_amount: '0.00',
            row_discount: 10,
            row_disamount: '0.00',
            row_youhui: '0.00',
            row_taxrate: '0.00',
            row_tax: '0.00',
            row_give_num: '0',
            row_heji: '0.00',
            row_taocan: 0,
            row_cardbag_id: 0,
            row_depot_id: 0,
            row_depot_name: ''
          },
          rows
        );
        items.row_sysid = _self.__ifValDefault(items.row_sysid, 0);
        items.row_id = _self.__ifValDefault(items.row_id, 0);
        items.row_name = _self.__ifValDefault(items.row_name, '');
        items.row_attr_value = _self.__ifValDefault(items.row_attr_value, 0);

        items.row_num = _self.__ifValDefault(items.row_num, 1);
        items.row_give_num = _self.__ifValDefault(items.row_give_num, 0);

        items.row_units = _self.__ifValDefault(items.row_units, '');

        items.row_price = _self.__ifValDefault(items.row_price, '0.00');
        items.row_amount = _self.__ifValDefault(items.row_amount, '0.00');
        items.row_discount = _self.__ifValDefault(items.row_discount, 10);
        items.row_disamount = _self.__ifValDefault(items.row_disamount, '0.00');

        items.row_youhui = _self.__ifValDefault(items.row_youhui, '0.00');
        items.row_taxrate = _self.__ifValDefault(items.row_taxrate, '0.00');
        items.row_tax = _self.__ifValDefault(items.row_tax, '0.00');
        items.row_heji = _self.__ifValDefault(items.row_heji, '0.00');
        items.row_taocan = _self.__ifValDefault(items.row_taocan, 0);
        items.row_cardbag_id = _self.__ifValDefault(items.row_cardbag_id, 0);

        items.row_depot_id = _self.__ifValDefault(items.row_depot_id, 0);
        items.row_depot_name = _self.__ifValDefault(items.row_depot_name, '');

        row_num_str = rows != undefined ? items.row_num : 0;
        var itemObj = $(_self.__loadTbodyTrHtml(items, row_num_str));
        itemObj.appendTo(_self.edittableObj.find('tbody'));
        if (rows != undefined) {
          _self.countTotal();
          this.fillTrBody();
        }
        trObjArr.push(itemObj);
      }
      this.__resetTable(trObjArr);
    },
    __initialize: function () {
      // 是否存在对象,不存在的话，追加到
      if ($('#' + this.settings.tabID).length <= 0) {
        this.__loadHtml();
        this.edittableObj = $('#' + this.settings.tabID);
        /* 绑定总折扣，总优惠 */
        if (this.settings.openPayAllCalc == true) {
          this.payallcalcObj = $('#' + this.settings.tabID + 'Payallcalc');
          this.__bindPayallCalcFunc();
        }

        if (this.settings.initData != '') {
          this.addTr(this.settings.initData, this.settings.initData.length);
        } else {
          this.addTr({}, this.settings.initNum);
        }
        this.__loadDroplistHtml();
        this.__pricelistHtmlAndBindFunc();
      } else {
        this.edittableObj = $('#' + this.settings.tabID);
      }

      this.droplistBoxObj = $('#' + this.settings.tabID + 'Drop');
      this.__bindBtnFunc();
      /* 绑定总折扣，总优惠 */
      if (this.settings.openPayAllCalc == true) {
        this.payallcalcObj = $('#' + this.settings.tabID + 'Payallcalc');
        this.__bindPayallCalcFunc();
      }
    },
    __resetTable: function (trObjArr) {
      var _self = this;
      // 重置table表单，并且绑定方法
      this.edittableObj.find('tbody tr').each(function (i, row) {
        $(this)
          .find('td:first')
          .html(i + 1);
      });
      trObjArr = trObjArr || '';
      if (trObjArr != '') {
        for (var i in trObjArr) {
          var trObj = trObjArr[i];

          // 仓库点击事件
          trObj.find('td.depot').on('click', function () {
            var row_id = trObj.find('input[name="detail_id[]"]').val();
            var row_attr_value = trObj.find('input[name="detail_attr_value[]"]').val();
            _self.depot_param = '?goods_id=' + row_id + '&goods_attr_value=' + row_attr_value;
            // 多选框
            _self.edittableObj
              .find('td.depot')
              .off()
              .editable(
                function (value, settings) {
                  $(this).parents('tr').find('input[name="detail_depot[]"]').val(value);
                  var sel_html = $(this).parents('tr').find('select option:checked').html();
                  return sel_html;
                },
                {
                  type: 'select',
                  placeholder: '',
                  cancel: '取消', // 取消编辑按钮的文字
                  submit: '确定', // 确认提交按钮的文字
                  indicator: '保存中...', // 提交处理过程中显示的提示文字
                  tooltip: '单击编辑...', // 鼠标悬停时的提示信息 onblur :
                  // 'submit',
                  // onblur : 'ignore',
                  // "callback" : function(sValue, y) {
                  // // var aPos = oTable.fnGetPosition(this);
                  // // oTable.fnUpdate(sValue, aPos[0], aPos[1]);
                  // },
                  submitdata: function (value, settings) {
                    // return {
                    // "row_id" :
                    // this.parentNode.getAttribute("id"),
                    // "column" : oTable.fnGetPosition(this)[2]
                    // }
                  },
                  loadurl: _self.settings.depotUrl + _self.depot_param,
                  width: 'calc(100% + 5px)',
                  height: '100%'
                }
              );
          });

          /* 商品搜索下拉 */
          trObj.find('td.text.droplist').on('click', function () {
            $(this).parents('tr').addClass('isKeyupTr').siblings().removeClass('isKeyupTr');
            $(this).trigger('keyup');
          });
          trObj
            .find('td.text.droplist')
            .off('keyup')
            .on('keyup', function () {
              clearTimeout(_self.searchTimeFlag);
              var droplistObj = $(this);
              _self.searchTimeFlag = setTimeout(function () {
                droplistObj.parents('tr').addClass('isKeyupTr').siblings().removeClass('isKeyupTr');
                // 当输入框变化的时候
                // 1.变更关键字
                var currLoadKeyword = droplistObj.find('input').val();
                // 2.定位
                _self.__dingweiBox(droplistObj, _self.droplistBoxObj, 500);
                $(window).on('resize', function () {
                  _self.__dingweiBox(droplistObj, _self.droplistBoxObj, 500);
                });
                // 3.重置,并传入关键字
                _self.currLoadKeyword = currLoadKeyword || '';
                _self.currLoadPage = 1;
                _self.droplistBoxObj.find('.dropList').html('');
                // 5.加载数据
                _self.ajaxLoadData();
              }, 100);
            });
          trObj.find('td.text.droplist').editable(
            function (value, settings) {
              _self.droplistBoxObj.hide();
              if (value == '') {
                _self.clearTr();
              }
              return value;
            },
            {
              placeholder: '',
              type: 'text',
              indicator: '保存中...' /* 提交处理过程中显示的提示文字 */,
              tooltip: '单击编辑...' /* 鼠标悬停时的提示信息 */,
              onblur: 'submit',
              data: function () {
                var row_name = $('tr.isKeyupTr').find('input[name="detail_name[]"]').val();
                $('tr.isKeyupTr').removeClass('isKeyupTr');
                return row_name;
              },
              event: 'click',
              width: 'calc(100% + 5px)',
              height: '100%'
            }
          );

          /* 商品价格 */
          trObj.find('td.price.row_price').on('click', function () {
            var priceObj = $(this);
            var parentObj = priceObj.parents('tr');
            parentObj.addClass('isKeyupTr').siblings().removeClass('isKeyupTr');

            // 1.获取商品信息
            var detail_id = parentObj.find('input[name="detail_id[]"]').val();
            var detail_attr_value = parentObj.find('input[name="detail_attr_value[]"]').val();
            var detail_attr_str = parentObj.find('input[name="detail_attr_str[]"]').val();

            if (detail_id == 0 || _self.settings.priceUrl == '' || _self.settings.priceOpen != true) {
              return false;
            }

            // 2.定位
            _self.__dingweiBox(priceObj, _self.pricelistBoxObj, 730);
            $(window).on('resize', function () {
              _self.__dingweiBox(priceObj, _self.pricelistBoxObj, 730);
            });
            // 3.传入商品相关参数,获取数据
            var goodsOptions = {
              goods_id: detail_id,
              goods_attr_value: detail_attr_value,
              goods_attr_str: detail_attr_str
            };

            _self.__pricelistGundongFunc(goodsOptions);
          });

          // 是否可编辑商品价格
          if (_self.settings.priceOpen == true) {
            trObj.find('td.price.row_price').editable(
              function (value, settings) {
                if (isNaN(value) || value <= 0) {
                  value = '0.00';
                }
                $(this).parents('tr').find('input[name="detail_price[]"]').val(value);
                value = parseFloat(value).toFixed(2);
                $(this).html(value);
                _self.countTotal();

                // 是否展示同步商品弹窗--begin
                var isComfirmPrice = $('input[name="comfirmPrice"][type="checkbox"]').is(':checked');
                if (isComfirmPrice) {
                  // 获取商品的id
                  var isKeyupTrObj = _self.edittableObj.find('tr.item.isKeyupTr');
                  var goods_id = isKeyupTrObj.find('input[name="detail_id[]"]').val() || 0;
                  if (goods_id == '') {
                    $.msglayer('请选择商品');
                  }
                  var co_str = '是否立即同步商品';
                  var priceChangeParam = '';
                  var recent_price = value;
                  if (_self.settings.priceParam.price_type == 'goods_price') {
                    co_str += '【市场售价】？';
                    priceChangeParam += '&goods_price_market=' + recent_price;
                  } else if (_self.settings.priceParam.price_type == 'goods_price') {
                    co_str += '【批发价】？';
                    priceChangeParam += '&goods_price_wholesale=' + recent_price;
                  } else if (_self.settings.priceParam.price_type == 'goods_price_cost') {
                    co_str += '【成本价】？';
                    priceChangeParam += '&goods_price_cost=' + recent_price;
                  }
                  if (confirm(co_str)) {
                    $.ajax({
                      type: 'GET',
                      url: _self.settings.priceChangeUrl + '?goods_id=' + goods_id + priceChangeParam,
                      dataType: 'json',
                      success: function (result) {
                        if (result.status == 200) {
                          $.msglayer('商品价格同步成功~');
                        } else {
                          $.msglayer('商品价格同步失败~');
                        }
                      }
                    });
                  }
                }
                // 是否展示同步商品弹窗--end

                // 返回商品价格
                _self.pricelistBoxObj.hide();
                return value;
              },
              {
                placeholder: '0',
                type: 'text',
                indicator: '保存中...',
                tooltip: '单击编辑...',
                onblur: 'submit',
                width: 'calc(100% + 5px)',
                height: '100%'
              }
            );
          }

          /* 数量 */
          trObj.find('td.num.row_num').editable(
            function (value, settings) {
              if (isNaN(value) || value <= 0) {
                value = 0;
              }
              $(this).parents('tr').find('input[name="detail_num[]"]').val(value);
              value = parseInt(value);
              $(this).html(value);
              _self.countTotal();
              return value;
            },
            {
              placeholder: '0',
              type: 'text',
              indicator: '保存中...',
              tooltip: '单击编辑...',
              onblur: 'submit',
              width: 'calc(100% + 5px)',
              height: '100%'
            }
          );
          /* 折扣 */
          trObj.find('td.price.row_discount').editable(
            function (value, settings) {
              if (isNaN(value) || value <= 0 || value >= 10) {
                value = 10;
              }
              if (value <= 1) {
                value = 1;
              }
              $(this).parents('tr').find('input[name="detail_discount[]"]').val(value);
              value = parseFloat(value).toFixed(2);
              $(this).html(value);
              _self.countTotal();
              return value;
            },
            {
              placeholder: '',
              type: 'text',
              indicator: '保存中...' /* 提交处理过程中显示的提示文字 */,
              tooltip: '1.00-10.00' /* 鼠标悬停时的提示信息 */,
              onblur: 'submit',
              width: 'calc(100% + 5px)',
              height: '100%'
            }
          );
          /* 优惠金额 */
          trObj.find('td.price.row_youhui').editable(
            function (value, settings) {
              if (isNaN(value) || value <= 0) {
                value = '0.00';
              }
              $(this).parents('tr').find('input[name="detail_youhui[]"]').val(value);
              var curr_amount = $(this).parents('tr').find('td.row_amount').html();
              if (curr_amount <= 0) {
                value = '0.00';
              }
              value = parseFloat(value).toFixed(2);
              $(this).html(value);
              _self.countTotal();
              return value;
            },
            {
              placeholder: '',
              type: 'text',
              indicator: '保存中...' /* 提交处理过程中显示的提示文字 */,
              tooltip: '单击编辑...' /* 鼠标悬停时的提示信息 */,
              onblur: 'submit',
              width: 'calc(100% + 5px)',
              height: '100%'
            }
          );
          /* 税率(%) */
          trObj.find('td.price.row_taxrate').editable(
            function (value, settings) {
              if (isNaN(value) || value < 0) {
                value = '0';
              }
              if (value >= 100) {
                value = 100;
              }
              $(this).parents('tr').find('input[name="detail_taxrate[]"]').val(value);
              value = parseFloat(value).toFixed(2);
              $(this).html(value);
              _self.countTotal();
              return value;
            },
            {
              placeholder: '0.00',
              type: 'text',
              indicator: '保存中...' /* 提交处理过程中显示的提示文字 */,
              tooltip: '单击编辑...' /* 鼠标悬停时的提示信息 */,
              onblur: 'submit',
              width: 'calc(100% + 5px)',
              height: '100%'
            }
          );

          /* 赠送数量 */
          trObj.find('td.num.row_give_num').editable(
            function (value, settings) {
              if (isNaN(value) || value <= 0) {
                value = 0;
              }
              $(this).parents('tr').find('input[name="detail_give_num[]"]').val(value);
              value = parseInt(value);
              $(this).html(value);
              _self.countTotal();
              return value;
            },
            {
              placeholder: '0',
              type: 'text',
              indicator: '保存中...',
              tooltip: '单击编辑...',
              onblur: 'submit',
              width: 'calc(100% + 5px)',
              height: '100%'
            }
          );
        }
      }
    },
    index: function () {
      this.__initialize();
    },
    __dingweiBox: function (dw_parent_Obj, dw_son_Obj, min_width) {
      // 获取父菜单的位置
      var X = dw_parent_Obj.offsetLeft + 'px';
      var Y = dw_parent_Obj.offsetTop + 'px';
      var dw_width = dw_parent_Obj.width();
      if (dw_width <= min_width) {
        dw_width = min_width;
      }
      dw_son_Obj.css({
        width: dw_width + 5,
        top: parseInt(Y) + parseInt('36px') + 'px',
        left: X
      });
    },
    gundong: function () {
      /* ===== 绑定滚动监听 ===== */
      var _self = this;
      var nScrollHight = 0; // 滚动距离总长(注意不是滚动条的长度)
      var nScrollTop = 0; // 滚动到的当前位置
      var max_height = _self.droplistBoxObj.find('.dropList').height();
      _self.droplistBoxObj
        .find('.dropList')
        .off('scroll')
        .on('scroll', function (event) {
          nScrollHight = $(this)[0].scrollHeight;
          nScrollTop = $(this)[0].scrollTop;
          if (nScrollTop + max_height >= nScrollHight) {
            console.log('滚动条到底部了');
            _self.ajaxLoadData();
            return;
          }
        });
    },
    countTotal: function () {
      var all_heji = 0;
      var all_num = 0;
      var all_disamount = 0;
      var all_youhui = 0;
      var all_tax = 0;
      var all_amount = 0;
      var all_give_num = 0;
      var _self = this;

      _self.edittableObj
        .find('tbody')
        .find('tr')
        .each(function () {
          /* 获取单价,数量,保证数值的类型,计算金额 和 合计 */
          var row_price = $(this).find('td.row_price').html();
          row_price = parseFloat(row_price);
          var row_num = $(this).find('td.row_num').html();
          row_num = parseInt(row_num);
          /* 判断是否是套餐0819 */
          var is_taocan = $(this).find('input[name="detail_taocan[]"]').val();
          is_taocan = is_taocan || 0;
          var row_amount = row_price * row_num;
          if (is_taocan == 1) {
            row_amount = 0;
          }
          row_amount = row_heji = parseFloat(row_amount).toFixed(2);
          $(this).find('td.row_amount').html(row_amount);
          $(this).find('td.row_heji').html(row_heji);

          // 当折扣字段存在并且改变
          var row_discount = $(this).find('td.row_discount').html();
          if (row_discount != undefined && isNaN(row_discount) == false && row_discount > 0) {
            row_discount = parseFloat(row_discount);
            if (row_discount > 0) {
              var row_disamount = row_amount * (1 - row_discount * 0.1);
              row_disamount = row_disamount.toFixed(2);
              $(this).find('td.row_disamount').html(row_disamount);
              row_heji = row_heji - row_disamount;
              all_disamount = parseFloat(all_disamount) + parseFloat(row_disamount);
            }
          }
          // 当优惠字段存在并且改变
          var row_youhui = $(this).find('td.row_youhui').html();
          if (row_youhui != undefined && isNaN(row_youhui) == false && row_youhui > 0) {
            if (row_amount > 0) {
              var row_disamount = row_amount * (1 - row_discount * 0.1);
              row_disamount = row_disamount.toFixed(2);
              $(this).find('td.row_disamount').html(row_disamount);
              row_heji = row_heji - row_youhui;
            }
          }
          // 当税率字段存在并且改变 获取税率,并且保证数值的 类型,并且重置总价*
          var row_taxrate = $(this).find('td.row_taxrate').html();
          if (row_taxrate != undefined && isNaN(row_taxrate) == false) {
            row_taxrate = parseFloat(row_taxrate);
            /* 3 税额 = 购货金额*税率 （折扣 >0 的情况下 ） */
            var row_tax = row_heji * row_taxrate * 0.01;
            if (row_tax == 0) {
              row_tax = row_tax;
            } else if (row_tax != 0) {
              row_tax = row_tax.toFixed(2);
            }
            $(this).find('td.row_tax').html(row_tax);
            row_heji = parseFloat(row_heji) + parseFloat(row_tax);
          }
          row_heji = parseFloat(row_heji).toFixed(2);
          $(this).find('td.row_heji').html(row_heji);

          all_num = parseInt(all_num) + parseInt(row_num);
          all_amount = parseFloat(all_amount) + parseFloat(row_amount);

          all_youhui = parseFloat(all_youhui) + parseFloat(row_youhui);
          all_tax = parseFloat(all_tax) + parseFloat(row_tax);
          all_heji = parseFloat(all_heji) + parseFloat(row_heji);

          // 当赠送数量改变的时候
          var row_give_num = $(this).find('td.row_give_num').html();
          row_give_num = parseInt(row_give_num);

          all_give_num = parseInt(all_give_num) + parseInt(row_give_num);
        });
      all_num = parseInt(all_num);
      all_amount = parseFloat(all_amount).toFixed(2);
      all_disamount = parseFloat(all_disamount).toFixed(2);
      all_youhui = parseFloat(all_youhui).toFixed(2);
      all_tax = parseFloat(all_tax).toFixed(2);
      all_heji = parseFloat(all_heji).toFixed(2);
      _self.edittableObj.find('tfoot').find('.all_num').html(all_num);
      _self.edittableObj.find('tfoot').find('.all_give_num').html(all_give_num);
      _self.edittableObj.find('tfoot').find('.all_amount').html(all_amount);
      _self.edittableObj.find('tfoot').find('.all_disamount').html(all_disamount);
      _self.edittableObj.find('tfoot').find('.all_youhui').html(all_youhui);
      _self.edittableObj.find('tfoot').find('.all_tax').html(all_tax);
      _self.edittableObj.find('tfoot').find('.all_heji').html(all_heji);
      if (_self.settings.openPayAllCalc == true) {
        _self.calcPayAll();
      }
    },
    __bindPayallCalcFunc: function () {
      var _self = this;
      _self.payallcalcObj.find('input.pay_discount').on('change', function () {
        var paydiscount = $(this).val();
        var all_heji = _self.edittableObj.find('tfoot').find('.all_heji').html();
        if (!isNaN(all_heji) && all_heji <= 0) {
          all_heji = 0;
          paydiscount = '10';
        } else {
          paydiscount = parseFloat(paydiscount);
          if (isNaN(paydiscount) || paydiscount <= 0) {
            paydiscount = '10';
          }
          if (paydiscount >= 10) {
            paydiscount = 10;
          }
          if (paydiscount < 1) {
            paydiscount = 10;
          }
          paydiscount = parseFloat(paydiscount).toFixed(2);
        }
        $(this).val(paydiscount);
        _self.calcPayAll();
      });
      _self.payallcalcObj.find('input.pay_youhui').on('change', function () {
        // 当优惠字段存在并且改变
        var pay_youhui = $(this).val();
        if (isNaN(pay_youhui)) {
          $(this).val('0.00');
          return;
        }
        pay_youhui = parseFloat(pay_youhui).toFixed(2);
        var all_heji = _self.edittableObj.find('tfoot').find('.all_heji').html();
        if (!isNaN(all_heji) && all_heji <= 0) {
          all_heji = 0;
        }
        all_heji = parseFloat(all_heji).toFixed(2);
        $(this).val(pay_youhui);
        var jisuancha = parseFloat(all_heji - pay_youhui);
        if (jisuancha < 0) {
          $.msglayer('超出合计范围~');
          $(this).val('0.00');
        }
        _self.calcPayAll();
      });
    },
    calcPayAll: function () {
      var _self = this;
      // ===============总报价
      var pay_allbaojia = _self.edittableObj.find('tfoot').find('.all_amount').html();
      if (!isNaN(pay_allbaojia) && pay_allbaojia <= 0) {
        pay_allbaojia = 0;
      }
      pay_allbaojia = parseFloat(pay_allbaojia).toFixed(2);
      _self.payallcalcObj.find('input.pay_allbaojia').val(pay_allbaojia);
      // ===============总折扣
      var pay_allzhekou = _self.edittableObj.find('tfoot').find('.all_disamount').html();
      if (!isNaN(pay_allzhekou) && pay_allzhekou <= 0) {
        pay_allzhekou = 0;
      }
      pay_allzhekou = parseFloat(pay_allzhekou).toFixed(2);
      _self.payallcalcObj.find('input.pay_allzhekou').val(pay_allzhekou);
      // ===============总优惠
      var pay_allyouhui = _self.edittableObj.find('tfoot').find('.all_youhui').html();
      if (!isNaN(pay_allyouhui) && pay_allyouhui <= 0) {
        pay_allyouhui = 0;
      }
      pay_allyouhui = parseFloat(pay_allyouhui).toFixed(2);
      _self.payallcalcObj.find('input.pay_allyouhui').val(pay_allyouhui);
      // 合计
      var all_heji = _self.edittableObj.find('tfoot').find('.all_heji').html();
      if (!isNaN(all_heji) && all_heji <= 0) {
        all_heji = 0;
      }
      all_heji = parseFloat(all_heji).toFixed(2);
      // ===============订单折扣
      var pay_discount = _self.payallcalcObj.find('input.pay_discount').val();
      pay_discount = parseFloat(pay_discount);
      if (isNaN(pay_discount) || pay_discount <= 0) {
        pay_discount = 10;
      }
      var pay_zhekou = all_heji * (1 - pay_discount * 0.1);
      pay_zhekou = pay_zhekou.toFixed(2);
      _self.payallcalcObj.find('input.pay_zhekou').val(pay_zhekou);
      // 总折扣
      pay_allzhekou = (parseFloat(pay_allzhekou) + parseFloat(pay_zhekou)).toFixed(2);
      _self.payallcalcObj.find('input.pay_allzhekou').val(pay_allzhekou);
      // ===============订单优惠
      var pay_youhui = _self.payallcalcObj.find('input.pay_youhui').val();
      pay_youhui = parseFloat(pay_youhui).toFixed(2);
      // 总优惠
      pay_allyouhui = (parseFloat(pay_allyouhui) + parseFloat(pay_youhui)).toFixed(2);
      _self.payallcalcObj.find('input.pay_allyouhui').val(pay_allyouhui);
      // 最终合计--pay-total应收或者应付
      var pay_total = pay_allbaojia - pay_allzhekou - pay_allyouhui;
      pay_total = parseFloat(pay_total).toFixed(2);
      _self.payallcalcObj.find('input.pay_total').val(pay_total);
    },
    ajaxLoadData: function () {
      var _self = this;
      _self.gundong();
      // 对象 拼接成 字符串
      var apiParam_arr = _self.settings.apiParam;
      var apiParam_str = '';
      for (var key in apiParam_arr) {
        apiParam_str += '&' + key + '=' + encodeURIComponent(apiParam_arr[key]);
      }
      var url_keyword = encodeURIComponent(_self.currLoadKeyword);
      $.ajax({
        type: 'GET',
        url: _self.settings.apiUrl + '?page=' + _self.currLoadPage + '&keyword=' + url_keyword + apiParam_str,
        dataType: 'json',
        success: function (result) {
          var lists = result.lists || '';
          var htmls = '';
          if (result.status == 200) {
            var lists = result.data;
            var htmls = '';
            // for ( var i in lists) {
            for (var i = 0; i < lists.length; i++) {
              // i 为数组的下标（序号），从0开始
              var rows = lists[i];
              htmls += '<div class="droplist-item" ';
              htmls += ' data-row_id ="' + rows.row_id + '"  ';
              htmls += ' data-row_units="' + rows.row_units + '" ';
              htmls += ' data-row_price ="' + rows.row_price + '"  ';
              htmls += ' data-row_cat="' + rows.row_cat + '"';
              htmls += ' data-row_rest="' + rows.row_rest + '"';
              htmls += ' data-row_name="' + rows.row_name + '"';
              htmls += ' data-row_attr_str="' + rows.row_attr_str + '"';
              htmls += ' data-row_attr_value="' + rows.row_attr_value + '"';
              htmls += ' title="' + rows.row_name + '"';
              htmls += ' alt="' + rows.row_name + '" >';
              htmls += '	<div class="name">' + rows.row_name;
              var attr_str = rows.row_attr_str || '';
              if (attr_str != '') {
                htmls += '<br>[ ' + attr_str + ' ]';
              }
              htmls += '</div>';
              htmls += '	<div class="price">' + rows.row_price + '</div>';
              if (attr_str != '') {
                htmls += '	<div class="rest">' + rows.row_attr_stock + '</div>';
              } else {
                htmls += '	<div class="rest">' + rows.row_rest + '</div>';
              }
              htmls += ' </div> ';
            }
            _self.droplistBoxObj.show();
            _self.currLoadPage++;
            if (_self.currLoadPage > result.maxpage) {
              _self.droplistBoxObj.find('.dropList').off('scroll');
            }
            _self.droplistBoxObj.find('.dropList').append(htmls);
          } else {
            _self.droplistBoxObj.hide();
            $.msglayer('没有商品');
          }
        }
      });
    },
    /* ========== 价格相关处理 ===================== */
    __pricelistHtmlAndBindFunc: function () {
      var _self = this;

      this.pricelistBoxObj = $('#' + this.settings.tabID + 'Price');
      if (_self.pricelistBoxObj.length <= 0) {
        var htmls = '';
        htmls += '<!-- B【 加载价格选择弹窗 @2017-11-06 cxt】-->';
        htmls += '<div class="cto-edittable-pricelist-wrap" id="' + this.settings.tabID + 'Price" style="display: none;">';
        htmls += ' <div class="pricelist-price-wrap" style="width: 100%;">';

        htmls += '<div class="pricelist-price">';
        htmls +=
          ' <div class="pricelist-gundong self_price" data-flag=".self_price" style="position: relative; overflow: auto; height: 182px; width: 50%; float: left;"></div>';
        htmls +=
          ' <div class="pricelist-gundong business_price" data-flag=".business_price" style="position: relative; overflow: auto; height: 182px; width: 50%; float: left;"></div>';
        htmls += '</div>';

        htmls += ' </div>';
        htmls += ' <div class="cto-clear"></div>';
        // htmls += ' <div class="pricelist-extra">其他提示</div>';
        htmls += '</div>';
        $('body').append(htmls);

        _self.pricelistBoxObj = $('#' + this.settings.tabID + 'Price');
      }

      // 绑定商品价格的选中按钮
      /* 绑定选中下拉选择弹窗单条数据 */
      _self.pricelistBoxObj.find('.pricelist-price').on('click', '.pricelist-item', function () {
        var isKeyupTrObj = _self.edittableObj.find('tr.item.isKeyupTr');
        // 当前价格，防止选中暂无数据的时候商品价格重置为0
        var now_price = isKeyupTrObj.find('td.row_price').find('input').val();
        /* 获取选中item参数 */
        var itemObj = $(this);
        var recent_price = itemObj.data('recent_price') || now_price;

        isKeyupTrObj.find('td.row_price').find('input').val(recent_price);
        isKeyupTrObj.find('input[name="detail_price[]"]').val(recent_price);
        _self.pricelistBoxObj.hide();
      });
    },
    __pricelistGundongFunc: function (goodsOptions) {
      /* ===== 绑定价格滚动监听 ===== */
      var _self = this;
      var nScrollHight = 0; // 滚动距离总长(注意不是滚动条的长度)
      var nScrollTop = 0; // 滚动到的当前位置

      var loadDataDivArr = _self.pricelistBoxObj.find('.pricelist-gundong');
      loadDataDivArr.each(function () {
        var gundong_item = $(this);
        var gundong_flag = gundong_item.data('flag');
        var load_type = '';
        if (gundong_flag == '.self_price') {
          load_type = 'self';
        } else if (gundong_flag == '.business_price') {
          load_type = 'others';
        }

        var gundong_options = $.extend(
          {},
          {
            page: 1, // 当前页数
            pageSize: 10, // 每页显示的个数
            keyword: '', // 当前关键字
            isLoad: true, // 是否禁止加载
            pageStart: 0, // 开始当前个数
            pageEnd: 0, // 结束当前个数
            gundong_flag: gundong_flag,
            load_type: load_type
          },
          goodsOptions
        );

        // 清空数据和初始化数据
        _self.pricelistBoxObj.find(gundong_options.gundong_flag).html('');
        _self.__pricelistAjaxLoadData(gundong_options);

        // 监听滚动事件
        var max_height = gundong_item.height();
        gundong_item.off('scroll').on('scroll', function (event) {
          nScrollHight = gundong_item[0].scrollHeight;
          nScrollTop = gundong_item[0].scrollTop;

          if (nScrollTop + max_height >= nScrollHight) {
            console.log('滚动条到底部了');
            _self.__pricelistAjaxLoadData(gundong_options);
            return;
          }
        });
      });
    },
    __pricelistAjaxLoadData: function (gundong_options) {
      var _self = this;
      _self.pricelistBoxObj.show();
      // 对象 拼接成 字符串
      var priceParam_arr = _self.settings.priceParam;
      var priceParam_str = '';
      for (var key in priceParam_arr) {
        priceParam_str += '&' + key + '=' + encodeURIComponent(priceParam_arr[key]);
      }

      priceParam_str += '&goods_id=' + gundong_options.goods_id;
      priceParam_str += '&goods_attr_value=' + gundong_options.goods_attr_value;
      priceParam_str += '&goods_attr_str=' + gundong_options.goods_attr_str;
      priceParam_str += '&load_type=' + gundong_options.load_type;

      $.ajax({
        type: 'GET',
        url: _self.settings.priceUrl + '?page=' + gundong_options.page + priceParam_str,
        dataType: 'json',
        success: function (result) {
          var htmls = '';
          if (result.status == 200) {
            var lists = result.data;
            for (var i = 0; i < lists.length; i++) {
              var rows = lists[i];
              htmls += '<div class="pricelist-item" data-recent_price="' + rows.recent_price + '">';
              htmls += '<div class="name">' + (rows.buyer_name || '&nbsp;') + '</div>';
              htmls += '<div class="name">' + (rows.car_code || '&nbsp;') + '</div>';
              htmls += '<div class="price">' + rows.recent_price + '</div>';
              htmls += '<div class="date">' + rows.order_indate + '</div>';
              htmls += '</div>';
            }
          } else {
            htmls += '<div class="pricelist-item">';
            htmls += '<div>暂无更多数据</div>';
            htmls += '</div>';
          }
          _self.pricelistBoxObj.show();

          gundong_options.page++;
          if (gundong_options.page > result.maxpage) {
            _self.pricelistBoxObj.find(gundong_options.gundong_flag).off('scroll');
          }
          _self.pricelistBoxObj.find(gundong_options.gundong_flag).append(htmls);
        }
      });
    }
  };
  // 在插件中使用ctoEdittableClass对象
  // 创建bsEdittableClass的实体
  // 调用默认方法
  // 支持链式调用
  // 我们都知道jQuery一个时常优雅的特性是支持链式调用，选择好DOM元素后可以不断地调用其他方法。
  // 要让插件不打破这种链式调用，只需return一下即可。
  $.fn.ctoEdittable = function (options) {
    var ctoEdittableObj = new ctoEdittableClass(this, options);
    ctoEdittableObj.index();
    return ctoEdittableObj;
  };
  $.ctoEdittable = function (options) {
    // 针对当前使用的系统进行配置。高级插件
    var defaults = {
      config: [
        ['type', 'row_num'],
        ['type', 'row_num']
      ],
      inputSubmitData: [
        {
          title: '数量',
          type: 'num',
          class: 'row_num',
          field: 'detail_num'
        }
      ]
    };
    var ctoEdittableObj = new ctoEdittableClass(this, options);
    ctoEdittableObj.index();
    return ctoEdittableObj;
  };
})(jQuery, window, document);
