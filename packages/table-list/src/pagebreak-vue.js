Vue.component('cto_vue_pagebreak', {
  template: `
	  <ul class="pagination">
	  <li v-show="current != 1" @click="current-- && pagegoto(current--)">
	  <a href="#">上一页</a>
	  </li>
	  <li v-for="index in pages" @click="pagegoto(index)" :class="{'active':current == index}" :key="index">
	  <a href="#">{{index}}</a>
	  </li>
	  <li v-show="allpage != current && allpage != 0 " @click="current++ && pagegoto(current++)">
	  <a href="#">下一页</a>
	  </li>
	  </ul>
  `,
  data: '',
  computed: {
    pages: function () {
      var pag = [];
      if (this.current < this.showItem) {
        // 如果当前的激活的项 小于要显示的条数
        // 总页数和要显示的条数那个大就显示多少条
        var i = Math.min(this.showItem, this.allpage);
        while (i) {
          pag.unshift(i--);
        }
      } else {
        // 当前页数大于显示页数了
        var xxxxx2 = Math.floor(this.showItem / 2);
        var middle = this.current - xxxxx2, // 从哪里开始
          i = this.showItem;
        if (middle > this.allpage - this.showItem) {
          middle = this.allpage - this.showItem + 1;
        }
        while (i--) {
          pag.push(middle++);
        }
      }
      return pag;
    }
  },
  created() {
    pagebreakData = ctoVueObj.getPagebreakData();
    this.current = pagebreakData.current;
    this.showItem = pagebreakData.showItem;
    this.allpage = pagebreakData.allpage;
  },
  mounted() {},
  methods: {
    pagegoto(index) {
      if (index == this.current) return;
      this.current = index;
      // 这里可以发送ajax请求
      ctoVueObj.getData(this.current);
    }
  }
});
