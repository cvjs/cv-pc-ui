import cvTableBase from './src/table-base.vue';
cvTableBase.install = function (Vue) {
  Vue.component(cvTableBase.name, cvTableBase);
};
import cvTableTree from './src/table-tree.vue';
cvTableTree.install = function (Vue) {
  Vue.component(cvTableTree.name, cvTableTree);
};
import cvTableEditor from './src/table-editor';
cvTableEditor.install = function (Vue) {
  Vue.component(cvTableEditor.name, cvTableEditor);
};
/**
 * 索引
 */
import cvTbColumnIndex from './src/column-index.vue';
cvTbColumnIndex.install = function (Vue) {
  Vue.component(cvTbColumnIndex.name, cvTbColumnIndex);
};
/**
 * 复选框
 */
import cvTbColumnCheck from './src/column-check.vue';
cvTbColumnCheck.install = function (Vue) {
  Vue.component(cvTbColumnCheck.name, cvTbColumnCheck);
};
/**
 * 常规文本
 */
import cvTbColumnText from './src/column-text.vue';
cvTbColumnText.install = function (Vue) {
  Vue.component(cvTbColumnText.name, cvTbColumnText);
};
/**
 * 图片
 */
import cvTbColumnImg from './src/column-img.vue';
cvTbColumnImg.install = function (Vue) {
  Vue.component(cvTbColumnImg.name, cvTbColumnImg);
};
/**
 * 操作
 */
import cvTbColumnOpt from './src/column-opt.vue';
cvTbColumnOpt.install = function (Vue) {
  Vue.component(cvTbColumnOpt.name, cvTbColumnOpt);
};
/**
 * 其他
 */
import cvTbColumnBase from './src/column-base.vue';
cvTbColumnBase.install = function (Vue) {
  Vue.component(cvTbColumnBase.name, cvTbColumnBase);
};
/**
 * 扩展
 */
import cvTbColumnExpand from './src/column-expand.vue';
cvTbColumnExpand.install = function (Vue) {
  Vue.component(cvTbColumnExpand.name, cvTbColumnExpand);
};
/**
 * 枚举
 */
import cvTbColumnEnum from './src/column-enum.vue';
cvTbColumnEnum.install = function (Vue) {
  Vue.component(cvTbColumnEnum.name, cvTbColumnEnum);
};

import cvTbEditSearch from './src/edit-search';
cvTbEditSearch.install = function (Vue) {
  Vue.component(cvTbEditSearch.name, cvTbEditSearch);
};

import cvTbEditOpt from './src/edit-opt';
cvTbEditOpt.install = function (Vue) {
  Vue.component(cvTbEditOpt.name, cvTbEditOpt);
};

import cvTbEditText from './src/edit-text';
cvTbEditText.install = function (Vue) {
  Vue.component(cvTbEditText.name, cvTbEditText);
};

import cvTbEditRadio from './src/edit-radio';
cvTbEditRadio.install = function (Vue) {
  Vue.component(cvTbEditRadio.name, cvTbEditRadio);
};

import cvTbEditCheckbox from './src/edit-checkbox';
cvTbEditCheckbox.install = function (Vue) {
  Vue.component(cvTbEditCheckbox.name, cvTbEditCheckbox);
};

import cvTbEditSelect from './src/edit-select';
cvTbEditSelect.install = function (Vue) {
  Vue.component(cvTbEditSelect.name, cvTbEditSelect);
};

export {
  cvTableBase,
  cvTableTree,
  cvTableEditor,
  /**
   * 子项操作
   */
  cvTbColumnBase,
  cvTbColumnExpand,
  cvTbColumnCheck,
  cvTbColumnIndex,
  cvTbColumnText,
  cvTbColumnImg,
  cvTbColumnOpt,
  cvTbColumnEnum,
  /**
   * 编辑
   */
  cvTbEditSearch,
  cvTbEditOpt,
  cvTbEditText,
  cvTbEditRadio,
  cvTbEditCheckbox,
  cvTbEditSelect
};
