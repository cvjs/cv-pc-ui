import { ExtendTable } from '/src/extend-ui.js';
export default {
  // 注入el-table-column组件实例，用于判断当前组件是
  // inject: {
  //   elTable: {
  //     default: ""
  //   }
  // },
  provide() {
    return {
      pageCurr: this.loadConfigSetting.pageCurr || 1,
      // page: this._getPageCurr(),
      pageLimit: this.loadConfigSetting.pageLimit || 10
    };
  },
  props: {
    ...ExtendTable.props,
    modelValue: {
      type: [Array, Object],
      // required: true,
      // default: () => {
      default() {
        return [];
      }
    },
    headerCellStyle: {
      type: [Function, Object],
      default: () => {
        return {
          // background: '#f8f8f9',
          // color: '#606266'
        };
      }
    }
  },
  data() {
    return {
      loadDataList: []
    };
  },
  watch: {
    modelValue(newVal) {
      this.loadDataList = newVal;
    },
    loadDataList(newVal) {
      this.$emit('update:modelValue', newVal);
    }
  },
  created() {
    this.loadDataList = this.modelValue;
  },
  methods: {
    _getPageCurr() {
      return this.loadPageCurr;
    },
    _getPageSize() {
      return this.loadPageLimit;
    },
    handleCurrentChange(e) {
      //监听页码
      this.loadPageCurr = e;
      this.getData();
    },
    handleSizeChange(e) {
      //监听页长
      this.loadPageLimit = e;
      this.getData();
    }
  }
};
