import cvShowText from './src/show-text.vue';
import cvShowImage from './src/show-image.vue';
import cvShowTip from './src/show-tip.vue';
import cvGallery from './src/gallery.vue';

cvShowText.install = function (Vue) {
  Vue.component(cvShowText.name, cvShowText);
};
cvShowImage.install = function (Vue) {
  Vue.component(cvShowImage.name, cvShowImage);
};
cvShowTip.install = function (Vue) {
  Vue.component(cvShowTip.name, cvShowTip);
};
cvGallery.install = function (Vue) {
  Vue.component(cvGallery.name, cvGallery);
};

export { cvShowText, cvShowImage, cvShowTip, cvGallery };
