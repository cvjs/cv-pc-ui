import cvCheckboxOptBase from './src/option-base.vue';
import cvCheckboxOptBtn from './src/option-button';
import cvCheckboxAll from './src/checkAll.vue';
import cvCheckboxGroup from './src/group.vue';

cvCheckboxOptBase.install = function (Vue) {
  Vue.component(cvCheckboxOptBase.name, cvCheckboxOptBase);
};
cvCheckboxOptBtn.install = function (Vue) {
  Vue.component(cvCheckboxOptBtn.name, cvCheckboxOptBtn);
};

cvCheckboxGroup.install = function (Vue) {
  Vue.component(cvCheckboxGroup.name, cvCheckboxGroup);
};
cvCheckboxAll.install = function (Vue) {
  Vue.component(cvCheckboxAll.name, cvCheckboxAll);
};

export { cvCheckboxOptBase, cvCheckboxOptBtn, cvCheckboxGroup, cvCheckboxAll };
