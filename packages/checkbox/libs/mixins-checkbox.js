import { ExtendCheckboxGroup } from '/src/extend-ui.js';
export default {
  props: {
    ...ExtendCheckboxGroup.props,
    modelValue: {
      type: [Array],
      default() {
        return [];
      }
    },
    dataLists: {
      type: Array,
      default() {
        return [];
      }
    },
    // 数据字段配置
    dataField: {
      type: [Object],
      default() {
        return {};
      }
    },
    disabled: {
      type: Boolean,
      default: false
    }
  },
  watch: {
    modelValue() {
      this.localVal = this.modelValue;
    },
    localVal() {
      this.handleChange();
    },
    dataLists: {
      handler() {
        this._parseLocalOpt();
      },
      immediate: true,
      deep: true
    }
  },
  data() {
    return {
      // 选中的选项
      localVal: this.modelValue,
      // 最终数据
      localOpt: []
    };
  },
  created() {
    // 进行数据处理
    this.handleChange();
    this._parseLocalOpt();
  },
  methods: {
    handleChange() {
      this.$emit('update:modelValue', this.localVal);
    },

    // 对传入的数据进行处理用于设置选项
    _parseLocalOpt() {
      let newArr = [];

      // 数组中是字符串时进行数据处理
      if (this.dataLists && this.dataLists.length !== 0) {
        if (typeof this.dataLists[0] === 'string') {
          this.dataLists.map((item) => {
            newArr.push({ label: item, value: item });
          });
          this.localOpt = newArr;
        } else {
          // 是否有 label 和 value 键值配置需求
          var fieldLabel = (this.dataField && this.dataField.label) || 'label';
          var fieldValue = (this.dataField && this.dataField.value) || 'value';

          this.dataLists.map((item) => {
            // 数值 0 会被判断为假，因此需要排除数值 0
            let lastLabel = item[fieldLabel] === 0 ? 0 : item[fieldLabel] || '';
            let lastVal = item[fieldValue] === 0 ? 0 : item[fieldValue] || '';
            lastVal = lastVal === 0 ? 0 : lastVal || lastLabel;

            newArr.push({
              label: lastLabel,
              value: lastVal,
              disabled: (item.disabled && true) || false
            });
            // 如果需要选中，帮忙选中
            if (item.checked == 'true' || item.checked == true) {
              if (typeof this.localVal == 'string') {
                this.localVal = lastVal;
              } else if (typeof this.localVal == 'array' || typeof this.localVal == 'object') {
                this.localVal.push(lastVal);
              }
            }
          });
          this.localOpt = newArr;
        }
      }
      this._parseLocalOptDiy();
    },
    _parseLocalOptDiy() {}
  }
};
