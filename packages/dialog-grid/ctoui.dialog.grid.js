/**
 * [前端高级编程][闭包插件] 键入搜索事件显示搜索框
 *
 * @author ctocode-zhw
 * @version 1.0.0.20171009_Beta
 * @copyright ctocode
 * @link http://www.ctocode.com
 */
(function ($, window, document, undefined) {
  'use strict';
  // 1.构造函数
  var ctoGridClass = function (ele, options) {
    // 添加属性
    this.$element = ele;
    this.settings = $.extend(
      {},
      {
        value: '', // 缺省值
        mode: 'remote',
        gridID: 'ctoGridDiv',
        gridLoadData: [],
        // "pluginID" : 'ctoGridDiv',
        width: 'auto',
        height: 'auto',
        event: 'click.editable',
        onblur: 'cancel',
        loadUrl: '',
        clearCallback: function (data, isSuccess) {
          // 这里可以写ajax内容，用于保存编辑后的内容
          // data: 返回的数据
          // isSuccess: 方法，用于保存数据成功后，将可编辑状态变为不可编辑状态
          // ajax请求成功（保存数据成功），才回调isSuccess函数（修改保存状态为编辑状态）
          // 清除回调函数
        },
        loadCallback: function (data, isSuccess) {},
        selectCallback: function (itemObj) {}
      },
      options
    );
    this.wrapObj = '';
  };
  // 方法--用prototype（原型）给一类的class添加方法
  ctoGridClass.prototype = {
    __bindFunc: function () {
      var _self = this;
      var settings = _self.settings;
      var ctoGrid_div = _self.wrapObj; /* 要显示的网格顶DIV */
      var ctoGrid_bd_wrap = ctoGrid_div.find('div.cto-grid-bd-wrap'); /* 网格内容部分 */
      var ctoGrid_bd = ctoGrid_bd_wrap.find('.cto-grid-table');
      var ctoGrid_bd_list = ctoGrid_bd.find('.cto-grid-list'); /* 网格内容列表 */

      var _eleObj = _self.$element;
      var _self_name = _eleObj.attr('name');
      var _self_id = _eleObj.attr('id');

      /* 绑定失去焦点时间---0927 */
      _eleObj.on('blur', function (e) {
        $(document).click(function (e) {
          if (!$(e.target).closest('#' + _self_id).length && !$(e.target).closest('.cto-grid-list').length) {
            ctoGrid_div.hide();
          }
        });
      });
      /* 设置选中item后的处理事件 0927 */
      ctoGrid_bd_list.on('click', '.item', function () {
        var forindex = $(this).data('forindex');
        var itemData = _self.gridLoadData[forindex];
        settings.selectCallback(itemData);
      });
      _eleObj.on('keyup', function (event) {
        /* 获取触发对象的位置 */
        var self_x = _eleObj.offsetLeft + 'px';
        var self_y = _eleObj.offsetTop + 'px';
        ctoGrid_div.css({
          top: parseInt(self_y) + parseInt(settings.top) + 'px',
          left: self_x
        });
        $(window).bind('resize', function () {
          self_x = _eleObj.offsetLeft + 'px';
          self_y = _eleObj.offsetTop + 'px';
          ctoGrid_div.css({
            top: parseInt(self_y) + parseInt('20') + 'px',
            left: self_x
          });
        });

        /* 上一次搜索内容 */
        var prev_keyword = _eleObj.data('keyword');
        prev_keyword = prev_keyword || '';
        /* 这次搜索内容 */
        var curr_keyword = _eleObj.val();
        curr_keyword = curr_keyword.replace(/[ ]/g, '');
        curr_keyword = $.trim(curr_keyword);
        // $(this).val(curr_keyword);
        /* 按了哪个键 */
        var key_action = event.keyCode;
        if (curr_keyword == prev_keyword) {
          return false;
        }
        if (curr_keyword == '') {
          ctoGrid_div.hide();
        } else {
          if (
            key_action != null &&
            key_action != 38 &&
            key_action != 40 &&
            key_action != 37 &&
            key_action != 39 &&
            key_action != 13
          ) {
            // 清空客户信息
            settings.clearCallback();
            ctoGrid_bd_list.find('.item').removeClass('on');
            ctoGrid_bd_list.attr('data-index', '');
            ctoGrid_bd_wrap.scrollTop(0);
            /* 请求加载并赋值，拼接 */
            _self.loadCallback(curr_keyword);
          }
          /* tr总条数 */
          var item_length = ctoGrid_bd_list.find('.item').length;
          /* 当前tr索引 */
          var item_index = ctoGrid_bd_list.attr('data-index');
          item_index = item_index || '';
          /* 先移除所有选中 */
          ctoGrid_bd_list.find('.item').removeClass('on');
          if (key_action == 38) {
            /* 向上按钮 */
            if (item_index == '') {
              item_index = 0;
            } else if (item_index == 0) {
              item_index = item_length - 1;
            } else {
              item_index = parseInt(item_index) - 1;
            }
            var curr_select_tr = ctoGrid_bd_list.children('.item').eq(item_index);
            if (item_index <= 2) {
              ctoGrid_bd_wrap.scrollTop(0);
            } else if (item_index > 2 && item_index < item_length - 2) {
              ctoGrid_bd_wrap.scrollTop(curr_select_tr.offsetTop - ctoGrid_bd_wrap.offsetTop + ctoGrid_bd_wrap.scrollTop() - 50);
            } else if (item_index == item_length - 1) {
              ctoGrid_bd_wrap.scrollTop(ctoGrid_bd_wrap.height() + curr_select_tr.offsetTop + ctoGrid_bd_wrap.offsetTop);
            }
            curr_select_tr.addClass('on');
          } else if (key_action == 40) {
            /* 向下按钮 */
            if (item_index == '') {
              item_index = 0;
            } else if (item_index == parseInt(item_length) - 1) {
              item_index = 0;
            } else {
              item_index = parseInt(item_index) + 1;
            }
            var curr_select_tr = ctoGrid_bd_list.children('.item').eq(item_index);
            if (item_index == 0) {
              ctoGrid_bd_wrap.scrollTop(0);
            } else if (item_index > 2) {
              ctoGrid_bd_wrap.scrollTop(curr_select_tr.offsetTop - ctoGrid_bd_wrap.offsetTop + ctoGrid_bd_wrap.scrollTop() - 50);
            }
            curr_select_tr.addClass('on');
          } else if (key_action == 13) {
            /* 触发方向键 回车键 end */
            ctoGrid_bd_list.children('.item').eq(item_index).trigger('click');
            return false;
          }
          ctoGrid_bd_list.attr('data-index', item_index);
        }
      });
    },
    // 调用默认方法
    index: function () {
      var _self = this;
      if ($(_self.settings.gridID).length <= 0) {
        _self.__loadHtml();
        _self.wrapObj = $('#' + _self.settings.gridID);
        _self.__bindFunc();
      }
      _self.wrapObj = $('#' + _self.settings.gridID);
    },
    /* 绑定加载数据 */
    loadCallback: function (curr_keyword) {
      var _self = this;
      var settings = _self.settings;
      var ctoGrid_div = _self.wrapObj; /* 要显示的网格顶DIV */
      var ctoGrid_bd_wrap = ctoGrid_div.find('div.cto-grid-bd-wrap'); /* 网格内容部分 */
      var ctoGrid_bd = ctoGrid_bd_wrap.find('.cto-grid-table');
      var ctoGrid_bd_list = ctoGrid_bd.find('.cto-grid-list'); /* 网格内容列表 */

      var _eleObj = _self.$element;

      $.ajax({
        url: settings.loadUrl + curr_keyword,
        dataType: 'json',
        success: function (result) {
          if (result.list != '') {
            ctoGrid_bd_list.html('');
            var itemList_html = '';
            _self.gridLoadData = result.data;

            for (var i in _self.gridLoadData) {
              var rows = _self.gridLoadData[i];
              itemList_html += '<tr class="item" ';
              itemList_html += ' data-forindex="' + i + '" ';
              itemList_html += '  style="height: 20px;cursor: pointer;">';
              /* 循环输出数据 */
              var title_data = settings.title;
              var titleLength = title_data.length;
              for (var j = 0; j < titleLength; j++) {
                var items = title_data[j];
                var width = items.width || '';
                var width_style = '';
                if (width != '') {
                  width_style = ' style="width: ' + width + 'px" ';
                }
                itemList_html += '  <td ' + width_style + ' >' + rows[items.field] + '</td>';
              }
              itemList_html += '</tr>';
            }
            ctoGrid_bd_list.html(itemList_html);
            ctoGrid_div.show();
          } else {
            ctoGrid_div.hide();
          }
        }
      });
    }
  };
  $.fn.ctoGrid = function (options) {
    var ctoClassObj = new ctoGridClass(this, options);
    ctoClassObj.index();
    return ctoClassObj;
  };
  $.ctoGrid = function (options) {
    var ctoClassObj = new ctoGridClass(this, options);
    ctoClassObj.index();
    return ctoClassObj;
  };
})($, window, document);
/**
 * @action ctocode 键入搜索事件
 * @author zhw
 * @version 2016-12-30
 */
/**
 * @<!-- 上下回车按键悬着框 -->
 * @1.s <div id="ctoGridDiv" class="cto-grid-wrap cto-grid-table-wrap" style="top: 20px; width: 320px;">
 * @2.1s <div class="cto-grid-hd-wrap"> <table class="cto-grid-hd"> <thead>
 *       <tr>
 *       <th style="width: 95px">车牌</th>
 *       <th style="width: 100px">客户名称</th>
 *       <th>手机号</th>
 *       </tr>
 *       </thead> </table>
 * @2.1e </div>
 * @2.2s <div class="cto-grid-bd-wrap">
 * @2.21 <table class="cto-grid-bd"> <tbody class="cto-grid-list"></tbody> </table>
 * @2.2e </div>
 * @1.e </div>
 *
 * @$("#bsJishi").ctoGrid({
 * @1 title : [
 * @1.1 { field : 'name', title : '车牌号', align : 'center', width : 96 },
 * @1.2 { field : 'carcode', title : '姓名', width : 100, align : 'center' },
 * @1.3 { field : 'mobile', title : '手机号', align : 'center' } ],
 * @2 onSelect : function(result) { console.log(result); } });
 */
