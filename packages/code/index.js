import cvCodeSms from './src/code-sms.vue';

cvCodeSms.install = function (Vue) {
  Vue.component(cvCodeSms.name, cvCodeSms);
};
export { cvCodeSms };
