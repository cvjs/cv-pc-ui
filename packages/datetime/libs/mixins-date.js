import { ExtendDatePicker } from '/src/extend-ui.js';
export default {
  props: {
    ...ExtendDatePicker.props,
    modelValue: {
      type: [String, Array, Number, Object, Date],
      default: null
    },
    // 快捷选项
    pickerOptions: {
      type: Boolean,
      default: false
    }
  },
  watch: {
    modelValue: {
      // 接收类型为时间戳时，将外部传入的时间戳精度转换为毫秒
      handler() {
        if (this.valueFormat == 'timestamp') {
          if (Array.isArray(this.modelValue)) {
            var localValue;
            localValue = this.modelValue.map((item) => {
              if (!isNaN(item) && item.toString().length == 10) return item * 1000;
            });
            // 外部发来的时间戳与内部不一致时，才对内部进行赋值操作
            if (this.localVal) {
              if (this.localVal[0] == localValue[0] && this.localVal[1] == localValue[1]) {
                return;
              }
            }
            this.localVal = localValue;
          } else if (this.modelValue && this.modelValue.toString().length == 10) {
            if (this.localVal != this.modelValue * 1000) this.localVal = this.modelValue * 1000;
          }
          return;
        }
        this.localVal = this.modelValue;
        console.log('----  this.localVal ----', this.localVal);
        // 当接收的 value 为时间戳，但未配置 valueFormat="timestamp" 时给予警告
        // if (Array.isArray(this.modelValue) && (!isNaN(this.modelValue[0]) || !isNaN(this.modelValue[1]))) {
        //   console.warn('[cv-pc-ui warn]: ', '传入的值是时间戳，需要配置格式 valueFormat="timestamp"');
        // } else if (!isNaN(this.modelValue)) {
        //   console.warn('[cv-pc-ui warn]: ', '传入的值是时间戳，需要配置格式 valueFormat="timestamp"');
        // }
      },
      immediate: true, // 初始化时立即执行一次
      deep: true
    },
    localVal() {
      // 返回值类型为时间戳时，将时间戳精度转换为秒再返回
      if (this.valueFormat == 'timestamp') {
        var localTimesTamp;
        if (Array.isArray(this.localVal)) {
          // （多个时间&&时间戳）精确度转换为秒 发往外部
          localTimesTamp = this.localVal.map((item) => {
            if (!isNaN(item) && item.toString().length == 13) return parseInt(item / 1000);
          });
          // 与外部发来的时间戳不一致时才向外发送新值
          if (this.modelValue) {
            if (localTimesTamp[0] == this.modelValue[0] && localTimesTamp[1] == this.modelValue[1]) {
              return;
            }
          }
          this.$emit('update:modelValue', localTimesTamp);
        } else if (!isNaN(this.localVal) && this.localVal.toString().length == 13) {
          // （单个日期或时间&&时间戳）精确度转换为秒 发往外部
          localTimesTamp = parseInt(this.localVal / 1000);
          if (this.modelValue != localTimesTamp) this.$emit('update:modelValue', localTimesTamp);
        }
        return;
      }
      this.$emit('update:modelValue', this.localVal);
    }
  },
  data() {
    return {
      localVal: ''
    };
  }
};
