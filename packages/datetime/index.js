import cvTimeBase from './src/time-base.vue';
import cvTimeRange from './src/time-range.vue';
import cvTimeSelect from './src/time-select.vue';
import cvDateBase from './src/date-base.vue';
import cvDateRange from './src/date-range.vue';
import cvDatetimeBase from './src/datetime-base.vue';
import cvDatetimeRange from './src/datetime-range.vue';

cvTimeBase.install = function (Vue) {
  Vue.component(cvTimeBase.name, cvTimeBase);
};
cvTimeRange.install = function (Vue) {
  Vue.component(cvTimeRange.name, cvTimeRange);
};
cvTimeSelect.install = function (Vue) {
  Vue.component(cvTimeSelect.name, cvTimeSelect);
};
cvDateBase.install = function (Vue) {
  Vue.component(cvDateBase.name, cvDateBase);
};
cvDateRange.install = function (Vue) {
  Vue.component(cvDateRange.name, cvDateRange);
};
cvDatetimeBase.install = function (Vue) {
  Vue.component(cvDatetimeBase.name, cvDatetimeBase);
};
cvDatetimeRange.install = function (Vue) {
  Vue.component(cvDatetimeRange.name, cvDatetimeRange);
};

export { cvTimeBase, cvTimeRange, cvTimeSelect, cvDateBase, cvDateRange, cvDatetimeBase, cvDatetimeRange };
