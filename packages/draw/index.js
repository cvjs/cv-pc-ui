import cvDrawCaptcha from './src/captcha.vue';
import cvDrawQrcode from './src/qrcode.vue';

cvDrawCaptcha.install = function (Vue) {
  Vue.component(cvDrawCaptcha.name, cvDrawCaptcha);
};
cvDrawQrcode.install = function (Vue) {
  Vue.component(cvDrawQrcode.name, cvDrawQrcode);
};
export { cvDrawCaptcha, cvDrawQrcode };
