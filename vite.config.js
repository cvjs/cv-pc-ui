import { resolve } from 'path';
import { defineConfig } from 'vite';
import vue from '@vitejs/plugin-vue';
import terser from '@rollup/plugin-terser';

import pkg from './package.json';

// import DefineOptions from 'unplugin-vue-define-options/vite';
const isPro = process.env.NODE_ENV === 'production';
const input = resolve('src'); // 入口文件
const output = resolve('dist'); // 输出文件

// https://vitejs.dev/config/
export default defineConfig({
  // 插件
  plugins: [
    vue()
    // DefineOptions(),
  ],
  // 项目根目录,项目部署的基础路径
  // root: './example/',
  // 环境配置
  mode: 'development',
  // 静态资源服务文件夹
  publicDir: 'public',
  server: {
    port: 8080, // 项目启动时，自定义端口
    open: false, // 项目启动时，是否自动浏览器打开
    host: '0.0.0.0', // 使用本地ip地址
    hmr: true // 开启热更新
  },
  // 全局变量替换 Record<string, string>
  define: {
    'process.env.NODE_ENV': `'${process.env.NODE_ENV}'`
  },
  optimizeDeps: {
    // 排除 __INDEX__
    exclude: ['__INDEX__']
  },
  resolve: {
    alias: [
      { find: '~', replacement: resolve('/example') },
      { find: '@', replacement: resolve('/src') }
      // "/": resolve(__dirname, "/"),
      //  '@': resolve(__dirname, 'src'),
      //  '@c': resolve(__dirname, 'src/components')
      // 'components': path.resolve(__dirname, 'src/components')
      // 为模块起一个别名，方便在项目中引用
    ],
    dedupe: [],
    // 情景导出package.json 配置中的 exports 字段
    conditions: [],
    // 解析package.json 中的字段
    mainFields: ['module', 'jsnext:main', 'jsnext'],
    // 导入时想要省略的扩展名列表
    extensions: ['.mjs', '.js', '.ts', '.jsx', '.tsx', '.json', '.vue']
  },
  css: {
    // 配置css modules 的行为， 选项被传递给postcss-modules
    modules: {},
    // PostCSS 配置（格式同postcss.config.js）
    // postcss-load-config 的插件配置
    postcss: {},
    // 指定传递给 CSS 预处理器的选项
    preprocessorOptions: {}
  },
  json: {
    // 是否支持从 .json 文件中进行按名导入
    namedExports: true,
    // 若设置为 true, 导入的 JSON 会被转换为 export default JSON.parse("...") 会比转译成对象字面量性能更好
    // 尤其是当 JSON 文件较大时
    // 开启此项， 则会禁用按名导入
    stringify: false
  },
  // 静态资源处理   字符串 || 正则表达式
  assetsInclude: '',
  // 调整控制台输出的级别 'info' | 'warn' | 'error' | 'silent'
  logLevel: 'info',
  // 设为 false 可以避免 Vite 清屏而错过在终端中打印某些关键信息
  clearScreen: true,
  // 打包
  build: {
    // 浏览器兼容性 ‘esnext’ | 'modules'
    target: 'modules',
    // 打包输出目录,输出路径
    outDir: 'lib',
    // 生成静态资源的存放路径
    assetsDir: 'assets',
    // 小于此阈值的导入或引用资源将内联为 base64 编码， 以避免额外的http请求， 设置为 0, 可以完全禁用此项，
    assetsInlineLimit: 4096,
    // 启动 / 禁用 CSS 代码拆分
    // cssCodeSplit: true, // 2024-0202 报错
    // 构建后是否生成 soutrce map 文件
    sourcemap: false,
    root: './demo.html',
    lib: {
      entry: resolve(__dirname, 'src/index.js'),
      name: 'cvUI',
      fileName: 'cv-ui',
      // cssFileName: 'style', // 默认和fileName一致
      // fileName: (format) => `cv-pc-ui.${format}.js`,
      // 输出格式
      // formats: ['es', 'cjs', 'umd']
      formats: ['es', 'cjs']
    },
    // 自定义底层的 Rollup 打包配置
    rollupOptions: {
      output: {
        exports: 'named',
        banner: `/*! cvPcUI v${pkg.version} */`,
        // 在 UMD 构建模式下为这些外部化的依赖提供一个全局变量
        globals: {
          vue: 'Vue',
          'element-plus': 'ElementPlus',
          'https://10ui.cn/jsoneditor/vanilla-jsoneditor@0.22.0/standalone.js': 'standalone_jsoneditor'
        }
        // manualChunks:(id){

        //   if (id.indexOf("node_modules/lodash/") !== -1) {
        //     return "lodash";
        //   }
        // },
        //   inlineDynamicImports: false,
        // manualChunks: {
        //   htmltomd: ['html-to-md'],
        //   codemirror: [
        //     'codemirror',
        //     // '@codemirror/autocomplete',
        //     '@codemirror/commands',
        //     '@codemirror/language',
        //     // '@codemirror/lint',
        //     // '@codemirror/search',
        //     '@codemirror/state',
        //     '@codemirror/view'
        //     // '@codemirror/legacy-modes'
        //   ],
        //   'codemirror-lang': [
        //     '@codemirror/lang-cpp',
        //     '@codemirror/lang-css',
        //     '@codemirror/lang-go',
        //     '@codemirror/lang-html',
        //     '@codemirror/lang-java',
        //     '@codemirror/lang-javascript',
        //     '@codemirror/lang-json',
        //     '@codemirror/lang-lezer',
        //     '@codemirror/lang-markdown',
        //     '@codemirror/lang-php',
        //     '@codemirror/lang-python',
        //     '@codemirror/lang-rust',
        //     '@codemirror/lang-sql',
        //     '@codemirror/lang-vue',
        //     '@codemirror/lang-xml'
        //   ]
        // }
      },
      // 打包过滤掉第三方库
      external: [
        'vue',
        'vue-router',
        'vue-router',
        'element-plus',
        '@element-plus/icons-vue',
        /**
         * 编辑器
         */
        'https://10ui.cn/jsoneditor/vanilla-jsoneditor@0.22.0/standalone.js',
        'html-to-md',
        'codemirror',
        // '@codemirror/autocomplete',
        '@codemirror/commands',
        '@codemirror/language',
        // '@codemirror/lint',
        // '@codemirror/search',
        '@codemirror/state',
        '@codemirror/view',
        '@codemirror/legacy-modes',
        '@codemirror/lang-cpp',
        '@codemirror/lang-css',
        '@codemirror/lang-go',
        '@codemirror/lang-html',
        '@codemirror/lang-java',
        '@codemirror/lang-javascript',
        '@codemirror/lang-json',
        '@codemirror/lang-lezer',
        '@codemirror/lang-markdown',
        '@codemirror/lang-php',
        '@codemirror/lang-python',
        '@codemirror/lang-rust',
        '@codemirror/lang-sql',
        '@codemirror/lang-vue',
        '@codemirror/lang-xml',
        '@ddietr/codemirror-themes',
        '@lezer/highlight'
      ],
      input: {
        '.': resolve(__dirname, 'src/index.js')
      },
      // output: {
      //   chunkFileNames: 'static/js/[name]-[hash].js',
      //   entryFileNames: 'static/js/[name]-[hash].js',
      //   assetFileNames: 'static/[ext]/name-[hash].[ext]'
      // }
      plugins: [
        terser({
          compress: {
            drop_console: true, // 如果你想去除掉 console 日志
            pure_funcs: ['console.log'] // 如果你想去除掉指定函数调用
          },
          format: {
            comments: false // 去除注释
          }
        })
      ]
    },
    // @rollup/plugin-commonjs 插件的选项
    commonjsOptions: {},
    // 设置为false 来禁用将构建好的文件写入磁盘
    write: true,
    // 默认情况下 若 outDir 在 root 目录下， 则 Vite 会在构建时清空该目录。
    emptyOutDir: true,
    // 启用 / 禁用 brotli 压缩大小报告
    brotliSize: false,
    // chunk 大小警告的限制
    chunkSizeWarningLimit: 500
    // minify: 'terser'
  },
  // 继承自 esbuild 转换选项， 最常见的用例是自定义 JSX
  esbuild: {
    drop: ['console', 'debugger']
    // jsxFactory: 'h',
    // jsxFragment: 'Fragment',
    // jsxInject: `import React from 'react'`
  }
});
