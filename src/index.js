// 统一注册所有组件
import { cvBadge } from '../packages/base/index.js';
import { cvRow, cvCol } from '../packages/layout/index.js';
import { cvBtnBase, cvBtnText } from '../packages/button/index.js';

/**
 * Form 表单
 */

// form表单容器
import { cvAutocompeteBase, cvAutocompeteLoad } from '../packages/autocomplete/index.js';
import { cvFormBase, cvFormInline, cvFormItem } from '../packages/form/index.js';

// input输入框
import {
  cvInputText,
  cvInputMobile,
  cvInputEmail,
  cvInputPassword,
  cvInputNumber,
  cvInputIdcard,
  cvInputDigit,
  cvTextarea,
  cvTextareabtn,
  cvInputBtn
} from '../packages/input/index.js';

import { cvInputCounter } from '../packages/input-counter/index';

import { cvShowText, cvShowImage, cvShowTip, cvGallery } from '../packages/show/index';

// radio单选框
import { cvRadioGroup, cvRadioOpt, cvRadioBtn } from '../packages/radio/index.js';
// checkbox多选框
import { cvCheckboxOptBase, cvCheckboxOptBtn, cvCheckboxGroup, cvCheckboxAll } from '../packages/checkbox/index.js';

// 单列选择器
import { cvGeoCity, cvGeoCity2 } from '../packages/geo/index.js';
import { cvSelectOpt, cvSelectBase, cvSelectTree } from '../packages/select/index.js';
// 级联选择器
import { cvCascaderBase } from '../packages/cascader/index.js';
// 日期时间选择
import {
  cvTimeBase,
  cvTimeRange,
  cvTimeSelect,
  cvDateBase,
  cvDateRange,
  cvDatetimeBase,
  cvDatetimeRange
} from '../packages/datetime/index.js';
// 开关
import { cvSwitchBase } from '../packages/switch/index';

// 验证码
import { cvCodeSms } from '../packages/code/index.js';

import { cvUploadAvatar, cvUploadImage, cvUploadFile, cvUploadVideo } from '../packages/upload/index';

/**
 * data 数据
 */
// 切换卡，跳转
import { cvTabsJump } from '../packages/tabs/index';
import { cvTabPane } from '../packages/tabs/index';

// 绘制
import { cvDrawCaptcha, cvDrawQrcode } from '../packages/draw/index.js';

// 标签
import { cvTagBase } from '../packages/tag/index';

import {
  // cvTransferBase,
  cvTransferTable
} from '../packages/transfer/index';

import {
  cvTableBase,
  cvTableTree,
  cvTableEditor,
  cvTbColumnIndex,
  cvTbColumnCheck,
  cvTbColumnText,
  cvTbColumnImg,
  cvTbColumnOpt,
  cvTbColumnBase,
  cvTbColumnExpand,
  cvTbColumnEnum,
  cvTbEditSearch,
  cvTbEditOpt,
  cvTbEditText,
  cvTbEditRadio,
  cvTbEditCheckbox,
  cvTbEditSelect
} from '../packages/table-list/index';

// 评论
import { cvCommentBase, cvCommentScroll, cvCommentNav } from '../packages/comment';
import { cvIcons, cvSvgs } from '../packages/icons';
import { cvChat } from '../packages/chat';
import { cvNoticeBar } from '../packages/notice-bar';
/**
 * 编辑器
 */
import {
  cvEditorCode,
  cvEditorJson,
  cvEditorTinymce,
  cvEditorMarkdown,
  cvMarkdownShow,
  cvMarkdownSimple,
  cvMarkdownNostyle
} from '../packages/editor/index.js';

var components = [
  cvBadge,
  cvRow,
  cvCol,
  /** */
  cvBtnBase,
  cvBtnText,
  /** */
  cvAutocompeteBase,
  cvAutocompeteLoad,
  /** */
  cvFormBase,
  cvFormInline,
  cvFormItem,
  /** */
  cvInputText,
  cvInputMobile,
  cvInputEmail,
  cvInputPassword,
  cvInputNumber,
  cvInputIdcard,
  cvInputDigit,
  cvInputBtn,
  /** */
  cvInputCounter,
  /** */
  cvTextarea,
  cvTextareabtn,
  /** */
  cvShowText,
  cvShowImage,
  cvShowTip,
  /** */
  cvRadioGroup,
  cvRadioOpt,
  cvRadioBtn,
  /** */
  cvCheckboxGroup,
  cvCheckboxOptBase,
  cvCheckboxOptBtn,
  cvCheckboxAll,
  /** */
  cvSelectOpt,
  cvSelectBase,
  cvSelectTree,
  cvCascaderBase,
  /** */
  cvTimeBase,
  cvTimeRange,
  cvTimeSelect,
  cvDateBase,
  cvDateRange,
  cvDatetimeBase,
  cvDatetimeRange,
  /** */
  cvSwitchBase,
  /** */
  cvUploadAvatar,
  cvUploadImage,
  cvUploadFile,
  cvUploadVideo,
  /** */
  cvTagBase,
  /** */
  // cvTransferBase,
  cvTransferTable,
  /** */
  cvCodeSms,
  /** */
  cvDrawCaptcha,
  cvDrawQrcode,

  /** 表格 */
  cvTableTree,
  cvTableBase,
  cvTableEditor,
  /** 表格编辑 */
  cvTbEditSearch,
  cvTbEditOpt,
  cvTbEditText,
  cvTbEditRadio,
  cvTbEditCheckbox,
  cvTbEditSelect,
  /** 表格展示 */
  cvTbColumnIndex,
  cvTbColumnCheck,
  cvTbColumnText,
  cvTbColumnImg,
  cvTbColumnOpt,
  cvTbColumnBase,
  cvTbColumnExpand,
  cvTbColumnEnum,
  /** */
  cvTabsJump,
  cvTabPane,
  /** */

  cvGeoCity,
  cvGeoCity2,
  cvCommentBase,
  cvCommentScroll,
  cvCommentNav,
  cvIcons,
  cvSvgs,
  cvChat,
  cvNoticeBar,
  /**
   * 编辑器
   */
  cvEditorCode,
  cvEditorJson,
  cvEditorTinymce,
  cvEditorMarkdown,
  cvMarkdownShow,
  cvMarkdownSimple,
  cvMarkdownNostyle
];

import { cvDialogRender, cvDrawerRender } from '../packages/dialog-render/index.js';
export { cvDialogRender, cvDrawerRender };

import { commonCopy } from './utils/index.js';
const install = function (Vue, opts = {}) {
  components.forEach((component) => {
    Vue.component(component.name, component);
  });
  Vue.config.globalProperties.$cvUiParams = {
    // zIndex: opts.zIndex || 2000
    // size: opts.size || 'default',
    ...opts
  };
  // Vue.config.globalProperties.$cvDialog = cvDialog
  Vue.config.globalProperties.$cvDialogRender = function () {
    return cvDialogRender(Vue, ...arguments);
  };
  Vue.config.globalProperties.$cvCopy = commonCopy;
  Vue.config.globalProperties.$cvDrawerRender = function () {
    return cvDrawerRender(Vue, ...arguments);
  };
  Vue.config.globalProperties.CDN_URL_UI = import.meta.env.VITE_UI_CDN || 'https://10ui.cn/';
};
// import './assets/cvjs.css';
/* istanbul ignore if */
// 通过use方式全部引入
if (typeof window !== 'undefined' && window._vueApp_) {
  install(window._vueApp_);
}

const cvPcUi = {
  version: '0.3.x',
  install: install
};
export { install, components };

export default cvPcUi;
