import { ExtendMessage } from '../extend-ui';
export function commonCopy(data) {
  const needContent = data;
  const oInput = document.createElement('textarea');
  oInput.value = needContent;
  document.body.appendChild(oInput);
  oInput.select(); // 选择对象;
  // console.log(oInput.value)
  document.execCommand('Copy'); // 执行浏览器复制命令
  ExtendMessage({
    message: '复制成功',
    type: 'success'
  });
  oInput.remove();
}
