/**
 * 动态加载css文件
 * @param {*} url
 * @param {*} isCache
 */
export function loadCdnCss(url, isCache = false) {
  let element = document.createElement('link');
  element.setAttribute('rel', 'stylesheet');
  element.setAttribute('type', 'text/css');
  element.setAttribute('id', url);
  if (isCache) {
    element.setAttribute('href', url + '?t=' + new Date().getTime());
  } else {
    element.setAttribute('href', url);
  }
  const existingLink = document.getElementById(url);
  if (existingLink) {
    // resolve();
  } else {
    document.head.appendChild(element);
    // element.onload = () => {
    //   resolve();
    // };
  }
}

/**
 * 动态加载js文件
 * @param {*} url
 * @param {*} isCache
 */
export function loadCdnScript(url, isCache = false, isOnly = true) {
  return new Promise((resolve, reject) => {
    const script = document.createElement('script');
    url = url.replace(/([^:]\/)\/+/g, '$1');

    // script.src = url;
    script.id = url;
    script.type = 'text/javascript';
    if (isCache) {
      script.src = url + '?t=' + new Date().getTime();
    } else {
      script.src = url;
    }

    const existingScript = document.getElementById(url);
    if (existingScript && isOnly) {
      resolve();
    } else {
      document.body.appendChild(script);
      script.onload = () => {
        resolve();
      };
    }
  });
}
