import attrsMixin from './attrsMixin';
import utils from './formMixin/utils';

export default {
  inheritAttrs: false,
  components: {},
  mixins: [attrsMixin],
  props: {
    modelValue: {},
    _disabled: Boolean,
    options: {
      type: Array,
      default() {
        return [];
      }
    },
    desc: {
      type: Object,
      default() {
        return {};
      }
    }
  },
  watch: {
    // 当值发生变化时
    modelValue: {
      handler(newVal) {
        if (this.desc.displayFormatter) {
          newVal = this.desc.displayFormatter(newVal);
        }

        if (this.checkType(newVal)) {
          if (this.customInit) {
            newVal = this.customInit(newVal);
          }
          this.newValue = newVal;
        }
      },
      immediate: true
    },
    // 检测默认值
    'desc.default': {
      handler() {
        const value = this.modelValue;
        const isArr = Array.isArray(value);
        // 值为空
        if (utils.isUnDef(value) || value === '' || (isArr && value.length === 0)) {
          // 默认值不为空
          if (utils.isDef(this.desc.default)) {
            // 默认值类型检查
            if (this.checkType && !this.checkType(this.desc.default, true)) {
              return;
            }

            this.newValue = this.desc.default;
            this.handleChange(this.newValue);
          }
        }
      },
      immediate: true
    }
  },
  data() {
    return {
      // 默认事件
      defaultOn: {
        input: this.handleChange
      },
      // 新值, 因为Vue不能修改props传递过来的值, 所以借助新的值进行传递
      newValue: this.modelValue
    };
  },
  methods: {
    // 变化处理
    handleChange(newVal) {
      this.$emit('update:modelValue', newVal);
    },

    // 检查类型
    checkType(val, isDefault = false) {
      if (this.type && utils.isDef(val)) {
        const isOk = utils.is(val, this.type);
        if (!isOk) {
          // eslint-disable-next-line
          console.warn(
            '[cv-pc-ui warn]: ',
            `${this.desc.label} 的${isDefault ? '默认' : ''}值类型错误, 期望 [${this.type}] 类型, 但`,
            val,
            '不是该类型, 请检查'
          );
        }
        return isOk;
      } else {
        return true;
      }
    }
  }
};
