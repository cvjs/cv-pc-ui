export default {
  props: {
    compConfig: {},
    // 数据回调，解析
    dataParse: {
      type: [Function],
      default: () => {
        return null;
      }
    }
    // configResponse:{}
    // dataResponse: {}
  },
  data() {
    return {
      searchData: {},
      loadDataLoading: false,
      loadDataList: [], //表格数据
      loadDataTotal: 0,

      loadPageCurr: 1,
      loadPageLimit: 10,

      loadConfigSetting: {},
      loadConfigDef: {
        pageCurr: 1,
        pageLimit: 10,
        isSkip: false,
        //默认配置
        url: null, //接口地址
        apiFunc: null, //接口地址
        mockData: null, // 模拟数据
        where: {}, //接口参数
        /**
         * 数据返回格式
         */
        response: {
          statusName: 'status', //数据状态字段名
          statusCode: 200, //成功状态码
          msgName: 'msg', //状态信息字段名称
          countName: 'total', //数据总数字段名称
          dataName: 'data' //数据列表字段名称
        },
        /**
         * 请求分页
         */
        //用于对分页请求参数：page、limit重新设定名称
        request: {
          pageName: 'page', //页码参数名称
          limitName: 'pagesize' //页长参数名称
        }
      }
    };
  },
  methods: {
    loadEnding() {
      this.loadDataLoading = false;
      this.$nextTick(() => {
        // 以服务的方式调用的 Loading 需要异步关闭
        this.loadDataLoading = false;
      });
    },
    loadGetData() {
      this.loadDataLoading = true;

      if (this.loadConfigSetting['apiFunc'] && typeof this.loadConfigSetting['apiFunc'] == 'function') {
        const loadConfigSetting = this.loadConfigSetting;
        let whereData = { ...loadConfigSetting.where };
        whereData[loadConfigSetting.request.pageName] = this.loadPageCurr;
        whereData[loadConfigSetting.request.limitName] = this.loadPageLimit;
        //处理搜索框参数
        whereData = Object.assign(whereData, this.searchData);

        /**
         * 不清空，如果要清空，请在上级清空
         */
        //请求数据前先清空旧的列表数据
        // this.loadDataList = [];
        /**
         * 请求数据
         */
        this.loadConfigSetting['apiFunc'](whereData)
          .then((getRR) => {
            // 参数变动组件内部会把页码重置成1，所以这里重新赋值实际页码
            this.loadPageCurr = whereData[loadConfigSetting.request.pageName];
            this.loadEnding();
            let getDataApi = this.sortOutData(getRR);
            if (getDataApi.status != loadConfigSetting.response.statusCode) {
              ExtendMessage({
                message: getDataApi.msg,
                type: 'error',
                duration: 1300
              });
              return;
            }
            if (getDataApi.data.length === 0) {
              ExtendMessage({
                message: '没有更多数据了',
                type: 'warning',
                duration: 1300
              });
              return;
            }
            let lastDataList = getDataApi[loadConfigSetting.response.dataName] || [];
            if (this.dataParse && typeof this.dataParse == 'function') {
              lastDataList = this.dataParse.call(null, lastDataList);
            }
            this.loadDataList.push(...lastDataList);
            this.loadDataTotal = getDataApi[loadConfigSetting.response.countName] || 0;
          })
          .catch((result) => {
            this.loadEnding();
          });
      } else {
        if (this.loadDataList.length > 0) {
          this.loadDataTotal = this.loadDataList.length;
        }
        this.loadEnding();
      }
    },
    //整理数据
    sortOutData(e) {
      let response = this.loadConfigSetting.response,
        data = {},
        list = [],
        newList = [];
      /* 整理自定义数据返回格式 */
      data.status = e[response.statusName];
      data.msg = e[response.msgName];
      data.total = e[response.countName];
      list = e[response.dataName];
      for (let key in list) {
        newList[key] = list[key];
      }
      data.data = newList;
      return data;
    }
  }
};
