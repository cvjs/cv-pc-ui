export default {
  props: {
    modelVal: {}
  },
  watch: {
    modelVal: {
      handler(newVal) {
        this.localVal = newVal;
      }
    }
  },
  data() {
    return {
      localVal: ''
    };
  },
  methods: {
    onUpdateValue() {
      this.$emit('update:modelVal', this.localVal);
    }
  }
};
