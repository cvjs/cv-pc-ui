/**
 * 转换字符串，为null转换为空字符串
 * @param val
 * @returns
 */
export function str(val) {
  if (val == null) {
    return '';
  }
  return String(val);
}

/**
 * 判断是否是图片类型
 * @param filePath
 * @returns
 */
export function isImage(filePath) {
  let type = ['png', 'jpg', 'jpeg', 'gif', 'webp', 'svg'];
  // 获取最后一个.的位置
  let index = filePath.lastIndexOf('.');
  //获取后缀
  let suffix = filePath.substring(index + 1);
  return type.indexOf(suffix.toLowerCase()) != -1;
}

//获取文件url
export function createObjectURL(blob) {
  if (window.URL) {
    return window.URL.createObjectURL(blob);
  } else if (window.webkitURL) {
    return window.webkitURL.createObjectURL(blob);
  } else {
    return '';
  }
}

export function dateDiff(currDate) {
  // 获取当前时间
  var currentTime = new Date();
  // 假设目标时间是一个已知的时间戳
  var targetTime = new Date(currDate);
  // 计算时间差（单位：毫秒）
  var timeDiff = currentTime - targetTime;
  // 将时间差转换为秒、分钟、小时
  var seconds = Math.floor(timeDiff / 1000);
  var minutes = Math.floor(seconds / 60);
  var hours = Math.floor(minutes / 60);
  // 判断时间差是否小于1分钟，显示几秒前
  if (minutes < 1) {
    var result = seconds + '秒前';
  }
  // 判断时间差是否小于1小时，显示几分钟前
  else if (hours < 1) {
    var result = minutes + '分钟前';
  }
  // 判断时间差是否小于24小时，显示几小时前
  else if (hours < 24) {
    var result = hours + '小时前';
  } else {
    // 超过24小时，显示具体时间
    var result = targetTime.toLocaleString();
  }
  return result;
}
