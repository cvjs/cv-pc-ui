# 十云出品

基于
- vue3
- vite
- element-plus
- volar


官网：[https://www.10yun.com/](https://www.10yun.com/)

## cv-pc-ui 电脑端ui组件

可选：
- 基于 elemnt-plus 的二次封装
- 基于 antd 的二次封装
- 最基础封装

手册文档：[https://cvjs.cn/cv-pc-ui](https://cvjs.cn/cv-pc-ui)




## Recommended IDE Setup

- [VS Code](https://code.visualstudio.com/) + [Volar](https://marketplace.visualstudio.com/items?itemName=Vue.volar)