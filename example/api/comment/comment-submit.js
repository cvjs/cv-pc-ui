import { ElMessage } from 'element-plus';
import { createObjectURL, throttle } from '@/util';

export const useCommentSubmit = (config) => {
  let temp_id = 100;
  // 提交评论事件
  // SubmitParamApi
  const commentSubmitApi = ({ content, parentId, files, finish, reply }) => {
    let str = '提交评论:' + content + ';\t父id: ' + parentId + ';\t图片:' + files + ';\t被回复评论:';
    console.log(str, reply);
    // let str = '提交评论:' + content + ';\t父id: ' + parentId + ';\t图片:' + files + ';\t被回复评论:' + reply;
    // console.log(str);
    console.log('提交评论: ' + content, parentId, files);
    /**
     * 上传文件后端返回图片访问地址，格式以'||'为分割; 如:  '/static/img/program.gif||/static/img/normal.webp'
     */
    let contentImg = files?.map((e) => createObjectURL(e)).join('||');

    temp_id += 1;
    const comment = {
      id: String(temp_id),
      parentId: parentId,
      uid: config.user.id,
      address: '来自江苏',
      content: content,
      likes: 0,
      createTime: '2023-05-05 06:00:00',
      createTime: '1分钟前',
      contentImg: contentImg,
      user: {
        username: config.user.username,
        avatar: config.user.avatar,
        level: 6,
        homeLink: `/${temp_id}`
      },
      reply: null
    };
    setTimeout(() => {
      console.log(comment);
      finish(comment);
      ElMessage({ message: '评论成功!', type: 'info' });
    }, 200);
  };
  return {
    commentSubmitApi
  };
};
