const replyList = [
  {
    content: `7年之后可以选择 续婚 也可以选择 离婚`,
    user: {
      username: '评论者1',
      avatar: 'https://f.10cdn.cn/default/default.png'
    }
  },
  {
    content: `万般皆虾球，惟有读书高`,
    user: {
      username: '评论者2',
      avatar: 'https://f.10cdn.cn/default/default.png'
    }
  },
  {
    content: `想改变一个人几乎是不可能，所以只能在前行的路上寻找志同道合的朋友💪`,
    user: {
      username: '评论者3',
      avatar: 'https://f.10cdn.cn/default/default.png'
    }
  },
  {
    content: `vue3相比vue2多了setup方法,在setup方法中this是undefined,取值很不方便,因此需要修改之前的写法,常用的有以下几点 setup中使用prop中的值 props: ["tabsItem","activeTab","activeN...1`,
    user: {
      username: '萬般皆是命',
      avatar: 'https://f.10cdn.cn/default/default.png'
    }
  },
  {
    id: '21',
    parentId: '2',
    uid: '3',
    address: '来自重庆',
    content: '说的对[大笑2]，所以，综上所述，上课睡觉不怪我呀💤',
    likes: 3,
    createTime: '2023-04-28 10:00',
    user: {
      username: '别扰我清梦*ぁ',
      avatar:
        'https://static.juzicon.com/user/avatar-8b6206c1-b28f-4636-8952-d8d9edec975d-191001105631-MDTM.jpg?x-oss-process=image/resize,m_fill,w_100,h_100',
      level: 5,
      homeLink: '/21'
    }
  },
  {
    id: '22',
    parentId: '2',
    uid: '4',
    content:
      '回复 <span style="color: var(--cv-color-success-dark-2);">@别扰我清梦*ぁ:</span> 看完打了一个哈切。。。会传染。。。[委屈]',
    address: '来自广州',
    likes: 9,
    createTime: '2023-04-28 10:00',
    user: {
      username: 'Blizzard',
      avatar:
        'https://static.juzicon.com/user/avatar-3cb86a0c-08e7-4305-9ac6-34e0cf4937cc-180320123405-BCV6.jpg?x-oss-process=image/resize,m_fill,w_100,h_100',
      level: 3,
      homeLink: '/22'
    }
  }
];

// export const comment = []as CommentApi[]
export const commentData = [
  {
    id: '1',
    parentId: null,
    uid: '1',
    address: '来自上海',
    content:
      '缘生缘灭，缘起缘落，我在看别人的故事，别人何尝不是在看我的故事?别人在演绎人生，我又何尝不是在这场戏里?谁的眼神沧桑了谁?我的眼神，只是沧桑了自己[喝酒]',
    likes: 2,
    contentImg: '/static/img/program.gif',
    contentImg: '/static/img/normal.webp',
    contentImg: 'https://gitee.com/undraw/undraw-ui/raw/master/public/docs/normal.webp',
    createTime: '2023-04-30 16:22',
    user: {
      username: '落🤍尘',
      avatar: 'https://static.juzicon.com/avatars/avatar-200602130320-HMR2.jpeg?x-oss-process=image/resize,w_100',
      level: 6,
      homeLink: '/1'
    }
  },
  {
    id: '2',
    parentId: null,
    uid: '2',
    address: '来自苏州',
    content: '知道在学校为什么感觉这么困吗？[大笑2]因为学校，是梦开始的地方。[脱单doge]',
    likes: 11,
    createTime: '1天前',
    createTime: '2023-04-28 09:00',
    user: {
      username: '悟二空',
      avatar: 'https://static.juzicon.com/user/avatar-bf22291e-ea5c-4280-850d-88bc288fcf5d-220408002256-ZBQQ.jpeg',
      level: 1,
      homeLink: '/2'
    },
    reply: {
      total: 2,
      list: replyList
    }
  },
  {
    id: '3',
    parentId: null,
    uid: '5',
    address: '来自北京',
    content: '人的一切痛苦，本质上都是对自己的无能的愤怒。',
    likes: 34116,
    createTime: '2023-04-27 09:00',
    user: {
      username: '半个句号',
      avatar:
        'https://static.juzicon.com/user/avatar-0d70406e-5d4a-4107-a689-652ffd063f99-200425180341-1QK6.jpg?x-oss-process=image/resize,m_fill,w_100,h_1000',
      level: 5,
      homeLink: '/3'
    },
    reply: {
      total: 2,
      list: [
        {
          id: '31',
          uid: '6',
          parentId: '3',
          address: '来自成都',
          content: '人生就像愤怒的小鸟，当你失败时，总有几只猪在笑。',
          likes: 7,
          createTime: '2023-04-28 09:00',
          user: {
            username: '陆呈洋',
            avatar:
              'https://static.juzicon.com/avatars/avatar-20220310090547-fxvx.jpeg?x-oss-process=image/resize,m_fill,w_100,h_100',
            level: 4,
            homeLink: '/31'
          }
        },
        {
          id: '32',
          parentId: '3',
          uid: '7',
          address: '来自杭州',
          content: '深思熟虑的结果往往就是说不清楚。',
          likes: 3,
          createTime: '2023-04-28 10:00',
          user: {
            username: '哑谜',
            avatar:
              'https://static.juzicon.com/avatars/avatar-190919180152-2VDE.jpg?x-oss-process=image/resize,m_fill,w_100,h_100',
            level: 3,
            homeLink: '/32'
          }
        }
      ]
    }
  },
  {
    id: '4',
    parentId: null,
    uid: '14',
    address: '来自杭州',
    content:
      '鱼说：我时时刻刻睁开眼睛，就是为了能让你永远在我眼中！<br>水说：我时时刻刻流淌不息，就是为了能永远把你拥抱！！<br>锅说：都快熟了，还这么贫。',
    likes: 13,
    createTime: '2023-03-28 13:00',
    user: {
      username: 'Blizzard1',
      avatar:
        'https://static.juzicon.com/user/avatar-3cb86a0c-08e7-4305-9ac6-34e0cf4937cc-180320123405-BCV6.jpg?x-oss-process=image/resize,m_fill,w_100,h_100',
      level: 3,
      homeLink: '/4'
    },
    reply: {
      total: 2,
      list: [
        {
          id: '41',
          parentId: '4',
          uid: '15',
          address: '来自北京',
          content: '鱼对水说，你看不到我流泪，因为我在水中。水对鱼说，我看到你悲伤，因为你在我心中。[呲牙]',
          likes: 36,
          createTime: '2023-04-01 13:00',
          user: {
            username: '过往~',
            avatar:
              'https://static.juzicon.com/avatars/avatar-20210308112705-zqf0.jpeg?x-oss-process=image/resize,m_fill,w_100,h_100',
            level: 4,
            homeLink: '/41'
          }
        },
        {
          id: '42',
          parentId: '4',
          uid: '16',
          address: '来自杭州',
          content: '约束条件变了，原来的收益，一下子都变为成本。生命如果架在锅上，成本自然也就很高了[tv_微笑]',
          likes: 16,
          createTime: '2023-04-01 15:00',
          user: {
            username: 'Blizzard1',
            avatar:
              'https://static.juzicon.com/user/avatar-3cb86a0c-08e7-4305-9ac6-34e0cf4937cc-180320123405-BCV6.jpg?x-oss-process=image/resize,m_fill,w_100,h_100',
            level: 3,
            homeLink: '/42'
          }
        }
      ]
    }
  },

  {
    id: '4',
    parentId: null,
    uid: '14',
    address: '来自杭州',
    content:
      '鱼说：我时时刻刻睁开眼睛，就是为了能让你永远在我眼中！<br>水说：我时时刻刻流淌不息，就是为了能永远把你拥抱！！<br>锅说：都快熟了，还这么贫。',
    likes: 13,
    createTime: '2023-04-29 14:00',
    user: {
      username: 'Blizzard1',
      avatar:
        'https://static.juzicon.com/user/avatar-3cb86a0c-08e7-4305-9ac6-34e0cf4937cc-180320123405-BCV6.jpg?x-oss-process=image/resize,m_fill,w_100,h_100',
      level: 3,
      homeLink: '/4'
    }
  },
  {
    id: '5',
    parentId: null,
    uid: '14',
    address: '来自杭州',
    content:
      '鱼说：我时时刻刻睁开眼睛，就是为了能让你永远在我眼中！<br>水说：我时时刻刻流淌不息，就是为了能永远把你拥抱！！<br>锅说：都快熟了，还这么贫。',
    likes: 13,
    createTime: '2023-04-30 17:00',
    user: {
      username: 'Blizzard1',
      avatar:
        'https://static.juzicon.com/user/avatar-3cb86a0c-08e7-4305-9ac6-34e0cf4937cc-180320123405-BCV6.jpg?x-oss-process=image/resize,m_fill,w_100,h_100',
      level: 3,
      homeLink: '/4'
    }
  }
];
