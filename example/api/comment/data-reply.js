// const reply as ReplyApi;
export const replyData = {
  total: 6,
  list: [
    {
      id: '31',
      parentId: '3',
      uid: '6',
      address: '来自成都',
      content: '人生就像愤怒的小鸟，当你失败时，总有几只猪在笑。',
      likes: 7,
      createTime: '2023-04-29 14:00',
      createTime: '1天前',
      user: {
        username: '陆呈洋',
        avatar:
          'https://static.juzicon.com/avatars/avatar-20220310090547-fxvx.jpeg?x-oss-process=image/resize,m_fill,w_100,h_100',
        level: 4,
        homeLink: '/31'
      }
    },
    {
      id: '32',
      parentId: '3',
      uid: '7',
      address: '来自杭州',
      content: '深思熟虑的结果往往就是说不清楚。',
      likes: 3,
      createTime: '2023-04-29 15:00',
      createTime: '2天前',
      user: {
        username: '哑谜',
        avatar: 'https://static.juzicon.com/avatars/avatar-190919180152-2VDE.jpg?x-oss-process=image/resize,m_fill,w_100,h_100',
        level: 3,
        homeLink: '/32'
      }
    },
    {
      id: '33',
      parentId: '3',
      uid: '8',
      level: 2,
      address: '来自深圳',
      content: '当我跨过沉沦的一切，向着永恒开战的时候，你是我的军旗。',
      createTime: '2023-04-29 17:00',
      createTime: '5天前',
      user: {
        username: 'Mia',
        avatar: 'https://static.juzicon.com/avatars/avatar-190919181554-L2ZO.jpg?x-oss-process=image/resize,m_fill,w_100,h_100',
        likes: 3,
        homeLink: '/33'
      }
    },
    {
      id: '34',
      parentId: '3',
      uid: '9',
      address: '来自西安',
      likes: 34,
      content: '不要由于别人不能成为我们所希望的人而愤怒，因为我们自己也难以成为自己所希望的人。',
      createTime: '2023-04-29 14:00',
      createTime: '1天前',
      user: {
        username: 'poli301',
        avatar: 'https://static.juzicon.com/avatars/avatar-190919180043-XPLP.jpg?x-oss-process=image/resize,m_fill,w_100,h_100',
        level: 4,
        homeLink: '/34'
      }
    },
    {
      id: '35',
      parentId: '3',
      uid: '10',
      likes: 32,
      address: '来自武汉',
      content: '世上莫名其妙走霉运的人多的是，都是一边为命运生气，一边化愤怒为力量地活着。',
      createTime: '2023-04-29 14:00',
      createTime: '11小时前',
      user: {
        username: 'fish_eno',
        avatar: 'https://static.juzicon.com/avatars/avatar-190919180320-NAQJ.jpg?x-oss-process=image/resize,m_fill,w_100,h_100',
        level: 6,
        link: '/35',
        username: 'poli301',
        avatar: 'https://static.juzicon.com/avatars/avatar-190919180043-XPLP.jpg?x-oss-process=image/resize,m_fill,w_100,h_100',
        level: 4,
        homeLink: '/34'
      }
    },
    {
      id: '36',
      parentId: '3',
      uid: '11',
      likes: 21,
      address: '来自上海',
      content: '这世上所有的不利情况，都是当事者能力不足造成的',
      createTime: '2023-04-29 14:00',
      createTime: '10小时前',
      user: {
        username: '十三',
        avatar:
          'https://static.juzicon.com/user/avatar-f103e42d-a5c9-4837-84e3-d10fad0bcb36-210108053135-E90E.jpg?x-oss-process=image/resize,m_fill,w_100,h_100',
        level: 4,
        homeLink: '/36'
      }
    },
    {
      id: '37',
      parentId: '3',
      uid: '12',
      likes: 18,
      address: '来自广州',
      content: ' 绝望自有绝望的力量，就像希望自有希望的无能。',
      createTime: '2023-04-30 14:00',
      createTime: '9小时前',
      user: {
        username: 'D.z.H****',
        avatar: 'https://static.juzicon.com/avatars/avatar-190919181051-M3HK.jpg?x-oss-process=image/resize,m_fill,w_100,h_100',
        level: 3,
        homeLink: '/37'
      }
    },
    {
      id: '38',
      parentId: '3',
      uid: '13',
      likes: 17,
      address: '来自重庆',
      content: ' 无论这个世界对你怎样，都请你一如既往的努力，勇敢，充满希望。',
      createTime: '2023-04-30 15:00',
      createTime: '8小时前',
      user: {
        username: '繁星Cong2',
        avatar:
          'https://static.juzicon.com/user/avatar-f81b3655-03fd-485c-811b-4b5ceaca52b6-210817180051-YTO4.jpg?x-oss-process=image/resize,m_fill,w_100,h_100',
        level: 1,
        homeLink: '/38'
      }
    }
  ]
};
