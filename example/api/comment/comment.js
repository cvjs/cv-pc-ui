export * from './comment-load-more';
export * from './comment-load-comment';
export * from './comment-load-reply';
export * from './comment-submit';

import { throttle } from '@/util';

const _throttle = throttle((type, comment, finish) => {
  switch (type) {
    case '删除':
      alert(`删除成功: ${comment.id}`);
      finish();
      break;
    case '举报':
      alert(`举报成功: ${comment.id}`);
      break;
  }
});

export function commentOperate(type, comment, finish) {
  _throttle(type, comment, finish);
}

// 点赞按钮事件 将评论id返回后端判断是否点赞，然后在处理点赞状态
export function commentLike(id, finish) {
  console.log('点赞: ' + id);
  setTimeout(() => {
    finish();
  }, 200);
}

import { commentData } from './data-comment';
export function commentInit() {
  return commentData;
}
