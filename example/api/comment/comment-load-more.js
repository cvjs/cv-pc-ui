import { ref } from 'vue';
import { commentData } from './data-comment';
import { replyData } from './data-reply';

//分页
const usePage = (pageNum, pageSize, arr = []) => {
  let skipNum = (pageNum - 1) * pageSize;
  let newArr = skipNum + pageSize >= arr.length ? arr.slice(skipNum, arr.length) : arr.slice(skipNum, skipNum + pageSize);
  return newArr;
};

export const useCommentLoadMore = (config) => {
  // 是否禁用滚动加载评论
  const loadMoreDisable = ref(false);
  // 当前页数
  let pageNum = 1;
  // 页大小
  let pageSize = 1;
  // 评论总数量
  let total = commentData.length;
  const loadMorePage = (pageNum, pageSize) => usePage(pageNum, pageSize, commentData);

  // 加载更多评论
  const loadMoreApi = () => {
    console.log(loadMoreDisable.value);
    if (pageNum <= Math.ceil(total / pageSize)) {
      setTimeout(() => {
        config.comments.push(...loadMorePage(pageNum, 1));
        // ++pageNum;
        pageNum++;
      }, 200);
    } else {
      loadMoreDisable.value = true;
    }
  };
  return {
    loadMoreDisable,
    loadMorePage,
    loadMoreApi
  };
};

export const useCommentLoadReply = () => {
  const loadReplyData = replyData;
  // debugger;
  //回复分页
  // : ReplyPageParamApi
  const loadReplyApi = ({ parentId, pageNum, pageSize, finish }) => {
    let tmp = {
      total: loadReplyData.total,
      list: usePage(pageNum, pageSize, loadReplyData.list)
    };
    setTimeout(() => {
      finish(tmp);
    }, 200);
  };
  return {
    loadReplyData,
    loadReplyApi
  };
};
