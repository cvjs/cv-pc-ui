export default {
  msg: '请求mock数据成功',
  status: 200,
  data: [
    {
      val: 'zhinan',
      text: '指南',
      disbled: true,
      children: [
        {
          val: 'shejiyuanze',
          text: '设计原则',
          children: [
            { val: 'yizhi', text: '一致' },
            { val: 'fankui', text: '反馈' },
            { val: 'xiaolv', text: '效率' },
            { val: 'kekong', text: '可控' }
          ]
        },
        {
          val: 'daohang',
          text: '导航',
          children: [
            { val: 'cexiangdaohang', text: '侧向导航' },
            { val: 'dingbudaohang', text: '顶部导航' }
          ]
        }
      ]
    },
    {
      val: 'zujian',
      text: '组件',
      children: [
        {
          val: 'basic',
          text: 'Basic',
          children: [
            { val: 'layout', text: 'Layout 布局' },
            { val: 'color', text: 'Color 色彩' },
            { val: 'typography', text: 'Typography 字体' },
            { val: 'icon', text: 'Icon 图标' },
            { val: 'button', text: 'Button 按钮' }
          ]
        },
        {
          val: 'form',
          text: 'Form',
          children: [
            { val: 'radio', text: 'Radio 单选框' },
            { val: 'checkbox', text: 'Checkbox 多选框' },
            { val: 'input', text: 'Input 输入框' },
            { val: 'input-number', text: 'InputNumber 计数器' },
            { val: 'select', text: 'Select 选择器' },
            { val: 'cascader', text: 'Cascader 级联选择器' },
            { val: 'switch', text: 'Switch 开关' },
            { val: 'slider', text: 'Slider 滑块' },
            { val: 'time-picker', text: 'TimePicker 时间选择器' },
            { val: 'date-picker', text: 'DatePicker 日期选择器' },
            { val: 'datetime-picker', text: 'DateTimePicker 日期时间选择器' },
            { val: 'upload', text: 'Upload 上传' },
            { val: 'rate', text: 'Rate 评分' },
            { val: 'form', text: 'Form 表单' }
          ]
        },
        {
          val: 'data',
          text: 'Data',
          children: [
            { val: 'table', text: 'Table 表格' },
            { val: 'tag', text: 'Tag 标签' },
            { val: 'progress', text: 'Progress 进度条' },
            { val: 'tree', text: 'Tree 树形控件' },
            { val: 'pagination', text: 'Pagination 分页' },
            { val: 'badge', text: 'Badge 标记' }
          ]
        },
        {
          val: 'notice',
          text: 'Notice',
          children: [
            { val: 'alert', text: 'Alert 警告' },
            { val: 'loading', text: 'Loading 加载' },
            { val: 'message', text: 'Message 消息提示' },
            { val: 'message-box', text: 'MessageBox 弹框' },
            { val: 'notification', text: 'Notification 通知' }
          ]
        },
        {
          val: 'navigation',
          text: 'Navigation',
          children: [
            { val: 'menu', text: 'NavMenu 导航菜单' },
            { val: 'tabs', text: 'Tabs 标签页' },
            { val: 'breadcrumb', text: 'Breadcrumb 面包屑' },
            { val: 'dropdown', text: 'Dropdown 下拉菜单' },
            { val: 'steps', text: 'Steps 步骤条' }
          ]
        },
        {
          val: 'others',
          text: 'Others',
          children: [
            { val: 'dialog', text: 'Dialog 对话框' },
            { val: 'tooltip', text: 'Tooltip 文字提示' },
            { val: 'popover', text: 'Popover 弹出框' },
            { val: 'card', text: 'Card 卡片' },
            { val: 'carousel', text: 'Carousel 走马灯' },
            { val: 'collapse', text: 'Collapse 折叠面板' }
          ]
        }
      ]
    },
    {
      val: 'ziyuan',
      text: '资源',
      children: [
        {
          val: 'axure',
          text: 'Axure Components'
        },
        {
          val: 'sketch',
          text: 'Sketch Templates'
        },
        {
          val: 'jiaohu',
          text: '组件交互文档'
        }
      ]
    }
  ]
};
