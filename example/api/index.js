const modulesFiles = import.meta.glob('./modules/*_api.js', { eager: true });

const modilesMods = modulesFiles.keys().reduce((modules, modulePath) => {
  const moduleName = modulePath.replace(/^\.\/(.*)\.\w+$/, '$1');
  const value = modulesFiles(modulePath);
  modules[moduleName] = value.default;
  return modules;
}, {});
// 初始化
let modulesAll = {};
for (let i in modilesMods) {
  if (modilesMods[i]) {
    modulesAll = Object.assign({}, modulesAll, modilesMods[i]);
  }
}
export default modulesAll;
