import { createApp } from 'vue';
import App from './App.vue';
import router from './router/index';

var app = createApp(App);
app.use(router);

/**
 * 以下这种模式不行
 */
// import "./plugins/request";
import { useCvUI } from './plugins/cv-pc-ui.js';
import { useElementUI } from './plugins/element.js';

useElementUI(app);
useCvUI(app);

// import.meta.glob('./plugins/*.js', { eager: true });
app.config.globalProperties.$http = () => {
  console.log('注册一个全局 方法');
};
app.config.globalProperties.$request = {
  urlPost() {}
};
app.mount('#app');
