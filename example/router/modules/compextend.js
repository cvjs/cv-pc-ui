export default [
  {
    title: 'Upload 上传',
    // collapsable: false,
    children: [
      { title: '头像上传', view_path: '/form_upload/upload-avatar' },
      { title: '图片上传', view_path: '/form_upload/upload-image' },
      { title: '文件上传', view_path: '/form_upload/upload-file' },
      { title: '视频上传', view_path: '/form_upload/upload-video' }
    ]
  },
  {
    title: 'Editor 编辑器',
    children: [
      { title: '代码编辑器', view_path: '/editor/editor-code' },
      { title: 'json编辑器', view_path: '/editor/editor-json' },
      { title: 'markdown编辑器', view_path: '/editor/editor-markdown' },
      { title: 'tinymce 富文本', view_path: '/editor/editor-tinymce' },
      { title: 'markdow展示', view_path: '/editor/markdown-show' }
    ]
  },
  {
    title: '高级插件',
    children: [
      { title: '思维导图', view_path: '/minder/minder' },
      { title: 'excel', view_path: '/excel/excel' }
    ]
  },
  {
    title: 'GEO 定位地图',
    children: [
      { title: '城市选择', view_path: '/form/geo-city' },
      { title: '城市选择2', view_path: '/form/geo-city2' }
    ]
  }
];
