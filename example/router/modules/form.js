export default [
  {
    title: 'Form 表单基础',
    children: [
      { title: 'formBase', view_path: '/form/form-base' },
      { title: 'formInline', view_path: '/form/form-inline' }
    ]
  },
  {
    title: 'Form 单行文本',
    children: [
      { title: '文本', view_path: '/form/input-text' },
      { title: '密码', view_path: '/form/input-password' },
      { title: '数字', view_path: '/form/input-number' },
      { title: '小数', view_path: '/form/input-digit' },
      { title: '手机', view_path: '/form/input-mobile' },
      { title: '身份证', view_path: '/form/input-idcard' },
      { title: '邮箱', view_path: '/form/input-email' },
      { title: '自带按钮', view_path: '/form/input-btn' }
    ]
  },
  {
    title: 'Form 多行文本',
    children: [{ title: '多行文本', view_path: '/form/textarea' }]
  },
  {
    title: 'Form 单列选择器',
    children: [
      { title: '单列选择', view_path: '/form/select-base' },
      { title: '树形选择', view_path: '/form/select-tree' }
    ]
  },
  {
    title: 'Form GEO',
    children: [
      { title: '城市选择', view_path: '/form/geo-city' },
      { title: '城市选择2', view_path: '/form/geo-city2' }
    ]
  },
  {
    title: 'Form 级联选择器',
    children: [{ title: '级联选择', view_path: '/form/cascader-base' }]
  },
  {
    title: 'Form 单选',
    children: [
      { title: '单选框组', view_path: '/form/radio-group' },
      { title: '单选样式', view_path: '/form/radio-option' }
    ]
  },
  {
    title: 'Form 多选',
    children: [
      { title: '多选框组', view_path: '/form/checkbox-group' },
      { title: '带全选按钮多选框', view_path: '/form/checkbox-all' },
      { title: '多选样式', view_path: '/form/checkbox-option' }
    ]
  },
  {
    title: 'Form 日期和时间',
    children: [
      { title: '时间选择', view_path: '/form/time-base' },
      { title: '日期选择', view_path: '/form/date-base' },
      { title: '日期和时间', view_path: '/form/datetime-base' },
      { title: '日期和时间区间', view_path: '/form/datetime-range' }
    ]
  },
  {
    title: '其他',
    children: [
      { title: '带搜索功能', view_path: '/form/autocomplete' },
      { title: '远程搜索加载', view_path: '/form/autocomplete' },
      { title: '短信验证', view_path: '/form/code-sms' },
      { title: '计数器', view_path: '/form/counter-base' },
      { title: '静态文本', view_path: '/form/show-text' },
      { title: '静态图片', view_path: '/form/show-image' },
      { title: '开关', view_path: '/form/switch-base' }
    ]
  }
];
