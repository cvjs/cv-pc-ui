export default [
  {
    title: 'Basic 基础',
    children: [
      { title: '按钮', view_path: '/basic/button' },
      { title: '图标', view_path: '/basic/icons' }
    ]
  },
  {
    title: 'Data 数据展示',
    children: [
      { title: '标签', view_path: '/data/tag-base' },
      { title: 'tab 栏跳转', view_path: '/data/tabs-jump' },
      { title: 'comment 评论', view_path: '/data/comment' },
      { title: 'chat', view_path: '/data/chat' },
      { title: 'notice-bar 通知栏', view_path: '/data/notice-bar' },
      { title: 'transfer穿梭框base', view_path: '/data/transfer-base' },
      { title: 'transfer穿梭框table', view_path: '/data/transfer-table' },
      { title: 'table-base 常规列表', view_path: '/data/table-base' },
      { title: 'table-tree 树形列表', view_path: '/data/table-tree' },
      { title: 'table-edit 商品编辑', view_path: '/data/table-edit-goods' },
      { title: 'table-edit 编辑', view_path: '/data/table-editor' }
    ]
  },
  {
    title: 'Dialog 弹窗',
    children: [
      { title: '提示框', view_path: '/dialog/tip' },
      { title: '操作框', view_path: '/dialog/dialog-render' },
      { title: '抽屉', view_path: '/dialog/drawer-render' }
    ]
  },
  {
    title: 'Draw 绘制',
    children: [
      { title: '验证码', view_path: '/draw/captcha' },
      { title: '二维码', view_path: '/draw/qrcode' }
    ]
  }
];
