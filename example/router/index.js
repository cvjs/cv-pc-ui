import { createWebHistory, createRouter } from 'vue-router';

const history = createWebHistory();

// 创建路由盒子
var routes = [
  {
    path: '/',
    redirect: '/index'
  },
  {
    path: '/index',
    name: 'index',
    component: () => import('../views/index.vue')
  }
];

/**
 * 将树形数据向下递归为一维数组
 * @param {*} arr 数据源
 * @param {*} childs  子集key
 */
function treeToFlatten(arr = [], childs = 'Children') {
  return arr.reduce((flat, item) => {
    return flat.concat(item, item[childs] ? treeToFlatten(item[childs], childs) : []);
  }, []);
}

import menuBase from './menu.js';

var menuRoute = treeToFlatten(menuBase, 'children');

menuRoute.forEach((item, index) => {
  if (!item.path) {
    return false;
  }
  const component_path = `./../views${item.view_path}.vue`;
  // console.log(component_path);
  routes.push({
    ...item,
    component: () => import(/* @vite-ignore */ component_path)
  });
});

const router = createRouter({
  mode: 'history',
  history: history,
  base: process.env.BASE_URL,
  routes: routes
});

export default router;
