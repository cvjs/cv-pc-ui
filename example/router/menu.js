const modulesFiles = import.meta.glob('./modules/*.js', { eager: true });

const modulesAll = [];
for (const key in modulesFiles) {
  if (Object.prototype.hasOwnProperty.call(modulesFiles, key)) {
    // modulesAll[key.replace(/(\.\/|\.ts)/g, '')] = modulesFiles[key].default
    let modItem = modulesFiles[key].default;
    // 如果是数组的话,返回“object”,true
    if (typeof modItem == 'object' && Array.isArray(modItem)) {
      for (let j in modItem) {
        if (modItem[j].children) {
          for (let k in modItem[j].children) {
            let view_path = modItem[j].children[k].view_path;
            if (!view_path) {
              continue;
            }
            let name = view_path.replace(/\//g, '_');
            let path = view_path.replace(/\/([^/]+)/g, function (match, p1) {
              return '/' + p1.replace(/\//g, '_');
            });
            modItem[j].children[k]['name'] = name;
            modItem[j].children[k]['path'] = path;
          }
        }
        modulesAll.push(modItem[j]);
      }
    } else {
      modulesAll.push(modItem);
    }
  }
}

export default modulesAll;
