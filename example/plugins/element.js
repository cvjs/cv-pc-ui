import ElementPlus from 'element-plus';
import 'element-plus/dist/index.css';
import zhCn from 'element-plus/dist/locale/zh-cn.mjs';
// 如果您正在使用CDN引入，请删除下面一行。
import * as ElementPlusIconsVue from '@element-plus/icons-vue';

export function useElementUI(app) {
  app.use(ElementPlus, {
    locale: zhCn
  });
  for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
    app.component(key, component);
  }

  // if (window._vueApp_) {
  //   console.log(window._vueApp_);
  //   window._vueApp_.use(ElementPlus);
  //   window._vueApp_.config.globalProperties.$http2 = () => {
  //     console.log('注册一个全局 方法222');
  //   };
  // }
}
