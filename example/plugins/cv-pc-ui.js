/**
 * 本地开发
 */

import cvPcUi from '../../src/index';
import '@/styles/index.css';
import { uploadOption } from './upload.js';

export function useCvUI(app) {
  app.use(cvPcUi, {
    // "cvUploadAvatar": {
    //   ...uploadOption,
    // },
    cvUploadImage: {
      ...uploadOption
    },
    upload: {
      ...uploadOption
    }
  });
}

/**
 * 打包后测试
 */

// import cvPcUi from "/lib/cv-pc-ui.common.js";
// import "/lib/cv-pc-ui.css";
// Vue.use(cvPcUi);
