import Vue from 'vue';

import StorageClass from '@10yun/cv-js-utils/plugins/storage.js';
import RequestClass from '@10yun/cv-js-utils/plugins/request.js';

var StorageObj = new StorageClass({});
/**
 * 测试的token
 */
let oldToken = StorageObj.localGet('syCacheAppToken') || '';
if (oldToken == '' || (oldToken == 'undefined' && process.env.NODE_ENV === 'development')) {
  StorageObj.localSet('syCacheAppToken', `${process.env.VITE_SY_TEST_TOKEN}`);
}

// let ApiFlagAll = {};

import ApiFlagAll from '../api/index';

/**
 * 【初始化绑定】 request
 */
var requestObj = new RequestClass({
  baseURL: process.env.VITE_SY_API_URL || '',
  flagMap: ApiFlagAll, //接口配置参数
  // storeHandle: $store,//store句柄
  headers: {
    syOpenAppProject: process.env.VITE_SY_APP_PRO,
    syOpenAppId: process.env.VITE_SY_APP_ID,
    syOpenAppToken: StorageObj.localGet('syCacheAppToken') || '',
    syOpenClientUuid: 'pc',
    Authorization: 'Bearer ' + StorageObj.localGet('syCacheAppJwt') || ''
  },
  requests: {}
});
Vue.prototype.$request = requestObj;
