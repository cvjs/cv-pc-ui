const envArr = import.meta.env;
export const uploadOption = {
  upAction: envArr.VITE_SY_API_URL + 'sdks.sdk_file/common/upload_image',
  upHeaders: {
    syOpenAppProject: envArr.VITE_SY_APP_PROJECT_SIGN,
    syOpenAppId: envArr.VITE_SY_APP_CONNECT_ID,
    syOpenAppToken: envArr.VITE_SY_TEST_TOKEN,
    syOpenClientPlatform: 'pc',
    'Sy-Response-Type': 'json',
    'Sy-Client-Platform': 'pc'
    // 'Authorization': '',
  },
  upName: 'file',
  upData: {
    postType: 'create',
    file_name: 'file'
  },
  upResponseFn(response, file, fileList) {
    console.log(response, file, fileList);
    //   // 需要返回一个 URL
    // 因为是 mock 地址, 所以, 总是返回同一张图片的URL, 正常使用的时候不会
    let apiData = response.data || {};
    return {
      name: apiData.file_name || '',
      url: apiData.img_cdn || '',
      value: apiData.img_time || ''
    };
  }
};
