import { createStore } from 'vuex';

export default new createStore({
  state: {},
  mutations: {},
  actions: {},
  modules: {}
});
