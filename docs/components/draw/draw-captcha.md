# cv-draw-captcha 验证码

### 简要

>验证码

### 示例

::: demo

<CvCodePreview>

<<< @/../example/views/draw/draw-captcha.vue

</CvCodePreview>

:::



### 参考

包名：`qrcodejs2`
网址：https://github.com/davidshimjs/qrcodejs