# $cvDialogRender 动态弹窗

### 简要

>动态弹窗  
>render

### vue 调用方式

- 方式1（只能在 setup 下调用 ）

```js
import { getCurrentInstance, nextTick } from 'vue';

// 获取当前组件实例
const instance = getCurrentInstance();
// 从实例中获取全局属性
const { $cvDrawerRender } = instance.appContext.config.globalProperties;

 nextTick(() => {
    $cvDrawerRender(
      // instance.ctx,
      instance.vnode,
      // 抽屉body，需要展示的内部内容的组件
      DiaTest,
      // 抽屉的属性
      { title: '', size: '100%' },
      // 传递给内部页面的参数，内部用props接收
      { flowID: '' }
    ).show();
  });

```

- 方式2（只能在 setup 下调用 ）

```js
import { getCurrentInstance, nextTick } from 'vue';
import { cvDrawerRender } from '@10yun/cv-pc-ui';
// 获取当前组件实例
const instance = getCurrentInstance();
 nextTick(() => {
    cvDrawerRender(
      // instance.ctx,
      instance.vnode,
      // 抽屉body，需要展示的内部内容的组件
      DiaTest,
      // 抽屉的属性
      { title: '', size: '100%' },
      // 传递给内部页面的参数，内部用props接收
      { flowID: '' }
    ).show();
  });
 
```
### 调用弹窗的页面 


<CvCodePreview>

<<< @/../example/views/dialog/dialog-render.vue

</CvCodePreview>


- 弹窗内部内容 dialogBody1

<CvCodePreview>

<<< @/../example/views/dialog/renderJsx/dialogBody1.vue

</CvCodePreview>

- 弹窗内部内容 dialogBody2

<CvCodePreview>

<<< @/../example/views/dialog/renderJsx/dialogBody2.vue

</CvCodePreview>

### $cvDialogRender 全局方法参数说明

-  参数说明

| 字段                    | 类型       | 注释                                 | 参考 |
| :---------------------- | :--------- | :----------------------------------- | :--- |
| 第1个参数 bodyComponent | 子组件     | 需要加载的《弹窗内容》的组件         |      |
| 第2个参数 dialogOption  | 对象object | 弹窗主体说明                         |      |
| 第3个参数 propsOption   | 对象object | 需要传入《弹窗内容》组件的props数据  |      |
| 第4个参数 onFunc        | 对象object | 《弹窗内容》组件调用父页面绑定的方法 |      |

### 参数 bodyComponent 说明 

### 参数 dialogOption 说明 

| 字段           | 类型     | 默认  | 说明                 | 参考        |
| :------------- | :------- | :---- | :------------------- | :---------- |
| title          | string   | -     | 弹窗标题             |             |
| width          | string   | -     | 弹窗宽度             |             |
| drag           | boolean  | false | 是否拖拽             | true、false |
| closeIsConfirm | Boolean  | false | 关闭弹窗时，是否确认 | true、false |
| closeIsDestroy | Boolean  | true  | 关闭弹窗时，是否销毁 | true、false |
| beforeClose    | Function | null  | 关闭前事件           |             |


### 参数 propsOption 说明 

### 参数 onFunc 说明 

可以监听多个方法

## $cvDialogRender 提供给 bodyComponent 弹窗内容调用的方法


| 方法     | 说明     | 参考 |
| :------- | :------- | :--- |
| close    | 关闭弹窗 |      |
| setTitle | 设置标题 |      |


在 bodyComponent 弹窗内容组件里面，调用方式

```js

this.$emit(方法名字,传递参数);

// 关闭弹窗
this.$emit('close');


// 设置弹窗标题
this.$emit('setTitle','这是一个新的弹窗标题');

```


### 流程

1、 业务页面    
  -》 唤起弹窗  
     标题、引入组件（先引入-写死）、是否显示、宽度、高度   
  -》 



### 参考

- 动态业务处理弹窗 dialog-box  
https://www.jb51.net/article/188191.htm

- Vue中render函数子组件调用父组件  
https://blog.csdn.net/weixin_42815873/article/details/121655043

