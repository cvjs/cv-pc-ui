# cv-dialog-tip 提示框

相关参数

| 参数   | 默认   | 注释               |
| :----- | :----- | :----------------- |
| id     | warpId |                    |
| title  | 空     | 提示框标题         |
| button |        | 按钮组(确认，取消) |
- 备注：无



## 快捷使用方法

```js
if (result.status == 200) {
}
this.$message.(result.msg).confirm(()=>{
  location.href = jumpUrl;
});

```

## 按钮操作提示