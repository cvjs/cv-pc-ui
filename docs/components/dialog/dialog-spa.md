# ctouiDialogSpa 组件

### 结构预览

``` Vue
<!-- 编辑弹窗 -->
<cvDialogBox :title="EFD_Title" v-model="formData" :visible.sync="EFD_Visible" :isLoading="EFD_IsLoading"
  :request-fn="handleRequest" @request-success="handleSuccess">
  <cv-input-text label="xxx" v-model="formData.xxx" />
</cvDialogBox>

```

> 


### 配置自定义参数（JSON） `:settDialog="settDialog"` 


| 参数         | 说明              | 类型   | 可选值 | 默认值  |
| ------------ | ----------------- | ------ | ------ | ------- |
| cancelText   | 取消按钮文字      | string | -      | "取消"  |
| cols         | json 方式配置表单 | Object | -      | -       |
| confirmText  | 确认按钮文字      | string | -      | "确认"  |
| diaWidth     | 弹窗宽度          | string | -      | "600px" |
| getdataUrl   | 表单当前数据接口  | string | -      | -       |
| primaryValue | 表单主键键值      | string | -      | -       |
| response     | 数据返回格式      | Object | -      | -       |
| tablePrimary | 表单主键键名      | string | -      | -       |
| updateUrl    | 表单数据提交接口  | string | -      | -       |
| where        | 接口条件          | Object | -      | -       |

#### settDialog.response

> 数据返回格式

| 参数       | 说明             | 类型   | 可选值 | 默认值   |
| ---------- | ---------------- | ------ | ------ | -------- |
| dataName   | 数据列表字段名称 | string | -      | "data"   |
| msgName    | 状态信息字段名称 | string | -      | "msg"    |
| statusCode | 成功状态码       | int    | -      | 200      |
| statusName | 数据状态字段名   | string | -      | "status" |

#### settDialog.cols

> json表单配置（暂不支持）
