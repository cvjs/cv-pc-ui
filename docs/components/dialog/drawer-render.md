# $cvDrawerRender 动态抽屉

### 简要

>动态抽屉  
>render

### 示例

::: demo

<CvCodePreview>

<<< @/../example/views/dialog/drawer-render.vue

</CvCodePreview>

:::

- 抽屉内部内容 drawerBody1

<CvCodePreview>

<<< @/../example/views/dialog/renderJsx/drawerBody1.vue

</CvCodePreview>

- 抽屉内部内容 drawerBody2

<CvCodePreview>

<<< @/../example/views/dialog/renderJsx/drawerBody2.vue

</CvCodePreview>



### $cvDrawerRender 全局方法参数说明

-  参数说明

| 字段                    | 类型       | 注释                                 | 参考 |
| :---------------------- | :--------- | :----------------------------------- | :--- |
| 第1个参数 bodyComponent | 子组件     | 需要加载的《抽屉内容》的组件         |      |
| 第2个参数 dialogOption  | 对象object | 抽屉主体说明                         |      |
| 第3个参数 propsOption   | 对象object | 需要传入《抽屉内容》组件的props数据  |      |
| 第4个参数 onFunc        | 对象object | 《抽屉内容》组件调用父页面绑定的方法 |      |

### 参数 bodyComponent 说明 

### 参数 dialogOption 说明 

| 字段           | 类型     | 默认 | 说明                 | 参考                                                  |
| :------------- | :------- | :--- | :------------------- | :---------------------------------------------------- |
| title          | string   | -    | 抽屉标题             |                                                       |
| size           | string   | 40%  | 抽屉大小             | 40%                                                   |
| direction      | string   | rtl  | 抽屉打开方向         | rtl(右向左) / ltr(左向右) / ttb(上向下) / btt(下向上) |
| closeIsConfirm | Boolean  | true | 关闭抽屉时，是否确认 | true、false                                           |
| closeIsDestroy | Boolean  | true | 关闭抽屉时，是否销毁 | true、false                                           |
| beforeClose    | Function | null | 关闭前事件           |                                                       |

      

### 参数 propsOption 说明 

### 参数 onFunc 说明 

可以监听多个方法

## $cvDrawerRender 提供给 bodyComponent 抽屉内容调用的方法


| 方法     | 说明     | 参考 |
| :------- | :------- | :--- |
| close    | 关闭抽屉 |      |
| setTitle | 设置标题 |      |


在 bodyComponent 抽屉内容组件里面，调用方式

```js

this.$emit(方法名字,传递参数);

// 关闭抽屉
this.$emit('close');


// 设置抽屉标题
this.$emit('setTitle','这是一个新的抽屉标题');

```