export default {
  text: 'Form 表单',
  collapsed: false,
  items: [
    {
      text: '基础',
      collapsed: false,
      items: [
        { text: 'form-base', link: '/components/form/form-base' },
        { text: 'form-inline', link: '/components/form/form-inline' },
        { text: 'form-item', link: '/components/form/form-item' }
      ]
    },
    {
      text: '输入框',
      collapsed: false,
      items: [
        { text: '文本', link: '/components/form/input-text' },
        { text: '密码', link: '/components/form/input-password' },
        { text: '数字', link: '/components/form/input-number' },
        { text: '小数', link: '/components/form/input-digit' },
        { text: '手机', link: '/components/form/input-mobile' },
        { text: '身份证', link: '/components/form/input-idcard' },
        { text: '邮箱', link: '/components/form/input-email' },
        { text: '自带按钮', link: '/components/form/input-btn' }
      ]
    },
    {
      text: '计数器',
      collapsed: false,
      items: [{ text: '计数器', link: '/components/form/input-counter' }]
    },
    {
      text: '多行文本框',
      collapsed: false,
      items: [
        { text: '多行文本', link: '/components/form/textarea-base' },
        { text: '带按钮多行文本', link: '/components/form/textarea-btn' }
      ]
    },
    {
      text: '单选',
      collapsed: false,
      items: [
        { text: '单选框组', link: '/components/form/radio-group' },
        { text: '单选框', link: '/components/form/radio-opt-btn' }
      ]
    },
    {
      text: '多选',
      collapsed: false,
      items: [
        { text: '多选框组', link: '/components/form/checkbox-group' },
        { text: '带全选按钮', link: '/components/form/checkbox-all' },
        { text: '多选框样式', link: '/components/form/checkbox-opt-btn' }
      ]
    },
    {
      text: '列表选择器',
      collapsed: false,
      items: [
        { text: '单列', link: '/components/form/select-base' },
        { text: '城市选择', link: '/components/form/select-city' },
        { text: '树形选择', link: '/components/form/select-tree' }
      ]
    },
    {
      text: '级联选择器',
      collapsed: false,
      items: [{ text: '多级联动', link: '/components/form/cascader-base' }]
    },
    {
      text: '日期时间选择器',
      collapsed: false,
      items: [
        { text: '时间选择', link: '/components/form/time-base' },
        { text: '时间区间', link: '/components/form/time-range' },
        { text: '日期选择', link: '/components/form/date-base' },
        { text: '日期区间', link: '/components/form/date-range' },
        { text: '日期和时间', link: '/components/form/datetime-base' },
        { text: '日期和时间区间', link: '/components/form/datetime-range' }
      ]
    },
    {
      text: '静态展示',
      collapsed: false,
      items: [
        { text: '展示文本', link: '/components/form/show-text' },
        { text: '展示图片', link: '/components/form/show-image' },
        { text: '展示提示', link: '/components/form/show-tip' }
      ]
    },
    {
      text: '其他',
      collapsed: false,
      items: [
        { text: '带搜索功能', link: '/components/form/autocomplete' },
        { text: '短信验证码', link: '/components/form/code-sms' },
        { text: '开关', link: '/components/form/switch-base' }
      ]
    }
  ]
};
