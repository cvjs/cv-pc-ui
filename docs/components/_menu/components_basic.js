export default {
  text: 'Basic 基础', // 必要的
  collapsed: false,
  items: [
    { text: '按钮', link: '/components/basic/button-base.md' },
    { text: '图标', link: '/components/basic/icons.md' },
    { text: '标签页', link: '/components/basic/tabs-box.md' },
    {
      text: 'Layout 布局',
      collapsed: false,
      items: [
        { text: 'cv-col', link: '/components/basic/cv-col.md' },
        { text: 'cv-row', link: '/components/basic/cv-row.md' }
      ]
    }
  ]
};
