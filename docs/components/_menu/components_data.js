export default {
  text: 'Data 数据展示',
  collapsed: false,
  items: [
    { text: 'tab栏切换', link: '/components/data/tabs-jump' },
    { text: '穿梭框', link: '/components/data/transfer-table' },
    { text: '标签', link: '/components/data/tag-base' },
    {
      text: '列表',
      collapsed: false,
      items: [
        { text: '表格列', link: '/components/data/table-column' },
        { text: '列表表格', link: '/components/data/table-base' },
        { text: '树形表格', link: '/components/data/table-tree' },
        { text: '表格编辑器', link: '/components/data/table-editor' },
        { text: '商品开单器', link: '/components/data/table-edit-goods' }
      ]
    }
  ]
};
