export default {
  text: 'Dialog 弹窗',
  collapsed: false,
  items: [
    { text: '动态弹窗', link: '/components/dialog/dialog-render' },
    { text: '动态抽屉', link: '/components/dialog/drawer-render' }
  ]
};
