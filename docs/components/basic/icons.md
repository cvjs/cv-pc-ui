# cv-icons 图标



### 示例

::: demo

<CvCodePreview>

<<< @/../example/views/basic/icons.vue

</CvCodePreview>

:::


### 属性

| 属性名 | 说明 | 类型            | 可选值 | 默认值 |
| :----- | :--- | :-------------- | :----- | :----- |
| size   | 尺寸 | String / Number |        | 12     |
| type   | 类型 | String          |        | -      |
| color  | 颜色 | String          |        | #000   |
