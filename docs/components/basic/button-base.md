# cv-btn 按钮

### 简要

>button按钮  
>可以通过`type`、`plain`、`round`和`circle`属性更改样式。

### 示例

::: demo

<CvCodePreview>

<<< @/../example/views/basic/button.vue

</CvCodePreview>

:::

### 属性

| 属性名      | 类型    | 默认值 | 说明            | 可选值                                             |
| :---------- | :------ | :----: | :-------------- | :------------------------------------------------- |
| size        | String  |   -    | 尺寸            | medium / small / mini                              |
| type        | String  |   -    | 类型            | primary / success / warning / danger / info / text |
| plain       | boolean | false  | 是否朴素按钮    | --                                                 |
| round       | boolean | false  | 是否圆角按钮    | --                                                 |
| circle      | boolean | false  | 是否圆形按钮    | --                                                 |
| loading     | boolean | false  | 是否加载中状态  | --                                                 |
| disabled    | boolean | false  | 是否禁用状态    | --                                                 |
| icon        | string  |   -    | 图标类名        | --                                                 |
| autoLoading | boolean | false  | 自动开启loading |                                                    |
