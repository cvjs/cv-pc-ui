# 表格column列说明


### 列 table-column 展示说明

> 展示表格column配置

| 可选参数         | 说明 | 默认定位 |
| :--------------- | :--- | :------- |
| cvTbColumnBase   | 基础 |          |
| cvTbColumnIndex  | 索引 | 左       |
| cvTbColumnCheck  | 选择 | 左       |
| cvTbColumnImg    | 图片 |          |
| cvTbColumnExpand | 扩展 |          |
| cvTbColumnText   | 文本 |          |
| cvTbColumnEnum   | 枚举 |          |
| cvTbColumnOpt    | 操作 | 右       |


### 列 table-column 编辑说明

> 可编辑表格column配置

| 可选参数         | 说明 | 默认定位 |
| :--------------- | :--- | :------- |
| cvTbEditSearch   | 搜索 |          |
| cvTbEditRadio    | 单选 | 左       |
| cvTbEditCheckbox | 多选 | 左       |
| cvTbEditSelect   | 选择 |          |
| cvTbEditText     | 文本 |          |
| cvTbEditOpt      | 操作 | 左       |


#### cvTbEditOpt 参数

| 可选参数 | 说明     | 默认 | 可选｜                |
| :------- | :------- | :--- | :-------------------- |
| btnAdd   | 添加一条 | true | Boolean( true、false) |
| btnDel   | 删除当前 | true | Boolean( true、false) |
