# cv-table-editor 编辑表格

**简要、特性**

> 数据驱动、数据格式化、表格内容编辑器  
> 加减、可编辑、行内编辑、输入校检、错误展示、一键禁用
 
### 示例

::: demo

<CvCodePreview>

<<< @/../example/views/data/table-editor.vue

</CvCodePreview>

::: 


### Props 属性说明
 
| 字段       | 类型    | 必须  | 默认             | 说明                      |
| :--------- | :------ | :---- | :--------------- | :------------------------ |
| tableAttrs | Object  | false | { border: true } | 表格的属性                |
| tableOn    | String  |       |                  | 表格事件                  |
| v-model    | Array   |       |                  | 双向绑定 表单数据         |
| disabled   | Boolean | false |                  | 禁用 (对所有input框禁用)) |
