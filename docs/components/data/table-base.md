# cv-table-base 常规列表

### 简要

- 表格组件
- 常规数据

### 示例

::: demo

<CvCodePreview>

<<< @/../example/views/data/table-base.vue

</CvCodePreview>

:::


### 属性


| 参数          | 类型     | 可选值       | 默认值 | 说明                  |
| ------------- | -------- | ------------ | ------ | --------------------- |
| tableConfig   | Object   | -            | -      | 配置表格 ，为必传参数 |
| tableRefresh  | Nubmber  | -            | -      | 表格刷新              |
| dataParse     | Function | -            | -      | 数据解析              |
| showPagebreak | Boolean  | true / false | true   | 是否分页              |

- [ `tableConfig` ]属性说明

| 参数         | 说明           | 类型     | 可选值 | 默认值 |
| ------------ | -------------- | -------- | ------ | ------ |
| tablePrimary | 主键 `键名`    | String   | -      | -      |
| url          | 接口地址       | String   | -      | -      |
| apiFunc      | 接口方法       | Function | -      | -      |
| where        | 接口条件       | Object   | -      | -      |
| pageCurr     | 当前页数       | Number   | -      | 1      |
| pageLimit    | 页长(每页数量) | Number   | -      | 10     |
| request      | 接口请求字段   | Object   | -      | -      |
| response     | 接口响应字段   | Object   | -      | -      |

<!-- | pageGroup    | 页长选项       | Array    | -      | [10, 20, 30, 50, 100, 200] | -->
 