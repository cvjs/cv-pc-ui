# cv-table-tree 树形列表

### 简要

- 表格组件
- 树形递归

### 示例

::: demo

<CvCodePreview>

<<< @/../example/views/data/table-tree.vue

</CvCodePreview>

:::