# cv-tabs-jump tab栏切换

### 简要

>tab栏切换

### 示例

::: demo

<CvCodePreview>

<<< @/../example/views/data/tabs-jump.vue

</CvCodePreview>

::: 


### 属性

| 属性名       | 类型    | 默认值 | 说明         | 可选值                |
| :----------- | :------ | :----: | :----------- | :-------------------- |
| label        | String  |   -    | 字段名       | --                    |
| tab-position | String  |  top   | 标签的位置   | top/right/bottom/left |
| current      | boolean | false  | 默认选中该项 | --                    |


### 插槽

| name | 说明                             |
| :--- | :------------------------------- |
| -    | 预留了匿名插槽可以自定义标签内容 |