# cv-geo-city 单列选择器

### 简要

>城市选择  
>省市区选择，一般和表单配合使用，默认为实心白色背景。

### 示例

::: demo

<CvCodePreview>

<<< @/../example/views/form/geo-city.vue

</CvCodePreview>

:::


### 属性

| 属性名        | 类型    | 默认值 | 说明                       | 可选值                |
| :------------ | :------ | :----: | :------------------------- | :-------------------- |
| cityHide      | Boolean |   -    | 隐藏市                     | -                     |
| cityLabel     | Boolean |   -    | 城市标题                   | -                     |
| districtHide  | Boolean |   -    | 隐藏区/县                  | -                     |
| districtLabel | Boolean |   -    | 区/县标题                  | -                     |
| label         | String  |   -    | 字段名                     | -                     |
| labelWidth    | String  |  80px  | 字段区域宽度，上下排列无效 | -                     |
| v-model       | Array   |   -    | 数据双向绑定               | -                     |
| tip           | String  |   -    | 字段提示/说明              | -                     |
| clearable     | boolean |  true  | 清空按钮                   | -                     |
| rules         | String  |   -    | 验证规则                   | -                     |
| size          | String  |   -    | 尺寸                       | medium / small / mini |
| gutter        | Number  |   20   | 选择框之间的间距           | -                     |
| initChoose    | array   |   []   | 默认选中                   | -                     |

- [ `rules` ] 内置规则

| 规则    | 说明                         |
| :------ | :--------------------------- |
| require | 是否必填（字段名前显示星号） |

- [ `initChoose` ] 默认选中支持的风格

```js
// 支持地址码
initChoose1: [
  "350000", "350100", "350102"
],

// 支持文字风格
initChoose2: [
  "福建省", "福州市", "鼓楼区"
],
```

### 事件

| 事件名   | 参数   | 说明                                                     |
| :------- | :----- | :------------------------------------------------------- |
| onChoose | objArr | 返回由选中对象组成的数组，对象中包含该对象在数组中的索引 |

- [ `onChoose` ] 返回数据格式

```json
{ 
  "province": { "text": "福建省", "value": "350000","index":"12" }, 
  "city": { "text": "福州市", "value": "350100","index":"0" }, 
  "district": { "text": "鼓楼区", "value": "350102","index":"0" } 
}
```