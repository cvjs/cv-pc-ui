# cv-datetime-range 日期和时间区间

### 简要

>选择日期时间区间  
>同时选择两个日期和时间。

### 示例

::: demo

<CvCodePreview>

<<< @/../example/views/form/datetime-range.vue

</CvCodePreview>

:::

### 属性

| 属性名           | 类型    | 默认值 | 说明                                               | 可选值                |
| :--------------- | :------ | :----: | :------------------------------------------------- | :-------------------- |
| startPlaceholder | String  |   -    | 第一个输入框占位文本                               | -                     |
| rangeSeparator   | String  |   -    | 两个输入框中间的占位文本                           | -                     |
| endPlaceholder   | String  |   -    | 第二个输入框占位文本                               | -                     |
| v-model          | String  |   -    | 数据双向绑定                                       | -                     |
| size             | String  |   -    | 尺寸                                               | medium / small / mini |
| valueFormat      | String  |   -    | 指定绑定值的格式，默认接收并返回格式化的字符串时间 | timestamp（时间戳）   |
| pickerOptions    | Boolean | false  | 是否开启快捷选项                                   |
