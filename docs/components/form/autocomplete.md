# cv-autocomplete-load 带搜索功能输入框

### 简要

>type属性为`text`的input  
>带有搜索功能的输入框，一般和表单配合使用，默认为实心白色背景。

### 示例

::: demo

<CvCodePreview>

<<< @/../example/views/form/autocomplete.vue

</CvCodePreview>

:::


### 属性

| 属性名      | 类型    | 默认值 | 说明           | 可选值                |
| :---------- | :------ | :----: | :------------- | :-------------------- |
| placeholder | String  |   -    | 输入框占位文本 | -                     |
| disabled    | Boolen  | false  | 是否禁止       | -                     |
| v-model     | String  |   -    | 数据双向绑定   | -                     |
| size        | String  |   -    | 尺寸           | medium / small / mini |
| clearable   | boolean |  true  | 清空按钮       | -                     |
| compConfig  | Object  |   -    | 组件配置       | -                     |

- 属性 ` compConfig `详情

| 属性名      | 类型     |  默认值   | 说明     | 可选值 |
| :---------- | :------- | :-------: | :------- | :----- |
| apiFunc     | Function |     -     | 请求接口 | -      |
| searchField | Arrat    | ['value'] | 查找字段 | -      |


### 属性

| name | 说明                         |
| :--- | :--------------------------- |
| -    | 预留了匿名插槽可以自定义按钮 |

### 事件

| 事件名 | 参数 | 说明 |
| :----- | :--- | :--- |
 