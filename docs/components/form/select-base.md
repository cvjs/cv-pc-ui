# cv-select-base 单列选择器

### 简要

>单列选择  
>支持单选和多选，一般和表单配合使用，默认为实心白色背景。

### 示例

::: demo

<CvCodePreview>

<<< @/../example/views/form/select-base.vue

</CvCodePreview>

:::


### 属性

| 属性名         | 类型    | 默认  | 说明                           | 可选值                |
| :------------- | :------ | :---: | :----------------------------- | :-------------------- |
| dataLists      | Array   |   -   | 设置选项，详见下面说明         | -                     |
| dataField      | Object  |   -   | 自定数据字段，详见下面说明     | -                     |
| dataDisabled   | Array   |   -   | 禁用对应的选项（根据value）    | -                     |
| multiple       | Boolean |   -   | 是否多选                       | -                     |
| multiple-limit | Number  |   -   | 多选时用户最多可以选择的项目数 | -                     |
| placeholder    | String  |   -   | 输入框占位文本                 | -                     |
| v-model        | String  |   -   | 数据双向绑定                   | -                     |
| disabled       | Boolen  | false | 是否禁止                       | -                     |
| clearable      | boolean | true  | 清空按钮                       | -                     |
| size           | String  |   -   | 尺寸                           | medium / small / mini |

- [ `dataLists` ] 说明  
默认格式为 {label:"选项1",value:"1"} ，label 作为选项名，value 作为选中的返回值 

- [ `dataField` ] 说明  
如果你的数据是这种格式 {text:"选项1",val:"1"} ，只需要用该属性传入 {label:"text",value:"val"} 组件会将 text 作为 label ，val 作为 value 进行渲染

- [ `rules` ] 内置规则

| 规则    | 说明                         |
| :------ | :--------------------------- |
| require | 是否必填（字段名前显示星号） |


### 事件

| 事件名   | 参数      | 说明                                     |
| :------- | :-------- | :--------------------------------------- |
| onChoose | obj,index | 返回当前选中对象，及该对象在数组中的索引 |
