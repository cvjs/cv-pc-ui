# cv-show-image 静态图片

### 简要

>静态图片  
>用来展示图片，支持大图预览。  

### 示例

::: demo

<CvCodePreview>

<<< @/../example/views/form/show-image.vue

</CvCodePreview>

:::

### 属性

| 属性名 | 类型          | 默认值 | 说明                                   |
| :----- | :------------ | :----: | :------------------------------------- |
| url    | String、Array |   -    | 图片的链接                             |
| width  | String        | 100px  | 图片宽度                               |
| height | String        | 100px  | 图片高度                               |
| tip    | String        |   -    | 字段提示/说明                          |
| col    | Number        |   24   | 栅格在一行中占据的宽度比，默认占满一行 |
| offset | Number        |   0    | 指定左侧间隔的栏数                     |
| rules  | String        |   -    | 验证规则                               |

- [ `rules` ] 内置规则

| 规则    | 说明                         |
| :------ | :--------------------------- |
| require | 是否必填（字段名前显示星号） |
