# cv-checkbox-group 多选框

### 简要

>多项选择  
>普通多选框，通过勾选表示选中，一般和表单配合使用，默认为实心白色背景。

### 示例

::: demo

<CvCodePreview>

<<< @/../example/views/form/checkbox-group.vue

</CvCodePreview>

:::

### 属性

| 属性名     | 类型    | 默认  | 说明                                   | 可选值                |
| :--------- | :------ | :---: | :------------------------------------- | :-------------------- |
| dataLists  | Array   |   -   |                                        | -                     |
| dataField  | Object  |   -   |                                        | -                     |
| min        | Number  |   -   | 可被勾选的最小数量                     | -                     |
| max        | Number  |   -   | 可被勾选的最大数量                     | -                     |
| label      | String  |   -   | 字段名                                 | -                     |
| labelWidth | String  | 80px  | 字段区域宽度，上下排列无效             | -                     |
| v-model    | String  |   -   | 数据双向绑定                           | -                     |
| tip        | String  |   -   | 字段提示/说明                          | -                     |
| col        | Number  |  24   | 栅格在一行中占据的宽度比，默认占满一行 | -                     |
| offset     | Number  |   0   | 指定左侧间隔的栏数                     | -                     |
| rules      | String  |   -   | 验证规则                               | -                     |
| size       | String  |   -   | 尺寸                                   | medium / small / mini |
| disabled   | Boolean |   -   | 禁用选项                               | -                     |

- [ `dataLists` ] 说明  
设置选项，默认格式为 {label:"选项1",value:"1"} ，label 作为选项名，value 作为选中的返回值

- [ `dataField` ] 说明  
自定数据字段，如果你的数据是这种格式 {text:"选项1",val:"1"} ，只需要用该属性
传入 {label:"text",value:"val"} 组件会将 text 作为 label ，val 作为 value 进行渲染