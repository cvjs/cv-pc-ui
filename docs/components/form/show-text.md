# cv-show-text 文本显示

### 简要

>静态文本  
>用来展示静态文本，默认为实心白色背景。  

### 示例

::: demo

<CvCodePreview>

<<< @/../example/views/form/show-text.vue

</CvCodePreview>

:::

### 属性

| 属性名 | 类型   | 默认值 | 说明                                   |
| :----- | :----- | :----: | :------------------------------------- |
| text   | String |   -    | 要显示的文本内容                       |
| tip    | String |   -    | 字段提示/说明                          |
| col    | Number |   24   | 栅格在一行中占据的宽度比，默认占满一行 |
| offset | Number |   0    | 指定左侧间隔的栏数                     |
| rules  | String |   -    | 验证规则                               |

- [ `rules` ] 内置规则

| 规则    | 说明                         |
| :------ | :--------------------------- |
| require | 是否必填（字段名前显示星号） |
