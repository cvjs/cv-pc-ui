# cv-input-text 文本输入框

### 简要

>type属性为`text`的input  
>普通单行文本输入框，一般和表单配合使用，默认为实心白色背景。

### 示例

::: demo

<CvCodePreview>

<<< @/../example/views/form/input-text.vue

</CvCodePreview>

:::


### 属性

| 属性名      | 类型    | 默认值 | 说明           | 可选值                |
| :---------- | :------ | :----: | :------------- | :-------------------- |
| placeholder | String  |   -    | 输入框占位文本 | -                     |
| disabled    | Boolen  | false  | 是否禁止       | -                     |
| v-model     | String  |   -    | 数据双向绑定   | -                     |
| size        | String  |   -    | 尺寸           | medium / small / mini |
| clearable   | boolean |  true  | 清空按钮       | -                     |

- [ `rules` ] 内置规则

| 规则     | 说明                                     |
| :------- | :--------------------------------------- |
| max      | 最大长度限制                             |
| min      | 最小长度限制                             |
| zh       | 必须中文                                 |
| en       | 必须英文                                 |
| ennum    | 必须字母和数字                           |
| username | 可以是中英文数字下划线，但不能以数字开头 |