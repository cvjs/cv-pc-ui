# cv-input-btn 带按钮输入框

### 简要

>type属性为`text`的input  
>带有按钮的文本输入框，一般和表单配合使用，默认为实心白色背景。

### 示例

::: demo

<CvCodePreview>

<<< @/../example/views/form/input-btn.vue

</CvCodePreview>

:::


### 属性

| 属性名      | 类型    | 默认值 | 说明               | 可选值                |
| :---------- | :------ | :----: | :----------------- | :-------------------- |
| placeholder | String  |   -    | 输入框占位文本     | -                     |
| disabled    | Boolen  | false  | 是否禁止           | -                     |
| v-model     | String  |   -    | 数据双向绑定       | -                     |
| size        | String  |   -    | 尺寸               | medium / small / mini |
| clearable   | boolean |  true  | 清空按钮           | -                     |
| btnText     | String  |   -    | 按钮显示的文字     | -                     |
| btnIcon     | String  |   -    | 按钮显示的图标类名 | -                     |

- [ `rules` ] 内置规则

| 规则     | 说明                                     |
| :------- | :--------------------------------------- |
| max      | 最大长度限制                             |
| min      | 最小长度限制                             |
| zh       | 必须中文                                 |
| en       | 必须英文                                 |
| ennum    | 必须字母和数字                           |
| username | 可以是中英文数字下划线，但不能以数字开头 |

### 插槽

| name | 说明                         |
| :--- | :--------------------------- |
| -    | 预留了匿名插槽可以自定义按钮 |

### 事件

| 事件名   | 参数  | 说明                                   |
| :------- | :---- | :------------------------------------- |
| btnClick | value | 单击按钮时触发的事件，会返回输入框的值 |