# cv-cascader-base 多级联动选择器

### 简要

>级联选择  
>多级列表联动选择，一般和表单配合使用，默认为实心白色背景。

### 示例

::: demo

<CvCodePreview>

<<< @/../example/views/form/cascader-base.vue

</CvCodePreview>

:::


### 属性

| 属性名        | 类型    | 默认  | 说明                                                                            | 可选值                |
| :------------ | :------ | :---- | :------------------------------------------------------------------------------ | :-------------------- |
| dataLists     | Array   | -     | 设置选项，详见下面说明                                                          | -                     |
| dataField     | Object  | -     | 自定数据字段，详见下面说明                                                      | -                     |
| dataDisabled  | Array   | -     | 禁用对应的选项（根据value）                                                     | -                     |
| placeholder   | String  | -     | 输入框占位文本                                                                  | -                     |
| v-model       | String  | -     | 数据双向绑定                                                                    | -                     |
| disabled      | Boolen  | false | 是否禁止                                                                        | -                     |
| size          | String  | -     | 尺寸                                                                            | medium / small / mini |
| showAllLevels | boolean | true  | 输入框中是否显示选中值的完整路径                                                | -                     |
| emitPath      | boolean | true  | 是否返回由该节点所在的各级菜单的值所组成的数组，若设置false，则只返回该节点的值 | -                     |
| filterable    | boolean | false | 是否可搜索选项                                                                  | -                     |
| multiple      | boolean | false | 是否多选                                                                        | -                     |


- [ `dataLists` ] 说明  
设置选项，默认格式为 {label:"选项1",value:"1"} ，label 作为选项名，value 作为选中的返回值

- [ `dataField` ] 说明  
自定数据字段，如果你的数据是这种格式 {text:"选项1",val:"1"} ，只需要用该属性
传入 {label:"text",value:"val"} 组件会将 text 作为 label ，val 作为 value 进行渲染
