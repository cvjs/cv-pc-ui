# cv-checkbox-opt-btn 多选框

### 简要

>多项选择  
>按钮样式多选框，通过勾选表示选中，一般和表单配合使用，默认为实心白色背景。


### 示例

::: demo

<CvCodePreview>

<<< @/../example/views/form/checkbox-option.vue

</CvCodePreview>

:::

### 属性

| 属性名 | 类型   | 默认  | 说明 | 可选值 |
| :----- | :----- | :---: | :--- | :----- |
| label  | String |   -   | 值   | -      |