# cv-switch-base 滑动开关

### 简要

>普通的开关  
>两种相互对立的状态间的切换，多用于触发「开/关」。
>带文字描述的开关   
>两种相互对立的状态间的切换，多用于触发「开/关」。

### 示例

::: demo

<CvCodePreview>

<<< @/../example/views/form/switch-base.vue

</CvCodePreview>

:::

### 属性

| 属性名        | 类型   | 默认值 | 说明           |
| :------------ | :----- | :----: | :------------- |
| v-model       | String |   -    | 数据双向绑定   |
| activeValue   | String |   -    | 打开值         |
| inactiveValue | String |   -    | 关闭值         |
| activeText    | String |   -    | 打开的文字描述 |
| inactiveText  | String |   -    | 关闭的文字描述 |
| activeColor   | String |   -    | 打开的颜色     |
| inactiveColor | String |   -    | 关闭的颜色     |

- [ `rules` ] 内置规则

| 规则    | 说明                         |
| :------ | :--------------------------- |
| require | 是否必填（字段名前显示星号） |
