# cv-form-item

### 简要

>表单单项

### 示例

::: demo

<CvCodePreview>

<<< @/../example/views/form/form-base.vue

</CvCodePreview>

:::


### 属性

| 属性名     | 类型    | 默认值 | 说明                                   | 可选值 |
| :--------- | :------ | :----: | :------------------------------------- | :----- |
| label      | String  |   -    | 字段名                                 | -      |
| labelWidth | String  |  80px  | 字段区域宽度，上下排列无效             |        |
| offset     | Number  |   0    | 指定栅格左侧间隔的栏数                 | -      |
| col        | Number  |   24   | 栅格在一行中占据的宽度比，默认占满一行 | -      |
| rules      | String  |   -    | 验证规则                               |        |
| tip        | String  |   -    | 字段提示/说明，不设置则没有提示        | -      |
| require    | Boolean |   -    | 是否必填（字段名前显示星号）           | -      |
