# cv-show-tip 文本显示

### 简要

>静态提示


### 示例

::: demo

<CvCodePreview>

<<< @/../example/views/form/show-text.vue

</CvCodePreview>

:::

### 属性

| 属性名      | 类型   | 默认值  | 说明         |
| :---------- | :----- | :-----: | :----------- |
| bgColor     | String | #ecf8ff | 背景颜色     |
| borderColor | String | #50bfff | 左边边框颜色 |

