# cv-time-base 选择时间

### 简要

>时间选择  
>用来选择某个时间点。

### 示例

::: demo

<CvCodePreview>

<<< @/../example/views/form/time-base.vue

</CvCodePreview>

:::


### 属性

| 属性名      | 类型   | 默认值 | 说明                                               | 可选值                |
| :---------- | :----- | :----: | :------------------------------------------------- | :-------------------- |
| placeholder | String |   -    | 输入框占位文本                                     | -                     |
| v-model     | String |   -    | 数据双向绑定                                       | -                     |
| size        | String |   -    | 尺寸                                               | medium / small / mini |
| valueFormat | String |   -    | 指定绑定值的格式，默认接收并返回格式化的字符串时间 | timestamp（时间戳）   |
