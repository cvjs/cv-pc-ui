# cv-date-range 日期区间

### 简要

>日期区间选择  
>同时选择两个日期。

### 示例

::: demo

<CvCodePreview>

<<< @/../example/views/form/date-range.vue

</CvCodePreview>

:::


### 属性

| 属性名           | 类型    | 默认值 | 说明                                               | 可选值                |
| :--------------- | :------ | :----: | :------------------------------------------------- | :-------------------- |
| startPlaceholder | String  |   -    | 第一个输入框占位文本                               | -                     |
| rangeSeparator   | String  |   -    | 两个输入框中间的占位文本                           | -                     |
| endPlaceholder   | String  |   -    | 第二个输入框占位文本                               | -                     |
| v-model          | String  |   -    | 数据双向绑定                                       | -                     |
| col              | Number  |   24   | 栅格在一行中占据的宽度比，默认占满一行             | -                     |
| offset           | Number  |   0    | 左侧间隔的栏数                                     | -                     |
| rules            | String  |   -    | 验证规则                                           | -                     |
| size             | String  |   -    | 尺寸                                               | medium / small / mini |
| valueFormat      | String  |   -    | 指定绑定值的格式，默认接收并返回格式化的字符串时间 | timestamp（时间戳）   |
| pickerOptions    | Boolean | false  | 是否开启快捷选项                                   |

