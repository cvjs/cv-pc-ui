# cv-radio-opt-btn 单选框

### 简要

>单项选择  
>按钮样式单选，一般和表单配合使用，默认为实心白色背景。

### 示例

::: demo

<CvCodePreview>

<<< @/../example/views/form/radio-option.vue

</CvCodePreview>

:::

### 属性

| 属性名 | 类型   | 默认值 | 说明 | 可选值 |
| :----- | :----- | :----: | :--- | :----- |
| label  | String |   -    | 值   | -      |
