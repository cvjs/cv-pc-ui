# cv-select-tree 树形选择器

### 简要

>树形列表选择  
>单列树形列表选择，一般和表单配合使用，默认为实心白色背景。

### 示例

::: demo

<CvCodePreview>

<<< @/../example/views/form/select-tree.vue

</CvCodePreview>

::: 

### 属性

| 属性名       | 类型    | 默认  | 说明                        | 可选值                |
| :----------- | :------ | :---: | :-------------------------- | :-------------------- |
| dataLists    | Array   |   -   |                             | -                     |
| dataField    | Object  |   -   |                             | -                     |
| dataDisabled | Array   |   -   | 禁用对应的选项（根据value） | -                     |
| placeholder  | String  |   -   | 输入框占位文本              | -                     |
| disabled     | Boolen  | false | 是否禁止                    | -                     |
| v-model      | String  |   -   | 数据双向绑定                | -                     |
| clearable    | boolean | true  | 清空按钮                    | -                     |
| size         | String  |   -   | 尺寸                        | medium / small / mini |
| accordion    | false   |       | 是否收起                    |
| multiple     | false   |       | 是否多选                    |
| clearable    | false   |       | 是否开启清除按钮            |


- [ `dataLists` ] 说明  
设置选项，默认格式为 {label:"选项1",value:"1"} ，label 作为选项名，value 作为选中的返回值

- [ `dataField` ] 说明  
自定数据字段，如果你的数据是这种格式 {text:"选项1",val:"1"} ，只需要用该属性
传入 {label:"text",value:"val"} 组件会将 text 作为 label ，val 作为 value 进行渲染





