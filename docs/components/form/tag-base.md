# cv-tag-base 基础Tab标签

### 简要

>Tag 标签  
>用于标记和选择。

### 示例

::: demo

<CvCodePreview>

<<< @/../example/views/data/tag-base.vue

</CvCodePreview>

:::



### 属性

| 属性名             | 类型    | 默认值 | 说明             | 可选值                            |
| :----------------- | :------ | :----: | :--------------- | :-------------------------------- |
| v-model            | String  |   -    | 数据双向绑定     |                                   |
| size               | String  |   -    | 尺寸             | medium / small / mini             |
| closable           | Boolean | false  | 是否可移除标签   |                                   |
| disableTransitions | Boolean | false  | 是否禁用渐变动画 |                                   |
| type               | String  |   -    | 类型             | success / info / warning / danger |
| effect             | String  | light  | 主题             | dark / light / plain              |
| color              | String  |   -    | 背景色           | / danger                          |
| hit                | Boolean |   -    | 是否有边框描边   | / danger                          |

- [ `rules` ] 内置规则

| 规则    | 说明                         |
| :------ | :--------------------------- |
| require | 是否必填（字段名前显示星号） |
