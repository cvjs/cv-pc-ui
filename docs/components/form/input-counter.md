# cv-input-counter 计数器

### 简要

>计数器\数量 
>仅允许输入标准的数字值，可定义起始数字和范围。

### 示例

::: demo

<CvCodePreview>

<<< @/../example/views/form/counter-base.vue

</CvCodePreview>

:::


### 属性

| 属性名      | 类型   |  默认值   | 说明                   | 可选值                |
| :---------- | :----- | :-------: | :--------------------- | :-------------------- |
| placeholder | String |     -     | 输入框占位文本         | -                     |
| disabled    | Boolen |   false   | 是否禁止               | -                     |
| v-model     | String |     -     | 数据双向绑定           | -                     |
| min         | Number | -Infinity | 设置计数器允许的最小值 | -                     |
| max         | Number | Infinity  | 设置计数器允许的最大值 | -                     |
| step        | Number |     1     | 计数器步长             | -                     |
| size        | String |     -     | 尺寸                   | medium / small / mini |

- [ `rules` ] 内置规则

| 规则    | 说明                         |
| :------ | :--------------------------- |
| require | 是否必填（字段名前显示星号） |
