# cv-code-sms 短信验证

### 简要

>type属性为`text`的input  
>带有发送短信按钮的验证码输入框，点击按钮会对传入的手机号进行格式预验证，通过触发onGetSms事件返回验证结果。
>用于填写短信验证码  
>一般和表单配合使用，默认为实心白色背景。
>mobile 用来绑定手机号

### 示例

::: demo

<CvCodePreview>

<<< @/../example/views/form/code-sms.vue

</CvCodePreview>

:::


### 属性

| 属性名      | 类型    | 默认值 | 说明                                      | 可选值                |
| :---------- | :------ | :----: | :---------------------------------------- | :-------------------- |
| placeholder | String  |   -    | 输入框占位文本                            | -                     |
| disabled    | Boolen  | false  | 是否禁止                                  | -                     |
| v-model     | String  |   -    | 数据双向绑定                              | -                     |
| size        | String  |   -    | 尺寸                                      | medium / small / mini |
| clearable   | boolean |  true  | 清空按钮                                  | -                     |
| mobile      | Number  |   -    | 手机号                                    | -                     |
| waitTime    | Number  |   60   | 短信发送间隔,发送成功后按钮禁用间隔（秒） | -                     |


- [ `rules` ] 内置规则

| 规则    | 说明                         |
| :------ | :--------------------------- |
| require | 是否必填（字段名前显示星号） |
| max     | 最大长度限制                 |
| min     | 最小长度限制                 |


### 事件

| 事件名   | 参数  | 说明                                  |
| :------- | :---- | :------------------------------------ |
| onGetSms | state | 返回参数为短信发送状态，true 或 false |