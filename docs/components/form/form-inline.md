# cv-form-inline

### 简要

>横向排列表单，用于搜索布局

### 示例

::: demo

<CvCodePreview>

<<< @/../example/views/form/form-inline.vue

</CvCodePreview>

:::