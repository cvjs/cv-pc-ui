# cv-form-base

### 简要

>纵向排列表单，用于常规布局

### 示例

::: demo

<CvCodePreview>

<<< @/../example/views/form/form-base.vue

</CvCodePreview>

:::