export default [
  {
    text: '网络请求',
    link: '/develop/http'
  },
  {
    text: '缓存',
    link: '/develop/storage'
  },
  { text: '使用案例', link: '/develop/case' },
  { text: '隐私', link: '/develop/privacy' },
  {
    text: '微前端', // 必要的
    collapsed: false,
    items: [
      { text: '介绍', link: '/develop/micro/' },
      { text: '基础包下载', link: '/develop/micro/base-pack' }
    ]
  },
  // {
  //   text: '快速使用',
  //   collapsed: false,
  //   items: [
  //     // {  text: '快速安装' ,link: '/develop/install/',},
  //     {  text: 'npm安装' ,link: '/develop/install/npm',},
  //   ]
  // },
  // {
  //   text: '微前端开发',
  //   collapsed: false,
  //   items: [
  //     { text: '介绍', link: '/develop/micro/' },
  //   ]
  // },
  {
    text: 'vscode高效开发', // 必要的
    collapsed: false,
    items: [
      { text: '介绍', link: '/develop/vscode/' },
      { text: '插件', link: '/develop/vscode/plugin' },
      { text: '配置', link: '/develop/vscode/setting' },
      { text: '自动补齐pc-ui标签', link: '/develop/vscode/snippets' },
      { text: 'pc端常用页面模版', link: '/develop/vscode/snippets-tpl' }
    ]
  }
];
