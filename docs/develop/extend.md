# 第三方扩展

cv-pc-ui 可以很简单实现扩展，以下是扩展的集合，如果您自己开发并开源了自己的form组件，可以在下方评论，会收集放到列表里：

- cv-form-dynamic 动态表单
- cv-form-bmap 百度地图 组件
- cv-form-gallery 图片/视频 展示组件
- ....(期待你的开发)
  

  