# 快速开始

## 工程方式

### 安装

```sh
# 安装
npm install @10yun/cv-pc-ui 

# 更新
npm install @10yun/cv-pc-ui -f
```

### 使用

```js
import Vue from 'vue'
import App from './App.vue';


// 引入 element
import ElementPlus from 'element-plus'
import 'element-plus/lib/theme-chalk/index.css'
Vue.use(ElementPlus)

// 引入 cv-pc-ui
// 注册 cv-pc-ui
import cvPcUi from '@10yun/cv-pc-ui'
Vue.use(cvPcUi)


Vue.config.productionTip = false

new Vue({
  render: h => h(App)
}).$mount('#app')
```

### 全局配置

！注意：不是 upload: { attrs: { action: "xxx"} }，也就是不需要 attrs 包裹

```js
// 在引入 cvPcUi 时，可以传入一个全局配置对象(可选), 例如:
Vue.use(cvPcUi, {
  // 所有和上传相关(上传图片, 上传视频, 富文本中图片上传)
  upload: {
    action: 'https://www.xxx.com/posts', // 请求地址,
    data: { token: 'xxx' }, // 附带的参数,
    upResponseFn (response) { // 处理响应结果
      return 'https://www.xxx.com/upload/' + response.id
    }
  },
  // number类型
  number: {
    min: 0 // 所有 number 类型, 最小值为 0
  }
})
```
 
## CDN方式

```js
<!-- 引入Vue -->
<script src="https://unpkg.com/vue"></script>
<!-- 引入element -->
<link rel="stylesheet" href="https://unpkg.com/element-ui/lib/theme-chalk/index.css">
<script src="https://unpkg.com/element-ui/lib/index.js"></script>
<!-- 引入 @10yun/cv-pc-ui -->
<script src="https://unpkg.com/@10yun/cv-pc-ui"></script>

<script type="text/javascript">
	// 如果需要，则进行全局配置
	Vue.prototype.$cvUiParams = {
	  "xxxxCxxx": {
	    action: 'xxx'.
	    // ...
	  },
	  // ...
	}
</script>
```