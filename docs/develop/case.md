# 案例/演示

- 基于微前端架构
- 适用各种子应用接入


- [十云-订单系统](https://demo5.10yun.com/) 
- [十云-商品系统](https://demo5.10yun.com/) 
- [十云-库存系统](https://demo5.10yun.com/) 
- [十云-会员系统](https://demo5.10yun.com/) 
- [十云-客户系统](https://demo5.10yun.com/) 
- [十云-财务系统](https://demo5.10yun.com/) 
- [十云-报表系统](https://demo5.10yun.com/) 


<span style="color:red; font-weight: bold;">[十云平台]</span> 

- 官方网站
- h5产品
- h5应用
- pc产品
- pc应用  
- 微信小游戏


### demo演示

- 演示地址：[http://console.10yun.cn/](http://console.10yun.cn/)  
- 基于微前端架构
- 适用各种子应用接入