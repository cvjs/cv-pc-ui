# 组件开发规范


### props 命名规范

- 定义 `Function` 属性的时候，用 `on` 开头，

例如： `onClick`，`onChange` 等





### methods 命名规范

- 监听 @click 方法时候，定义为 `handle` 开头

例如 ` @click="handleClick" `，` @click="handleChange" `，