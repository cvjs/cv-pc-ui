# 配置cv-pc-ui自动标签补齐

1、 打开vscode  
2、 配置
   - mac 电脑 按下 `shift + command + p `
   - window电脑 按下 `shift + ctrl + p`

3、 在输入框 搜索 `代码片段` 再选择 `首选项:配置用户代码片段`  
4、 选择 `新建全局代码片段`  
5、 输入代码片段储存文件名称 `cv-pc-ui` 按回车确定。  
6、 把以下内容复制到文件 （下面代码区域右上角，点击按钮可快捷复制） 

::: demo

<CvCodePreview>

<<< @/../docs/_tpl/快捷pc端.code-snippets

</CvCodePreview>

:::





