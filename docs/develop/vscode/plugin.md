# vscode 插件



| 插件名称                      | 是否建议 | 插件备注     |
| :---------------------------- | :------: | :----------- |
| Vue Language Features (Volar) |   建议   | vue          |
| Version Lens                  |   建议   | 依赖版本检查 |
| Prettier - Code formatter     |   建议   | 代码格式化   |