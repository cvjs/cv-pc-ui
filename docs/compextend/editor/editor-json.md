# cv-editor-json json编辑器

### 简要

>编辑 JSON 对象或者数组  
> Vue JSONEditor

- json 编辑器
- jsoneditor
- vue-jsoneditor
- vue3-jsoneditor
- element-jsoneditor


### 示例

::: demo

<CvCodePreview>

<<< @/../example/views/editor/editor-json.vue

</CvCodePreview>

:::


![demo](/comextend/v-jsoneditor.gif)

 
 
### 属性

| 属性名             | 描述                                            | 类型    | 可选值              | 默认值             |
| :----------------- | :---------------------------------------------- | :------ | :------------------ | :----------------- |
| v-model/modelValue | Object value                                    | Object  | -                   |
| options            | Jsoneditor params, You can look at the detailed | Object  | -                   | `{ mode: 'code' }` |
| plus               | Whether full screen switching is added          | Boolean | -                   | true               |
| height             | Default height                                  | String  | -                   |
| mode               | 模式                                            | String  | text / tree / table | text               |
| readOnly           | 是否只读                                        |


更多参数，相见  https://github.com/josdejong/svelte-jsoneditor#properties


### 事件

| 事件名 | 说明           | 参数 |
| ------ | -------------- | ---- |
| error  | Error callback |      |


### 相关链接

两个都是同一个作者

- 【最终选择新的】vue的json编辑器 [vanilla-jsoneditor](https://github.com/josdejong/svelte-jsoneditor) 
- 【以前选择】json编辑 [jsoneditor](https://github.com/josdejong/jsoneditor) 
