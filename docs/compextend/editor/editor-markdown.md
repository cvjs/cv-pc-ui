# cv-editor-markdown markdown编辑器

### 简要

> makrodwn 编辑器


### 示例

::: demo

<CvCodePreview>

<<< @/../example/views/editor/editor-markdown.vue

</CvCodePreview>

:::

**全局配置**（一般情况下不需要）

如果需要全局配置，请修改 `src/plugins/cv-pc-ui.js` 扩展

```js
import cvPcUI from '@10yun/cv-pc-ui'
// 注册 全局设置
Vue.use(cvPcUI, {
  // 可以为编辑器配置全局属性, 每个实例都共享这个属性
  // 属性请参考下面
  "cvEditorMarkdown": {
    options: {
      // ...详见[属性options参数]
    },
    toolbars:{

    }
  }
});
``` 

### 属性


| 属性名               | 说明                                                | 类型              | 默认               |
| :------------------- | :-------------------------------------------------- | :---------------- | :----------------- |
| v-model / modelValue | 组件接受的输入值也支持双向绑定。                    | `String`/`Number` | -                  |
| options              | 参数，详见 [属性 options](#属性options)             | `Object`          |
| toolbars             | 工具，详见 [属性 toolbars](#属性toolbars)           | `Object`          | `{copy:true}`      |
| language             | 语言，详见 [属性 language](#属性language)           | `String`          | `Plain Text`       |
| theme                | 主题，详见 [属性 theme](#属性theme)                 | `String`          | `oneDark`          |
| readOnly             | 是否只读                                            | `Boolean`         | false              |
| autoSave             | 自动保存，通过绑定`@on-act-save`事件                | `Boolean`         | false              |
| interval             | 自动保存间隔，单位：`mm`，需`autoSave=true`时才有效 | `Number`          | 10000              |
| width                | 初始宽度                                            | `String`/`Number` | 'auto'             |
| height               | 初始高度，支持`%`                                   | `String`/`Number` | 600                |
| bordered             | 是否边框                                            | `Boolean`         | true               |
| exportFileName       | 导出文件名称，默认`YYYY-MM-DD_HH:mm`                | `String`          | `YYYY-MM-DD_HH:mm` |  |
| isFullscreen         | 是否全局                                            | `Boolean`         | false              |
| isPreview            | 是否预览                                            | `Boolean`         | true               |


#### 属性options

| 参数名 | 说明 | 类型 | 默认 |
| :----- | :--- | :--- | :--- |

#### 属性toolbars

| 工具名             | 说明             | 类型      | 默认  |
| :----------------- | :--------------- | :-------- | :---- |
| strong             | 粗体             | `Boolean` | true  |
| italic             | 斜体             | `Boolean` | true  |
| overline           | 删除线           | `Boolean` | true  |
| h1                 | 标题1            | `Boolean` | true  |
| h2                 | 标题2            | `Boolean` | true  |
| h3                 | 标题3            | `Boolean` | true  |
| h4                 | 标题4            | `Boolean` | false |
| h5                 | 标题5            | `Boolean` | false |
| h6                 | 标题6            | `Boolean` | false |
| hr                 | 分割线           | `Boolean` | true  |
| quote              | 引用             | `Boolean` | true  |
| ul                 | 无序列表         | `Boolean` | true  |
| ol                 | 有序列表         | `Boolean` | true  |
| code               | 代码块           | `Boolean` | true  |
| link               | 链接             | `Boolean` | true  |
| image              | image            | `Boolean` | true  |
| uploadImage        | 本地图片         | `Boolean` | false |
| table              | 表格             | `Boolean` | true  |
| checked            | 已完成列表       | `Boolean` | true  |
| notChecked         | 未完成列表       | `Boolean` | true  |
| preview            | 预览             | `Boolean` | true  |
| split              | 分屏模式切换     | `Boolean` | true  |
| print              | 打印             | `Boolean` | false |
| theme              | 主题切换         | `Boolean` | true  |
| fullscreen         | 全屏             | `Boolean` | true  |
| exportmd           | 导出为*.md文件   | `Boolean` | true  |
| importmd           | 导入本地*.md文件 | `Boolean` | true  |
| copy               | 复制             | `Boolean` | true  |
| clear              | 清空内容         | `Boolean` | false |
| save               | 保存按钮         | `Boolean` | false |
| toc                | 目录             | `Boolean` | false |
| custom_image       | 自定义图片空间   | `Boolean` | true  |
| custom_uploadImage | 自定义图片上传   | `Boolean` | true  |
| custom_uploadFile  | 自定义文件上传   | `Boolean` | true  |

#### 属性language

支持的语法

| 语言       |
| :--------- |
| javascript |
| html       |
| json       |
| markdown   |
| vue        |
| php        |
| java       |
| sql        |
| python     |
| xml        |
| go         |
| css        |


详见：https://codemirror.net/

#### 属性theme

支持的主题

| 主题            |
| :-------------- |
| default /light  |
| oneDark         |
| materialLight   |
| materialDark    |
| solarizedLight  |
| solarizedDark   |
| dracula         |
| githubLight     |
| githubDark      |
| aura            |
| tokyoNight      |
| tokyoNightStorm |
| tokyoNightDay   |

详见：https://codemirror.net/


### Slots插槽

| 插槽名 | 说明                    |
| :----- | :---------------------- |
| title  | 在toolbars 增加标题名称 |

### emits事件

| 事件名              | 说明                       | 参数                                                                            |
| :------------------ | :------------------------- | :------------------------------------------------------------------------------ |
| update:modelValue   | **仅当**内容发生更改时     | `(value: string, viewUpdate: ViewUpdate)`                                       |
| on-ready            | 加载成功时                 | `(payload: { view: EditorView; state: EditorState; container:HTMLDivElement })` |
| on-action           | 执行任意toolbars返回action | `(action, state)`                                                               |
| on-act-copy         | 复制成功后                 |                                                                                 |
| on-act-save         | 自动保存后                 |                                                                                 |
| on-act-theme        | 主题选择后                 |                                                                                 |
| on-act-custom       | 自定义事件                 |                                                                                 |
| on-act-choose-image | 选择图片后                 |                                                                                 |


- on-save
编辑器保存事件，自动保存或者手动保存时触发，支持`ctrl+s`或`command+s`触发保存，返回值类型为`Object`，包含当前输入值`value`和选择的代码块主题`theme`。

- on-act-choose-image

图片上传事件，用户自定义上传图片，复制粘贴图片截图、文件和点开菜单栏上传按钮时式触发，返回`file`文件，上传文件后可结合`on-ready`事件内返回的`insertContent`插入图片。
监听编辑器粘贴图片事件，在编辑区域内手动粘贴图片时触发，可用于支持粘贴插入图片文件，返回`file`文件，上传文件后可结合`on-ready`事件内返回的`insertContent`插入图片。




 
### 相关链接

UI库
---
- [element-plus](https://element-plus.gitee.io/)  


编辑器
---
- [vue-codemirror](https://github.com/surmon-china/vue-codemirror)  
- [codemirror](https://codemirror.net/)  


html-to-md
---
- https://www.npmjs.com/package/html-to-md  
- https://github.com/stonehank/html-to-md  

<!--@highlightjs/cdn-assets-->
