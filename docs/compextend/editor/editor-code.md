# cv-editor-code 代码编辑器

### 简要

- 代码编辑器，自动补齐


### 示例

::: demo

<CvCodePreview>

<<< @/../example/views/editor/editor-code.vue

</CvCodePreview>

:::

**全局配置**（一般情况下不需要）

如果需要全局配置，请修改 `src/plugins/cv-pc-ui.js` 扩展

```js
import cvPcUI from '@10yun/cv-pc-ui'
// 注册 全局设置
Vue.use(cvPcUI, {
  // 可以为编辑器配置全局属性, 每个实例都共享这个属性
  // 属性请参考下面
  "cvEditorCode": {
    options: {
      // ...详见[属性options参数]
    },
    toolbars:{

    }
  }
});
``` 

### 属性

| 属性名               | 说明                                                | 类型              | 默认               |
| :------------------- | :-------------------------------------------------- | :---------------- | :----------------- |
| v-model / modelValue | 组件接受的输入值也支持双向绑定。                    | `String`/`Number` | -                  |
| options              | 参数，详见 [属性 options](#属性options)             | `Object`          |
| toolbars             | 工具，详见 [属性 toolbars](#属性toolbars)           | `Object`          | `{copy:true}`      |
| language             | 语言，详见 [属性 language](#属性language)           | `String`          | `Plain Text`       |
| theme                | 主题，详见 [属性 theme](#属性theme)                 | `String`          | `oneDark`          |
| readOnly             | 是否只读                                            | `Boolean`         | false              |
| autoSave             | 自动保存，通过绑定`@on-act-save`事件                | `Boolean`         | false              |
| interval             | 自动保存间隔，单位：`mm`，需`autoSave=true`时才有效 | `Number`          | 10000              |
| width                | 初始宽度                                            | `String`/`Number` | 'auto'             |
| height               | 初始高度，支持`%`                                   | `String`/`Number` | 600                |
| bordered             | 是否边框                                            | `Boolean`         | true               |
| exportFileName       | 导出文件名称，默认`YYYY-MM-DD_HH:mm`                | `String`          | `YYYY-MM-DD_HH:mm` |  |
| isFullscreen         | 是否全局                                            | `Boolean`         | false              |

<!-- | isPreview | 是否预览 | `Boolean`         | true               | -->


#### 属性options

| 参数名      | 说明                     | 类型    | 默认 |
| :---------- | :----------------------- | :------ | :--- |
| language    | 语言类型                 | String  | ''   |
| tabSize     | 制表符,按Tab键时指定缩进 | Number  | 2    |
| lineNumbers | 显示行数                 | Boolean | true |
| indentUnit  | 缩进位数                 | Number  | 2    |
| line        | 是否显示行数             | Boolean | true |

<!-- 
| ineWiseCopyCut |                                                           |                       | true        |
| viewportMargin |                                                           |                       | 1000        |
| autofocus      | 安装后立即使用焦点编辑器                                  | Boolean               | false       |
| disabled       | 禁用输入行为并禁用更改状态                                | Boolean               | false       |
| indentWithTab  | 绑定键盘Tab键事件                                         | Boolean               | true        |
| placeholder    | 空时显示                                                  | String                | ''          |
| style          | 作用于CodeMirror本身的CSS样式对象                         | Object                | {}          |
| phrases        | 短语 《详见短语》                                         | Object                | {}          |
| autoDestroy    | 在组件卸载之前自动销毁CodeMirror实例                      | Boolean               | true        |
| extensions     | 已传递给CodeMirror `EditorState.create（｛extensions｝）` | Extension             | []          |
| selection      | 已传递给CodeMirror `EditorState.create({ selection })`    | EditorSelection       | -           |
| root           | 已传递给CodeMirror `new EditorView({ root })`             | ShadowRoot / Document | -           |
 -->

- 详见短语: Codemirror [internationalization phrases](https://codemirror.net/examples/translate/)

#### 属性toolbars

| 工具名 | 说明 | 类型      | 默认 |
| :----- | :--- | :-------- | :--- |
| copy   | 复制 | `Boolean` | true |


#### 属性language

支持的语法

| 语言       |
| :--------- |
| javascript |
| html       |
| json       |
| markdown   |
| vue        |
| php        |
| java       |
| sql        |
| python     |
| xml        |
| go         |
| css        |


详见：https://codemirror.net/

#### 属性theme

支持的主题

| 主题            |
| :-------------- |
| default /light  |
| oneDark         |
| materialLight   |
| materialDark    |
| solarizedLight  |
| solarizedDark   |
| dracula         |
| githubLight     |
| githubDark      |
| aura            |
| tokyoNight      |
| tokyoNightStorm |
| tokyoNightDay   |

详见：https://codemirror.net/


### Slots插槽

| 插槽名 | 说明                    |
| :----- | :---------------------- |
| title  | 在toolbars 增加标题名称 |

### emits事件

| 事件名              | 说明                       | 参数                                                                            |
| :------------------ | :------------------------- | :------------------------------------------------------------------------------ |
| update:modelValue   | **仅当**内容发生更改时     | `(value: string, viewUpdate: ViewUpdate)`                                       |
| on-ready            | 加载成功时                 | `(payload: { view: EditorView; state: EditorState; container:HTMLDivElement })` |
| on-action           | 执行任意toolbars返回action | `(action, state)`                                                               |
| on-act-copy         | 复制成功后                 |                                                                                 |
| on-act-save         | 自动保存后                 |                                                                                 |
| on-act-theme        | 主题选择后                 |                                                                                 |
| on-act-custom       | 自定义事件                 |                                                                                 |
| on-act-choose-image | 选择图片后                 |                                                                                 |



### 相关链接

UI库
---
- [element-plus](https://element-plus.gitee.io/)  


编辑器
---
- [vue-codemirror](https://github.com/surmon-china/vue-codemirror)  
- [codemirror](https://codemirror.net/)  


