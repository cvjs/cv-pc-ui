# cv-markdown-show markdown显示组件

### 简要

>独立扩展  
>用于显示 markdown文本
 
### 示例

::: demo

<CvCodePreview>

<<< @/../example/views/editor/markdown-show.vue

</CvCodePreview>

:::

### 属性

| 属性名             | 说明         | 类型   | 默认值 |
| :----------------- | :----------- | :----- | :----- |
| v-model/modelValue | 显示的md内容 | String | true   |


### 相关链接

- https://www.npmjs.com/package/marked   
- CDN下载 https://cdn.jsdelivr.net/npm/marked/marked.min.js  