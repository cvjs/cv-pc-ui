# cv-editor-tinymce 富文本编辑器

### 简要

>独立扩展

- element quill editor
- tinymce editor


### 示例

::: demo

<CvCodePreview>

<<< @/../example/views/editor/editor-tinymce.vue

</CvCodePreview>

:::

**全局配置**

如果需要全局配置（一般情况下不需要），请修改 `src/plugins/cv-pc-ui.js` 扩展

```js
import cvPcUI from '@10yun/cv-pc-ui'
// 注册 全局设置
Vue.use(cvPcUI, {
  // 可以为编辑器配置全局属性, 每个实例都共享这个属性
  'cvEditorTinymce': {
    // 比如设置上传 upAction 后, 所有的上传图片都会采用这个属性
    upAction: 'https://xxx.com/upload/images', // 上传地址
    upHeader:{ token: 'xxx' },
    upData:{},
    upName:'file',// 文件名
    // 对响应结果进一步处理
    upResponseFn (response, file) {
      // 这里返回上传后的url即可
      // 因为是 mock 地址, 所以, 总是返回同一张图片的URL, 自己项目使用, 不会
      return response.url
      return 'https://xxx.com/upload/images' + response 
    },
  }
})
 
```


### 属性

| 属性名       | 说明                              | 类型     | 默认值 |
| :----------- | :-------------------------------- | :------- | :----- |
| v-model      | 文本内容                          | String   | ''     |
| upAction     | 上传地址                          | String   | true   |
| upFileSize   | 图片大小限制                      | Number   | -      |
| upName       | 文件名                            | String   | file   |
| upHeaders    | 上传图片携带的header              | Object   | -      |
| upData       | 自定义上传数据, 例如 {token: xxx} | Object   | -      |
| upCookie     | 是否需要带cookie                  | Boolean  | false  |
| upResponseFn | 上传成功的进一步处理              | Function | -      |


### 相关链接

- [element-plus](https://element-plus.gitee.io/)


### 其他

https://panjiachen.gitee.io/vue-element-admin-site/zh/feature/component/rich-editor.html#常见富文本


tinymce
---

- [代码 https://github.com/tinymce/tinymce](https://github.com/tinymce/tinymce)  
- [文档 https://www.tiny.cloud/docs/tinymce/latest/](https://www.tiny.cloud/docs/tinymce/latest/)    
- [代码包 https://www.tiny.cloud/get-tiny/self-hosted/](https://www.tiny.cloud/get-tiny/self-hosted/)   
- [语言包 https://www.tiny.cloud/get-tiny/language-packages/](https://www.tiny.cloud/get-tiny/language-packages/)   






https://www.jianshu.com/p/ad02e71a4fae
https://segmentfault.com/a/1190000023041835
https://blog.csdn.net/liub37/article/details/83310879
https://www.cnblogs.com/wisewrong/p/8985471.html
