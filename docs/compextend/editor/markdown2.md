
#### 二次开发
### 粘贴插入图片

`on-paste-image`虽然可以支持图片粘贴事件的监听，但不会处理图片上传至服务器并将链接插入编辑器这段逻辑。

目前如果想要支持粘贴插入图片，需要在`on-paste-image`方法里上传图片文件，拿到图片地址后，使用`on-ready`方法里返回的insertContent方法插入图片。

上述操作显得过于复杂，可以直接在源码里扩展mixins里的`handlePaste`方法,图片上传完成后，直接调用`this.insertContent`方法插入图片。

修改`/markdown/mixins/common.js`

```js
handlePaste(_, e) {// 粘贴图片
    const { clipboardData = {} } = e;
    const { types = [], items } = clipboardData;
    let item = null;
    for (let i = 0; i < types.length; i++) {
        if (types[i] === 'Files') {
            item = items[i];
            break;
        }
    }
    if (item) {
        const file = item.getAsFile();
        if (/image/gi.test(file.type)) {
            e.preventDefault();
            // 1.上传操作
            // 2.插入图片 this.insertContent(`![image](imgUrl)`)
        }
    }
}
```
## 支持流程图、甘特图等语法

目前编辑器只支持常见code语法，如果需要实现如流程图等功能，需要进一步扩展，以实现一个简单的流程图为例，具体实现思路如下：

默认情况下，markedjs会使用renderer.code方法对输入的代码块进行解析，并会借助`highlight.js`支持语法高亮。
可以将流程图语法输入到代码块内，并标明语言，重写marked.Renderer的code解析方法，结合结合`flowchart.js`路程图代码进行解析，返回文本内容。

修改`/markdown/libs/js/simple.js

```js
import hljs from './hljs';
import index from 'index';
import {parse} from 'flowchart.js'

hljs.initHighlightingOnLoad();

const renderer = new index.Renderer();
renderer.code = (code, language) => {
    if (language === 'flow') {// 流程图
        const dom = document.createElement('div');
        const flowchart = parse(code);
        flowchart.drawSVG(dom, {/*相关配置*/});
        return  dom.innerHTML;
    } else {// 默认解析
        return `<pre class="hljs"><code class="${language}">${hljs.highlightAuto(code).value}</code></pre>`
    }
}
export default index.setOptions({
    renderer,
    gfm: true,
    tables: true,
    breaks: false,
    pedantic: false,
    sanitize: false,
    smartLists: true,
    highlight: function (code) {
        return hljs.highlightAuto(code).value;
    }
})
```

## 自定义markdown语法转换

项目内使用的`index.js均为其默认配置功能，如需要特殊转换，可重写其内部的解析方法，即重写其renderer相关方法
[参考文档](https://github.com/markedjs/marked/blob/master/docs/USING_PRO.md)。

## 自动生成文档目录

预览区域和文档预览组件暂不支持自动生成目录，实现自动生成目录思路目前想到的大致有
- 重写`renderer.heading` 方法，为生成的标题添加id，输入特定快捷键，如`[TOC]`时，查找预览区域内的的所有标题标签，分析等级关系，生成目录标签

## icon替换
项目内所有的icon和命名参考`/assets/font/index.html`，替换时需注意，预览区域的checkbox为icon，注意一并替换，
修改`/assets/css/index.less`内的`input[type="checkbox"]`的`:after`样式。



# 介绍

vue-marodown是一款使用marked和highlight.js开发的一款markdown编辑器，除常见markdown语法外，支持快捷输入、图片粘贴、代码复制、全屏编辑、预览等功能。

主要包括三个部分：


1. 简单版编辑器，左侧文本输入框使用textarea实现；
2. 专业版编辑器，左侧输入框使用codemirror实现；
3. markdown预览组件，可单独使用。

使用起来简单方便，只需几行代码，即可在你的页面上引入一个markdown编辑器，编辑区支持像专业编辑器那样。
编辑器涵盖了常用的markdown编辑器功能，可通过已有属性进行配置，对编辑器功能和样式进行基本的配置，也可根据需求进行深度定制。

## 特点
- 使用简单，只需要安装npm包，引入项目即可使用，不需要繁琐的初始化配置。
- 方便扩展，根据实际需求，支持常见的功能配置，也可根据实际需求进行深度定制。
- 体积小，加载速度快，npm包删除了highlight.js和codemirror里的依赖。
- 灵活的主题，默认支持四种代码块风格，也可根据实际需求定制自己的主题样式
- 功能强大，支持专业版的编辑器，使用codemirror实现编辑窗口，可识别markdown语法
- 键盘事件监听，如保存、粘贴、回车时上次输入语法判断等
- 可扩展性强，除了提供的属性配置编辑器，也可直接在原有组件基础上进行二次开发

## 实现思路

通过监听文本输入区域内内容的变化，实时将输入的markdown语法进行编译，并渲染到预览区域。

编辑器大致分为头部菜单栏、左侧内容输入区域、右侧预览区域三个部分。
头部菜单主要为定自定义标题区域和菜单按钮，菜单按钮可通过配置文件进行显示和隐藏；左侧编辑区域，简单版使用textarea开发，满足基本需求，
专业版使用codemirror开发，编辑区域支持手动输入文本和通过头部菜单插入；右侧预览区域可实时预览输入文本，并可通过菜单按钮，进行编辑区域和预览区域的切换。





## 简单版编辑器
简单版编辑器为默认编辑器，具有体积小的优势，功能上相对于专业版无删减，区别在于
- 不支持输入文本识别，无法根据不同的语法在输入框内显示不同的样式
- 头部菜单插入内容时不支持对已选择文本区域操作

### 专业版编辑器
左侧的编辑区域使用codemirror实现，看起来更像一个专业的编辑器，保留了编辑器绝大多数功能，相比于普通版区别为
- 支持根据输入的文本类型显示不同样式
- 支持设置代码块语言，并高亮显示
- 支持对输入框内的文本选中后进行操作

## 预览组件
预览组件为单独的一个组件，区别于编辑器内的预览模式，显示样式与编辑器内预览区域完全一致，支持设置主题，复制代码，预览图片、设置maked.js初始化设置等功能。

## 如何选用

- 如果支持想简单的实现一个富文本编辑器，对功能和输入框样式没有过多的要求，建议简单版即可，毕竟体积优势大，gzip压缩后仅有50kb。
- 如果想让编辑器看起来更专业一点儿，文本输入区域功能更强大一点儿，建议选用pro版，代价是文件体积将会变大。
- 如果只是想单纯的展示markdown文件，选用markdown-preview即可。
 

### 代码体积优化

### 公共代码提取
npm包构建时，三个组件完全独立，没有抽离公共文件，所以，当同一个项目内引入其中的两个或三个组件都引入时，存在一定的重复代码，
主要为`highlight.js`、`marked`、`iconfont`、css样式几个部分。

解决方式：将组件复制到本地项目，打包时将这些文件作为公共文件抽离出来。

**注意**:三个组件中使用的iconfont为同一套，如果只是单纯的使用`preview`组件,
将会引入整个项目所使用的iconfont,可删除iconfont的引入，
重写`input[type="checkbox"]`的样式，preview组件体积将会减少一半，样式文件位于`markdown/assets/css/index.less`。

### codemirror体积优化
codemirror主要分为主文件、mode相关文件和样式文件，主文件体积异常的大，mode文件目前只选用了css/jsvascript/markdown/meta/xml五个文件，
其中markdown.js和meta.js为必须引用的，项目中已将常见的编程语言代码风格定义为css/js/xml之一，例如less/sass/scss按照css规则解析，html/vue按照xml规则解析。
优化可从一下方面入手
- 减少codemirror主文件体积
- 减少引用的mode依赖


#### iconfont 体积优化
只需要preview组件时，避免引入所有icon，参考功能扩展里icon替换方法。


