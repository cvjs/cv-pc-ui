# cv-upload-image 图片上传

### 简要

- 单张图片模式
- 多张图片模式
- 拖拽上传
- 裁剪上传


**全局配置**


```js 
// 注册 cv-pc-ui
Vue.use(cvPcUI, {
  // 可以在这里设置全局的 cvUploadImage 属性
  'cvUploadImage': {
    fileSize: 10
    upAction: 'https://jsonplaceholder.typicode.com/posts', // 上传地址
    data: {token: 'xxx'}, // 附带数据
    // 上传后对响应处理, 拼接为一个图片的地址
    upResponseFn(response, file, fileList) {
      // 根据响应结果, 设置 URL
      return 'https://xxx.xxx.com/image/' + response.id
    }
  }
});
```

### 示例


::: demo

<CvCodePreview>

<<< @/../example/views/form_upload/upload-image.vue

</CvCodePreview>

:::
 

### 属性

| 属性名             | 类型                | 默认值 | 说明                               |
| :----------------- | :------------------ | :----: | :--------------------------------- |
| v-model/modelValue | String,Object,Array |   -    | 文本内容                           |
| upAction           | String              |   -    | 必选参数，上传的地址               |
| upName             | String              |  file  | 文件名                             |
| upHeaders          | Object              |   -    | 上传图片携带的header               |
| upData             | Object              |   -    | 自定义上传数据, 例如 {token: xxx}  |
| upCookie           | Boolean             | false  | 是否需要带cookie                   |
| upAccept           | String              |   -    | 接受上传的文件类型                 |
| upResponseFn       | Function            |   -    | 上传成功的进一步处理               |
| drag               | Boolean             | false  | 是否启用拖拽上传                   |
| multiple           | Boolean             |  true  | 是否支持多选文件                   |
| limit              | Number              |   -    | 最大允许上传个数 ,文件个数显示     |
| beforeRemove       | Function            |   -    | 删除前的操作                       |
| httpRequest        | Function            |   -    | 覆盖默认上传行为，自定义上传       |
| fileType           | Array               |   -    | 文件类型, 例如['png','jpg','jpeg'] |
| fileSize           | Number              |   -    | 是否禁用                           |
| lazy               | Boolean             | false  | 图片懒加载                         |
| isShowTip          | Boolean             |  true  | 是否显示提示                       |
| isShowSuccessTip   | Boolean             |  true  | 是否显示提示                       |
| size               | Number              |  150   | 图片显示大小                       |
| thumbSuffix        | String              |   -    | 缩略图后缀                         |
 

> thumbSuffix 说明
 // 缩略图后缀, 例如七牛云缩略图样式 (?imageView2/1/w/20/h/20)
  // 如果存在, 则列表显示是加缩略图后缀的, 弹窗不带缩略图后缀

#### 裁剪组件属性

| 属性名     | 类型    | 默认值 | 说明     |
| :--------- | :------ | :----: | :------- |
| crop       | Boolean | false  | 是否剪裁 |
| cropHeight | Number  |   -    | 裁剪高度 |
| cropWidth  | Number  |   -    | 裁剪宽度 |


#### 事件

| 事件名 | 说明     |
| ------ | -------- |
| error  | 上传失败 |

 
### 相关链接
 
- [element-plus](https://element-plus.gitee.io/)
