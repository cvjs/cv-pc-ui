# cv-upload-video 视频上传

### 简要

- 视频上传扩展组件
- 优化了视频上传的体验

**使用**

```js
// 注册 cv-pc-ui
Vue.use(cvPcUI, {
  // 可以在这里设置全局的 cvUploadVideo 属性
  'cvUploadVideo': {
    fileSize: 10
    upAction: 'https://jsonplaceholder.typicode.com/posts', // 上传地址
    data: { token: 'xxx' }, // 附带数据
    // 上传后对响应处理, 拼接为一个视频的地址
    handleResponse(response, file) {
      // 根据响应结果, 设置 URL
      return 'https://xxx.xxx.com/video/' + response.id
    }
  }
});

```

### 示例

::: demo

<CvCodePreview>

<<< @/../example/views/form_upload/upload-video.vue

</CvCodePreview>

::: 

### 属性

| 属性名             | 类型                | 默认值 | 说明                               |
| :----------------- | :------------------ | :----: | :--------------------------------- |
| v-model/modelValue | String,Object,Array |   -    | 文本内容                           |
| upAction           | String              |   -    | 必选参数，上传的地址               |
| upName             | String              |  file  | 文件名                             |
| upHeaders          | Object              |   -    | 上传图片携带的header               |
| upData             | Object              |   -    | 自定义上传数据, 例如 {token: xxx}  |
| upCookie           | Boolean             | false  | 是否需要带cookie                   |
| upAccept           | String              |   -    | 接受上传的文件类型                 |
| upResponseFn       | Function            |   -    | 上传成功的进一步处理               |
| drag               | Boolean             | false  | 是否启用拖拽上传                   |
| multiple           | Boolean             |  true  | 是否支持多选文件                   |
| limit              | Number              |   -    | 最大允许上传个数 ,文件个数显示     |
| beforeRemove       | Function            |   -    | 删除前的操作                       |
| httpRequest        | Function            |   -    | 覆盖默认上传行为，自定义上传       |
| fileType           | Array               |   -    | 文件类型, 例如['png','jpg','jpeg'] |
| fileSize           | Number              |   -    | 是否禁用                           |
| isShowTip          | Boolean             |  true  | 是否显示提示                       |
| isShowSuccessTip   | Boolean             |  true  | 是否显示提示                       |
| width              | Number              |  360   | 显示宽度(px)                       |
| height             | Number              |   -    | 显示高度(默认auto)                 |
 
 

## 参考链接

- [element-plus](https://element-plus.gitee.io/)
- [element-plus upload 组件](https://element-plus.gitee.io/zh-CN/component/upload.html)
- [element-plus progress 组件](https://element-plus.gitee.io/zh-CN/component/progress.html)
