# cv-upload-avatar 头像上传

### 简要

- 头像上传

### 示例


::: demo

<CvCodePreview>

<<< @/../example/views/form_upload/upload-avatar.vue

</CvCodePreview>

:::
 
 
### 属性

| 属性名             | 类型         | 默认值 | 说明                              |
| :----------------- | :----------- | :----: | :-------------------------------- |
| v-model/modelValue | String,Array |   -    | 上传后的文件路径                  |
| src                | String       |   -    | 默认图片名称                      |
| upAction           | String       |   -    | 必选参数，上传的地址              |
| upName             | String       |  file  | 文件名                            |
| upHeaders          | Object       |   -    | 上传图片携带的header              |
| upData             | Object       |   -    | 自定义上传数据, 例如 {token: xxx} |
| upCookie           | Boolean      | false  | 是否需要带cookie                  |
| upAccept           | String       |   -    | 接受上传的文件类型                |
 