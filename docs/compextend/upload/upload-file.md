# cv-upload-file 文件上传

### 简要

- 使 el-upload 组件更简单、好用


**全局配置**

```js
 
// 注册 cv-pc-ui
Vue.use(cvPcUI, {
  // 专门设置全局的 cvUploadFile 属性
  'cvUploadFile': {
    fileSize: 10
    // 上传地址
    upAction: 'https://jsonplaceholder.typicode.com/posts',
    isCanDelete: false,
    // 上传后对响应处理, 拼接为一个URL地址
    upResponseFn(response, file) {
      // 根据响应结果, 设置 URL
      return {
        name: file.name,
        url: 'https://xxx.com/file/' + response.path, // 示例而已
        size: file.size
      }
    }
  }
})
```
 

### 示例

::: demo

<CvCodePreview>

<<< @/../example/views/form_upload/upload-file.vue

</CvCodePreview>

:::
 

### 属性
 
| 属性名             | 类型                  | 默认值 | 说明                                 |
| :----------------- | :-------------------- | :----: | :----------------------------------- |
| v-model/modelValue | String, Object, Array |   -    | 文本内容                             |
| upAction           | String                |   -    | 必选参数，上传的地址                 |
| upName             | String                |  file  | 文件名                               |
| upHeaders          | Object                |   -    | 上传图片携带的header                 |
| upData             | Object                |   -    | 自定义上传数据, 例如 {token: xxx}    |
| upCookie           | Boolean               | false  | 是否需要带cookie                     |
| upAccept           | String                |   -    | 接受上传的文件类型                   |
| upResponseFn       | Function              |   -    | 上传成功的进一步处理                 |
| multiple           | Boolean               |  true  | 是否支持多选文件                     |
| limit              | Number                |   -    | 最大允许上传个数                     |
| beforeRemove       | Function              |   -    | 删除前的操作                         |
| disabled           | Boolean               |   -    | 是否禁用                             |
| fileSize           | Number                |   -    | 图片大小限制 (MB)                    |
| fileType           | Array                 |   -    | 文件类型, 例如['png', 'jpg', 'jpeg'] |
| isCanDownload      | Boolean               |  true  | 是否显示下载                         |
| isCanDelete        | Boolean               |  true  | 是否可删除                           |
| isCanUploadSame    | Boolean               |  true  | 是否可上传相同文件                   |
| isShowSize         | Boolean               |  true  | 是否显示文件大小                     |
| isShowTip          | Boolean               |  true  | 是否显示提示                         |
| isShowSuccessTip   | Boolean               |  true  | 是否显示提示                         |

### 事件

| 事件名称 | 说明               | 回调参数         |
| -------- | ------------------ | ---------------- |
| remove   | 当文件被删除时触发 | (file, fileList) |
| success  | 文件上传成功时触发 | (file, fileList) |
| error    | 上传失败时触发     | (error)          |


### 相关链接

- [element-plus](https://element-plus.gitee.io/)
- [element-plus upload 组件](https://element-plus.gitee.io/zh-CN/component/upload.html)

