export default [
  {
    text: '复制',
    link: '/compextend/copy'
  },
  {
    text: '评论',
    link: '/compextend/comment'
  },
  {
    text: '上传',
    collapsed: false,
    items: [
      { text: '头像上传', link: '/compextend/upload/upload-avatar' },
      { text: '图片上传', link: '/compextend/upload/upload-image' },
      { text: '文件上传', link: '/compextend/upload/upload-file' },
      { text: '视频上传', link: '/compextend/upload/upload-video' }
    ]
  },
  {
    text: '编辑器',
    collapsed: false,
    items: [
      { text: '代码编辑器', link: '/compextend/editor/editor-code' },
      { text: 'json编辑器', link: '/compextend/editor/editor-json' },
      { text: 'tinymce 富文本', link: '/compextend/editor/editor-tinymce' },
      { text: 'markdown编辑器', link: '/compextend/editor/editor-markdown' },
      { text: 'markdown显示', link: '/compextend/editor/markdown-show' }
    ]
  },
  {
    text: 'geo定位地图',
    collapsed: false,
    items: [{ text: '百度地图', link: '/compextend/geo/geo-bmap' }]
  }
];
