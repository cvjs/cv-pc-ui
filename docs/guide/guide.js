export default [
  {
    text: '开发指南',
    collapsed: false,
    items: [
      { text: '介绍', link: '/guide/index' },
      { text: '快速开始', link: '/guide/quick' },
      { text: '作者', link: '/guide/author' },
      { text: '团队', link: '/guide/team' },
      { text: '交流反馈', link: '/guide/feedback' },
      { text: '赞助我们', link: '/guide/sponsor' },
      { text: '更新日志', link: '/guide/changelog' },
      { text: '更新日志vue2', link: '/guide/changelog-vue2' }
    ]
  }
];
