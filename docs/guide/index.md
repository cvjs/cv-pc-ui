<style>
.disanfang {
  text-align: center;
}
.disanfang p {
  display: inline-block;
  margin-right: 10px;
}
</style>

<p align="center">
  <img alt="logo" src="/logo.png" style="margin-bottom: 10px;width:250px">
</p>
<h1 align="center" style="font-weight: bold;">cv-pc-ui 组件 v0.3.x</h1>
<h4 align="center">pc端ui</h4>
<h4 align="center">vue3 、vite 、element-plus</h4>
<h4 align="center">一款高性能，简洁、快速开发的ui组件，功能全面，上手简单</h4>
<div class="disanfang">
<!-- 
<a href='https://gitee.com/cvjs/cv-pc-ui/stargazers'><img src='https://gitee.com/cvjs/cv-pc-ui/badge/star.svg?theme=dark' alt='star'></img></a> 
<a href='https://gitee.com/cvjs/cv-pc-ui/members'><img src='https://gitee.com/cvjs/cv-pc-ui/badge/fork.svg?theme=dark' alt='fork'></img></a>
-->

<!-- [![MIT Licence](https://badges.frapsoft.com/os/mit/mit.svg?v=103)](https://opensource.org/licenses/mit-license.php)    -->

[![star](https://gitee.com/cvjs/cv-pc-ui/badge/star.svg?theme=dark)](https://gitee.com/cvjs/cv-pc-ui/stargazers)

[![fork](https://gitee.com/cvjs/cv-pc-ui/badge/fork.svg?theme=dark)](https://gitee.com/cvjs/cv-pc-ui/members)

[![npm](https://img.shields.io/npm/v/@10yun/cv-pc-ui.svg)](https://www.npmjs.com/package/@10yun/cv-pc-ui)

[![download](https://img.shields.io/npm/dw/@10yun/cv-pc-ui.svg)](https://npmcharts.com/compare/@10yun/cv-pc-ui?minimal=true)

</div> 

---


## 特点优势

- 追去简介、快速、节省代码
- 表单组件自动判断是否在表单内，节省代码


## 基于ui完成

根据需求 选择 ui 界面

- [x] 基于 `element-plus`（开发中）
- [ ] 基于 `andt ui`（后续开发）
- [ ] 纯原生ui开发（后续开发）