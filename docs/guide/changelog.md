# 更新日志

### 2024-03-20
> 版本：v0.3.26
* [优化] `cv-editor-markdown` 编辑器，升级codemirror6，改成内部调用
* [优化] `cv-editor-code` 编辑器，升级codemirror6，改成内部调用
* [优化] `cv-markdown-show` 展示
* [优化] `cv-upload-*` 上传组件
* [优化] 打包去除console、注释，减少体积


### 2023-10-08
> 版本：v0.3.2
* [优化] `cv-pc-docs` 优化 pc-ui的文档
* [优化] `cv-editor-code` 编辑器，完善【回显、只读、高亮、主题、参数、高度】
* [优化] `cv-editor-markdown` 编辑器
* [新增] `$cvCopy` 全局复制函数

### 2023-09-01
> 版本：v0.3.1
* [新增]发布 `vue3` 版本
* [优化]合并 `cv-editor-markdown` 编辑器
* [优化]合并 `cv-editor-quill` 编辑器
* [优化]合并 `cv-editor-code` 编辑器
* [优化]合并 `cv-editor-json` 编辑器