# 快速开始

### 安装

**下载**
```sh
# npm
npm install @10yun/cv-pc-ui 
# pnpm（推荐）
pnpm install @10yun/cv-pc-ui 
```

**更新**

```sh
npm install @10yun/cv-pc-ui -f
```


### 配置

在 vue3+vite下，需要配置 ` vite.config.js ` 文件


```js

export default defineConfig({
  resolve: {
    alias: [
      // 配置路径别名，因为@冲突问题
      { find: '@10yun/cv-pc-ui', replacement: 'node_modules/@10yun/cv-pc-ui' }
    ]
  }
});

```

### 使用

详见文档 
- [组件](/components/)  
- [扩展组件](/compextend/)  
