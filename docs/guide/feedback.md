# 交流反馈


cv-pc-ui为采用MIT许可证的开源项目，使用完全免费。欢迎加QQ群交流反馈，一起学习，共同进步。

## 官方QQ交流群

点击群号即可快速加群

- 群：468705115 [点此加入](https://qm.qq.com/cgi-bin/qm/qr?k=LWz85OiKcjfshpiYPfyDd_rl6pomzP3j&jump_from=webapi) 


### 贡献代码

在使用 `cv-pc-ui` 中，如遇到无法解决的问题，请提 [Issues](https://gitee.com/cvjs/cv-pc-ui/issues) 给我们；  
假如您有更好的点子或更好的实现方式，也欢迎给我们提交 [PR](https://gitee.com/cvjs/cv-pc-ui/pulls)

