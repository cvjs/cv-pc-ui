# 更新日志

### 2023-10-08
> 版本：v0.2.58
* [优化] `cv-pc-docs` 优化 pc-ui的文档
* [优化] `cv-editor-code` 编辑器，完善【回显、只读、高亮、主题、参数、高度、焦点、双向、隐藏后显示】
* [新增] `$cvCopy` 全局复制函数


### 2023-10-04
> 版本：v0.2.54
* [优化] `cv-editor-markdown` 编辑器

### 2023-09-01

* [优化] 简化代码层级
* [优化] 代码抽象兼容vue3写法，尽量减少同步代码精力
* [合并] `cv-editor-markdown` 编辑器
* [合并] `cv-editor-quill` 编辑器
* [合并] `cv-editor-code` 编辑器
* [合并] `cv-editor-json` 编辑器

### 2022-12-16
> 版本：v0.2.47
* 文档升级 vitepress

### 2022-11-02
> 版本： v0.2.43
* [新增] `cv-show-tip` 静态提示
* 
### 2022-04-21
> 版本： v0.2.26
* [优化] `cv-table-editor` 组件使用方式
* [新增] `cv-transfer-table` 表格穿梭框

表格列编辑
* [新增] `cv-tb-edit-search` 编辑搜索
* [新增] `cv-tb-edit-text` 编辑文本
* [新增] `cv-tb-edit-opt` 新增删除操作
* [新增] `cv-tb-edit-radio` 编辑单选
* [新增] `cv-tb-edit-checkbox` 多选
* [新增] `cv-tb-edit-select` 下拉

 表格列展示
* [新增] `cv-tb-column-enum` 枚举
* [变更] `cv-table-column-text` 为 `cv-tb-column-text`
* [变更] `cv-table-column-img` 为 `cv-tb-column-img`
* [变更] `cv-table-column-base` 为 `cv-tb-column-base`
* [变更] `cv-table-column-opt` 为 `cv-tb-column-opt`
* [变更] `cv-table-column-index` 为 `cv-tb-column-index`
* [变更] `cv-table-column-check` 为 `cv-tb-column-check`
* [变更] `cv-table-column-expand` 为 `cv-tb-column-expand`

### 2022-04-09
> 版本： v0.2.24
* [优化] `cv-table-base` 、`cv-table-tree` 组件使用方式
* [新增] `cv-table-column-text` 表格列
* [新增] `cv-table-column-img` 表格列
* [新增] `cv-table-column-base` 表格列
* [新增] `cv-table-column-opt` 表格列
* [新增] `cv-table-column-index` 表格列
* [新增] `cv-table-column-check` 表格列
* [新增] `cv-table-column-expand` 表格列

### 2022-01-08
> 版本： v0.2.13
* [修复] `cv-tab-pane` 组件`label`，属性



### 2022-01-06
> 版本： v0.2.12
* [优化] `cv-table-editor` 组件`value`，监听值变化

### 2022-01-02
> 版本： v0.2.11
* [优化] `cv-input-*` 的部分组件`prepend`、`append`插槽
  
  
### 2021-12-27
> 版本： v0.2.8
* [新增] ` $cvDialogRender ` ,全局动态弹窗方法 
* [优化] 优化代码
  
### 2021-12-11
> 版本： v0.2.3
* [新增] `cv-draw-captcha` ,图形验证码 
* [优化] 优化代码

### 2021-12-04
> 版本： v0.2.2
* [优化] `cv-form-search` ,调整为 `cv-form-inline` 
* 
> 版本： v0.2.1
* [优化] cv-pc-ui ,独立 cv-form-item 出来


### 2021-12-03
> 版本： v-0.1.1
* [优化] 去除`cv-editor-quill` 的 `form—item`，减少依赖
* [优化] `cv-editor-markdown `双向绑定，重新赋值问题，去除[form-item]，独立出来

### 2021-11-08
> 版本： v0.1.72
* [优化] `cv-upload-image`、图片上传组件，回显问题、去除 `form-item`，独立处理

### 2021-06-24
> 版本： v0.1.67
* [优化] `cvTabsJump`、`cvTabPane` 组件重构，优化冗余功能，新增 `tab-position` 属性，用于定义标签位置

### 2021-06-23
> 版本： v0.1.66
* [新增] `cvTabsJump`、`cvTabPane` 组件

### 2021-06-10
> 版本： v0.1.65
* [优化] `单选框、多选框` 相关组件支持 `disabled` 属性禁用所有选项
* [优化] `cvTableList` 相关组件支持 `type=slot` 自定义插槽列

### 2021-05-27
> 版本： v0.1.62
* [优化] `cv-input-password` 组件新增 `showPassword` 属性，用来切换显示隐藏的密码框

### 2021-05-18
> 版本： v0.1.61
* [优化] 修复 `cvTableList` 组件通过 `cols` 配置 `align` 属性无效问题
* [新增] `cvTableList` 新增 `index_align` 和 `selection_align` 属性，控制索引和复选框的对齐方式
* [优化] 修改 `cvTableList` 组件全局 `align` 属性默认值为 `left`

### 2021-05-14
> 版本： v0.1.60
* [优化] `cvTableList` 组件支持 `height` 属性，设置表格列表高度
* [优化] `cvTableList` 组件支持自定义 `type="expand"` 的展开行 `slot="expand"`

### 2021-05-12
> 版本： v0.1.58
* [优化] 修复 `cv-select-base` 组件 `multiple`、`multipleLimit` 属性失效问题

### 2021-05-11
> 版本： v0.1.57
* [优化] `rules` 验证规则处理方式优化，其他优化

### 2021-05-10
> 版本： v0.1.56
* [优化] 样式优化，其他优化

### 2021-05-08
> 版本： v0.1.55
* [优化] `cv-form-item` 组件支持 `required` 属性显示必填星号

### 2021-05-07
> 版本： v0.1.54
* [优化] Form表单的 `labelWidth` 、 `col` 属性，也会在 `cv-form-item` 组件上生效

### 2021-04-29
> 版本： v0.1.53
* [新增] 时间和日期选择器相关组件，分别添加相应快捷选项，通过 `pickerOptions` 属性开启

### 2021-04-28
> 版本： v0.1.52
* [优化] 选择框双向绑定 `value` 值不再限制类型

### 2021-04-27
> 版本： v0.1.51
* [新增] `cvTableList` 组件新增 `align` 属性

### 2021-04-21
* [优化] `cv-input-counter` 组件优化


### 2021-04-21
> 版本： v0.1.50
* [新增] 添加 `Tag` 组件
* [优化] `Tag` 组件完善，优化代码逻辑，样式优化，添加更多属性支持
* [优化] `switch` 组件优化
* [优化] `Tag` 组件添加按钮优化，新增属性
* [优化] `cvCodeSms` 组件优化
* [优化] `cvCascaderBase` 组件逻辑优化，新增更多属性支持
* [优化] `cv-select-tree` 组件优化


### 2021-04-20
* [新增] 选择器相关组件新增 `dataDisabled` 属性，接收一个数组，用来禁用数组内的选项
* [优化] 静态展示分类组件命名规范统一 `imgContainer` 组件更名为 `cv-show-image` 

* [优化] `cvCodeSms` 组件优化
* [优化] `cv-input-mobile` 组件优化
* [优化] `cv-input-idcard` 组件优化
* [优化] `cvCascaderBase` 组件完善

### 2021-04-19
* [优化] `cv-select-tree` 组件 `v-model` 数据接收，输入框宽度根据 `col` 分配自适应
* [优化] 当日期时间相关组件 `valueFormat` 属性未配置时传入时间戳，控制台会输出提示
* [优化] `cv-date-range` `cv-time-range` `cv-datetime-range` 组件外部值无法传入组件内部的问题
* [优化] 日期时间相关组件逻辑优化

### 2021-04-17
* [新增] `cv-geo-city` 组件添加 `onChoose` 属性方法，返回选中对象组成的数组
* [优化] `cv-geo-city` 组件 `size` 和 `label` 属性
* [新增] `cv-select-base` 组件添加 `onChoose` 属性方法，返回两个参数分别是：当前选中的对象，和该对象在数组中的索引
* [优化] `cv-select-tree` 组件混入优化

### 2021-04-16
> 版本： v0.1.43
* [优化] 修复 `switch` 组件从外部传参时无法接收的问题
* [修复] `select选择器相关` 组件绑定数据发生变动时，组件不能及时更新显示数据的问题
* [优化] `cv-select-tree` 组件默认展开所有选项，可以通过 `defaultExpandAll` 属性修改是否展开
* [优化] 统一 `cv-select-tree` 组件属性风格，通过 `dataLists` `dataField` 配置选项数据与字段
* [优化] `cv-select-tree` 组件其他属性完善，详情见组件说明文档

### 2021-04-15
> 版本： v0.1.42
* [**重要**] 去除 form-item 组件内部重复代码 
* [新增] `cv-upload-avatar` 头像上传
* [新增] `cv-upload-image` 图片上传
* [新增] `cv-upload-file` 文件上传
* [新增] `cv-upload-video` 视频上传
* [优化] 文档完善
* [优化] ` cvTableEditor` 组件添加 `cv-form-item` 包裹
* [优化] 修复 `cvTableEditor` 无法显示必填星号的问题
* [优化] 混入mixins文件抽离，使其他扩展组件，可以快速复用


### 2021-04-14
* [优化] `imgContainer` 组件通过 `url` 绑定图片地址
* [优化] 日期时间相关选择器
* [优化] `cv-geo-city` 组件支持 `gutter` 属性调整选择框间距 默认为 `20`

### 2021-04-13

> 版本： v0.1.35
* [新增] `cvTableEditor` 表格编辑器
* [新增] `cv-form-item` 组件
* [优化] `radio` 相关组件、 `checkbox` 相关组件、 `select` 相关组件 `value` 值为数值 `0` 时无法正确选中的问题
* [优化] `radio` 相关组件、 `checkbox` 相关组件冗余属性
* [优化] `imgContainer` 组件通过 `v-model` 绑定图片 `url`

### 2021-04-12
> 版本： v0.1.34
* [优化] 日期时间选择器 返回值为 “年-月-日 时:分:秒” 字符串格式
* [新增] 日期时间相关组件新增 `valueFormat` 属性，用来指定组件接收与返回数据的格式

### 2021-04-09
> 版本： v0.1.32
* [优化] 时间选择器 返回值为 “年/月/日 时:分:秒” 字符串格式
* [优化] `cvTableList` 组件分页区域 “刷新按钮” 与 ”总条数“ 之间的间距

### 2021-04-08
* [优化] `cv-textarea` 组件 `autosize` 属性失效问题

### 2021-04-07
> 版本： v0.1.31
* [新增] `cv-datetime-range` （日期时间区间） 组件及代码片段
* [优化] `cv-form-base` 组件 `gutter` 属性默认值为 0
* [优化] `cv-form-base` 组件 `gutter` 属性默认值为 10
* [优化] `size` 属性
* [优化] `form` 和 `item` 组件具有 `size` 属性，可以控制表单内组件的大小

### 2021-04-06
> 版本： v0.1.29
* [新增] `cv-form-base` 组件及代码片段
* [优化] `cv-input-btn` 组件
* [优化] `cv-form-base` `cv-form-base` 表单组件可以设置 `col` 属性，组件内部子元素会继承该属性设置
* [优化] `cv-form-base` `cv-form-base` 表单组件可以设置 `gutter` 属性，从而控制内部元素间距
* [优化] `cv-btn-base` 组件

### 2021-04-03
* [新增] `select-city` 组件及代码片段
* [新增] `cv-show-text` 组件及代码片段
* [新增] `cvImgContainer` 组件及代码片段
* [优化] `input` 输入框自适应宽度
* [优化] `labelWidth` 属性，可自动补齐单位 `px`
* [优化] 日期时间的 `label` 属性值默认为空

### 2021-04-02
> 版本： v0.1.27
* [新增] `cv-btn-text` 固定文本形式按钮组件
* [优化] 接口请求 `RequestClass` 类
* [优化]  `cvTableList` 组件
* [优化]  `cvTableTree` 组件

### 2021-04-01
> 版本： v0.1.26
* [优化] `cv-input-text` 组件 `label` 属性值默认为空
* [优化] `cv-btn-base` 组件 `click` 事件传递，现在可以通过 @click 给该组件绑定单击事件
* [优化] `select` 选择器相关组件支持接收 `Number` 类型数据

### 2021-03-31
* [新增] `cv-show-text` 组件，通过 `text` 属性绑定要显示的文本
* [优化] `cv-input-text` 组件 `label` 属性值默认为空
* [优化] `cv-btn-base` 组件 `click` 事件传递，现在可以通过 @click 给该组件绑定单击事件
* [优化] `select` 选择器相关组件支持接收 `Number` 类型数据
* [优化] `imgContainer` 组件优化

### 2021-03-31
* [优化] `tip` 属性
* [优化] `cv-select-base` 组件

### 2021-03-29
> 版本： v0.1.23
* [新增] 接口请求 `RequestClass` 类
* [新增] 接口请求 `cvTableEditGoods` 可编辑商品表格
* [优化] `input` 相关组件
* [优化] `cvRow` 属性
* [优化] `cvCol` 属性

### 2021-03-28
* [优化] `cv-form-base` 组件 `labelWidth` 属性可以设置内部子元素的 `labelWidth` 属性值

### 2021-03-24
> 版本： v0.1.9
* 组件优化完善
* 列表表格组件相关`bug`

### 2021-03-20
> 版本： v0.1.0
* cv-pc-ui 新版本
* 组件优化完善
* 新增 `cv-checkbox-opt-btn` 组件

### 2021-03-18
> 版本： v0.0.1
* cv-pc-ui 新版本

## < 2021-03-18
> 版本： v0.0.0
* 弹窗
* 列表
* 一些其他的整合

### 2020-05-03

1. 初始化 【ctoui-dialog 】组件版本

### 2020-05-02

1. 初始化[ctoui-table]组件版本