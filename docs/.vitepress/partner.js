export default {
  platinum: [
    {
      name: '十云',
      url: 'https://www.10yun.com/',
      img: 'stackblitz.svg'
    },
    {
      name: 'Shopify',
      url: 'https://shopify.com/',
      img: 'shopify.svg'
    },
    {
      name: 'Storyblok',
      url: 'https://www.storyblok.com',
      img: 'storyblok.png'
    }
  ],
  gold: [
    {
      name: 'Tailwind Labs',
      url: 'https://tailwindcss.com',
      img: 'tailwind_labs.svg'
    },
    {
      name: 'Repl.it',
      url: 'https://replit.com/',
      img: 'repl_it.svg'
    },
    {
      name: 'Vue Jobs',
      url: 'https://vuejobs.com/?ref=vuejs',
      img: 'vue_jobs.png'
    },
    {
      name: 'divriots',
      url: 'https://divriots.com/',
      img: 'divriots.png'
    },
    {
      name: 'Cypress.io',
      url: 'https://cypress.io',
      img: 'cypress_io.svg'
    },
    {
      name: 'Prefect.io',
      url: 'https://www.prefect.io/',
      img: 'prefect_io.svg'
    },
    {
      name: 'JetBrains',
      url: 'https://www.jetbrains.com/',
      img: 'jetbrains.png'
    }
  ]
};
