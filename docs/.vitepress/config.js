import { defineConfig } from 'vitepress';

// import { fileURLToPath } from 'url';
// import { dirname, resolve } from 'path';
// const vitepressDir = dirname(fileURLToPath(import.meta.url));
// console.log(vitepressDir);
// console.log(resolve(vitepressDir, '../../example/views/'));

const ogDescription = 'cv-pc-ui是极简快速开发的ui组件';
const ogImage = 'https://vitejs.dev/og-image.png';
const ogTitle = 'cvjs';
const ogUrl = 'https://cvjs.cn';

import renderPermaLink from './render-perma-link';
import MarkDownItCustomAnchor from './markdown-it-custom-anchor';

// import configConfig from './config/config_config.js';

import guideAll from '../guide/guide.js';
import componentsAll from '../components/components.js';
import compextendAll from '../compextend/compextend.js';
import developAll from '../develop/develop.js';
import questionAll from '../question/question.js';

// a[title='站长统计'] {
//   display: none;
// }

//友盟代码统计
// let script = document.createElement('script');
// script.src = 'https://s4.cnzz.com/z_stat.php?id=1279265777&web_id=1279265777';
// script.language = 'JavaScript';
// document.body.appendChild(script);

// export default {
export default defineConfig({
  // 网站标题
  title: 'cv-pc-ui 官方文档',
  author: '十云',
  //网站描述
  description: 'cvui、cvjs.cn、十云、10yun、uniui',
  //  部署时的路径 默认 /  可以使用二级地址 /base/
  base: '/cv-pc-ui/',
  outDir: '../dist',
  //语言
  // lang: 'zh-CN',
  // lang: 'cn-ZH',
  // lang: 'en-US',
  // vue: {
  //   reactivityTransform: true
  // },
  lastUpdated: true,
  // 改变title的图标
  head: [
    ['link', { rel: 'icon', type: 'image/svg+xml', href: '/logo.svg' }],
    ['meta', { property: 'og:type', content: 'website' }],
    ['meta', { property: 'og:title', content: ogTitle }],
    ['meta', { property: 'og:image', content: ogImage }],
    ['meta', { property: 'og:url', content: ogUrl }],
    ['meta', { property: 'og:description', content: ogDescription }],
    ['meta', { name: 'twitter:card', content: 'summary_large_image' }],
    ['meta', { name: 'twitter:site', content: '@vite_js' }],
    ['meta', { name: 'theme-color', content: '#646cff' }],

    // ['style', {}, "a[title='站长统计'] {display: none;}"],
    // ['script ', { type: 'text/javascript', src: '' }]
    // ['meta', { name: 'keywords', content: 'cvjs,cv-pc-ui,cv,ui组件,使用教程,uni-app组件,vue组件' }],
    // ['meta', { name: 'description', content: 'cv-pc-ui是极简快速开发的ui组件；基于uniapp开发；' }],
    // [
    //   'script',
    //   {
    //     src: 'https://cdn.usefathom.com/script.js',
    //     'data-site': 'CBDFBSLI',
    //     'data-spa': 'auto',
    //     defer: ''
    //   }
    // ],
    //图片放在public文件夹下
    ['link', { rel: 'icon', href: '/img/linktolink.png' }]
  ],
  // 主题配置
  themeConfig: {
    logo: '/logo-nobg.png',
    outline: 3, //需要的大纲级别
    outlineTitle: '内容大纲',
    lastUpdatedText: '最近更新时间',
    editLink: {
      editLinks: true,
      text: '为此页在<码云>上提供修改建议',
      // pattern: 'https://github.com/10yun/cv-mobile-docs/edit/main/:path',
      // pattern: 'https://gitee.com/cvjs/cv-mobile-docs/blob/master/docs/:path',
      pattern: 'https://gitee.com/cvjs/cv-pc-ui/issues/new?title=文档建议:path'
    },
    docFooter: {
      prev: '上一篇',
      next: '下一篇'
    },
    footer: {
      message: 'Copyright © 2017 10yun.com | 十云提供计算服务-IPV6 | ctocode组开发',
      copyright:
        '<a href="https://beian.miit.gov.cn" target="_blank">闽ICP备18000673号-5</a><br/>本文档内容版权为 cvjs 官方团队所有，保留所有权利。'
    },
    socialLinks: [
      {
        icon: {
          svg: '<img src="https://gitee.com/assets/favicon.ico" style="width: 20px;height: 20px;" />'
        },
        link: 'https://gitee.com/cvjs/cv-pc-ui'
      },
      { icon: 'github', link: 'https://github.com/10yun' },
      { icon: 'github', link: 'https://github.com/vuejs/vitepress' }
    ],
    // algolia: {
    //   appId: '7H67QR5P0A',
    //   apiKey: 'deaab78bcdfe96b599497d25acc6460e',
    //   indexName: 'vitejs',
    //   searchParameters: {
    //     facetFilters: ['tags:cn']
    //   }
    // },

    // carbonAds: {
    //   code: 'CEBIEK3N',
    //   placement: 'vitejsdev'
    // },

    localeLinks: {
      text: '简体中文',
      items: [
        // { text: 'English', link: 'https://vitejs.dev' },
        // { text: '日本語', link: 'https://ja.vitejs.dev' },
        // { text: 'Español', link: 'https://es.vitejs.dev' }
      ]
    },

    // 头部导航
    nav: [
      { text: '指南', link: '/guide/', activeMatch: '/guide/' },
      { text: '组件', link: '/components/', activeMatch: '/components/' },
      { text: '扩展组件', link: '/compextend/', activeMatch: '/compextend/' },
      { text: '应用开发', link: '/develop/', activeMatch: '/develop/' },
      { text: '常见问题', link: '/question/', activeMatch: '/question/' },
      { text: '常用函数', link: 'https://cvjs.cn/cv-js-utils' },
      {
        text: '相关链接',
        items: [
          { text: 'cvjs', link: 'https://cvjs.cn/' },
          { text: '开发平台', link: 'https://open.10yun.com/' }
        ]
      }
    ],
    // 侧边导航
    sidebar: {
      '/guide/': guideAll,
      // '/config/': [configConfig],
      '/components/': componentsAll,
      '/compextend/': compextendAll,
      '/develop/': developAll,
      '/question/': questionAll
    },
    sidebarDepth: 2
  },

  markdown: {
    // 显示行号
    // lineNumbers: true,
    anchor: {
      permalink: renderPermaLink
    },
    config: (md) => {
      md.use(MarkDownItCustomAnchor);
    }
  }
  // vite: {
  //   resolve: {
  //     alias: {
  //       exampleDir: resolve(vitepressDir, '../../example/views/')
  //     }
  //   }
  // }

  // plugins: [
  //   ['vuepress-plugin-code-copy', true],
  //   ['vuepress-plugin-demo-container', true]
  // ],
  // configureWebpack: {
  //   resolve: {
  //     alias: {
  //       '@alias': baseLink
  //     }
  //   }
  // },
});
