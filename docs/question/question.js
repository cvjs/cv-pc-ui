export default [
  {
    text: '常见问题',
    collapsed: true,
    items: [
      { text: '问题', link: '/question/' },
      { text: '启动时同步加载配置', link: '/question/load-config' },
      { text: '主题设计', link: '/question/themes' },
      { text: '防止按钮重复点击', link: '/question/btn-click' }
    ]
  }
];
