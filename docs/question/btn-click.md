# 时间段内防止重复点击


### 防抖模式

- 创建 `项目/src/plugins/bindGlobal/base-btn-.js`

```js
import Vue from 'vue'
Vue.directive('noMoreClick', {
  inserted(el, binding) {
    el.addEventListener('click', e => {
      el.classList.add('is-disabled')
      el.disabled = true
      setTimeout(() => {
        el.disabled = false
        el.classList.remove('is-disabled')
      }, 2000)//我这里设置的是2000毫秒也就是2秒
    })
  }
});
```

使用 v-no-more-click指令

```vue
<cv-btn-base v-no-more-click type="primary" @click="submitForm('ruleForm')">确 定</cv-btn-base>
```


### 自带回调模式(推荐)

```vue
<template>
  <div>
    
    <h3>加载中，防止重复点击</h3>
    <cv-btn-base type="primary" :autoLoading="true" @click="handleSubmit1($event, 'aaaa', 'bbb')">提交按钮</cv-btn-base>

    <h3>文字按钮</h3>
    <cv-btn-text type="primary" autoLoading @click="handleSubmit1($event)">提交按钮</cv-btn-text>
    <cv-btn-text type="primary" autoLoading @click="handleSubmit2($event, 'aaaa', 'bbb')">删除按钮</cv-btn-text>

  </div>
</template>

<script>
export default {
  methods: {
    handleSubmit1(done, aaa, bbb) {
      // 这里供业务组件处理一些事情,比如ajax请求,此处用setTimeout模拟, 执行done()方法消失loading
      setTimeout(() => {
        done();
      }, 1000);
    },
    // 传递参数，防止重复提交
    handleSubmit2(done, aaa, bbb) {
      // console.log(aaa, bbb, done);
      setTimeout(() => {
        done();
      }, 1000);
    }
  }
};
</script>

```

